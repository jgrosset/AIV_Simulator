# AIV Simulator

The simulator is designed in a generic way to integrate different types of traffic plans. It is implemented in Python with the Qt framework. We are using and developing this simulator for various laboratory experiments, and for teaching engineering students.
The project repository is available at : [https://gitlab.inria.fr/jgrosset/AIV_Simulator](https://gitlab.inria.fr/jgrosset/AIV_Simulator) 

![appli](/images/appli.png)

# Install the environment

-  Download all the repository and follow the subsection **Create the conda environment** to install the required modules. 

After the installation, just follow the section **Launch the application**.

## Create the conda environment 

The required modules of the simulator are in the *.devcontainer/* folder: **simulator_env.yml** file. 
- `conda env create -f simulator_env.yml`

# Launch the application

To launch the simulator_aiv: 
- in *src/* folder: `python Warehouse_Application`
