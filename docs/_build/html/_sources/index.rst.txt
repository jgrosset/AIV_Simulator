.. AIV Simulator documentation master file, created by
   sphinx-quickstart on Mon Mar  6 18:17:48 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

AIV Simulator Documentation
==========================================

Welcome to AIV Simulator Documentation! This page will guide you through the basic steps to install the AIV Simulator and run it to experiment different scenarios in a given warehouse traffic plan. It is possible to describe a different traffic plan by integrated it in the code.

.. toctree::
   :maxdepth: 2
   :caption: Getting Started

   readme

.. toctree::
   :maxdepth: 2
   :caption: Knowledge Base

   scenarios
   task-allocation
   class

.. toctree::
   :maxdepth: 2
   :caption: Research

   contribution
   team

.. toctree::
   :maxdepth: 2
   :caption: Developer

   modules
   wishlist


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
