# Scenarios 

| Scenario presented in the contribution | In the simulator (which button) |
|-----------------------------------|--------------------------|
| Sc0 - Nominal Case | Scenario 0  |
| Sc1 – Obstructed aisle | Scenario 1 |
| Sc2 – AIV Breakdown | Scenario 2 |
| Sc3 – Inaccessible point | Scenario 3 |
| Sc4 – Camera failure | Scenario 4 |

- **Simulation:** Create a simulation with the same task flow as scenarios 0, 1, 2, 3, and 4. During the scenario, it is possible to generate obstacles or simulate robot breakdowns.

- **Random Scenario:** Create a simulation with a random task flow.

## Scenario 0

**Problem**: AIVs perform all tasks (mission, allocation, path planning) without encountering any problems.

**Communication**: 
- I2V: Supervisor gives tasks to auctioneer AIV
- V2V: CTM and CRM messages for the task allocation (auctions, reception and feedback)

**Example figure**: ![circuit](/images/circuit.png)

**Dijkstra graph**: ![circuit-dijsktra](/images/circuit-dijkstra.png)

## Scenario 1

**Problem**: If an obstacle obstructs an aisle, then an AIV that has to cross this aisle must quickly replan its path.
**Solution**: Cooperation AIV <-> infrastructure

**Communication**: 
- I2V: 
    - Supervisor gives tasks to auctioneer AIV 
    - Camera send a CPM “Obstacle detected”
- V2V: CTM and CRM messages for the task allocation (auctions, reception and feedback)

**Example figure**: ![scenario_1](/images/scenario_1.png)

**Sequence diagram** ![seq-diag_sc1](/images/sequence-diag_sc1.png)

## Scenario 2

**Problem**: An inability for an AIV to complete its mission (obstacles or breakdown).
**Solution**: Cooperation between the AIVs (for sharing missions).

**Communication**: 
- I2V: 
    - Supervisor gives tasks to auctioneer AIV 
- V2V: 
    - CTM and CRM messages for the task allocation (auctions, reception and feedback)
    - CTM messages to share missions

**Example figure**: ![scenario_2](/images/scenario_2.png)

**Sequence diagram** ![seq-diag_sc2](/images/sequence-diag_sc2.png)

## Scenario 3

**Problem**: An inability for an AIV to complete its mission due to an event occurring in the defined environment to perform the task (stock point is inaccessible).
**Solution**: Cooperation AIV <-> Worker (to remove an obstacle).

**Communication**: 
- I2V: 
    - Supervisor gives tasks to auctioneer AIV - Camera send CPM: obstacle detected
- I2P: 
    - CPM message for an obstacle at stock point 
- V2V: 
    - CTM and CRM messages for the task allocation (auctions, reception, and feedback)
    - CTM messages to share missions
- V2P: 
    - DENM messages for a blocking problem P2V: CPM message indicating no more obstacle

**Example figure**: ![scenario_0](/images/scenario_3.png)

**Sequence diagram** ![seq-diag_sc3](/images/sequence-diag_sc3.png)

## Scenario 4

**Problem**: If a camera sends false information, this information has to be checked.
**Solution**: Cooperation AIV <-> Worker or between the AIVs (to verify information sent by the camera)

**Communication**: 
- I2V: 
    - Supervisor gives tasks to auctioneer AIV - Camera send CPM: obstacle detected
- I2P: 
    - CPM message for an obstacle at stock point 
- V2V: 
    - CTM and CRM messages for the task allocation (auctions, reception, and feedback)
    - CTM messages to share missions
- V2P: 
    - DENM messages for a blocking problem 
- P2V: 
    - CPM message indicating no more obs

**Example figure**: ![scenario_0](/images/scenario_4.png)

**Sequence diagram** ![seq-diag_sc4](/images/sequence-diag_sc4.png)

