# Contribution 

AIV Simulator has been used in:

- J. Grosset, A.-J. Fougères, M. Djoko-Kouam, J.-M. Bonnin. ‘Multi-agent Simulation of Autonomous Industrial Vehicle Fleets: Towards Dynamic Task Allocation in V2X Cooperation Mode’. 1 Jan. 2024, 1 – 18, DOI: 10.3233/ICA-240735.

If you plan to use the simulator, please cite this paper. 

