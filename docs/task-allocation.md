# Task Allocation

**Task allocation process**: ![task-allocation](/images/task-allocation.png)

**CTM and CRM messages exchanges during task allocation**: ![sequence-diag_task-allocation](/images/sequence-diag_task-allocation.png)