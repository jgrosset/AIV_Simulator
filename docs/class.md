# Class 

## Architecture diagram 
In this architecture, an industrial infrastructure (pro- duction lines, warehouses, etc.) is deployed in an en- vironment, and is composed of a circuit and active el- ements such as beacons, tags, stations and cameras. These active elements are modelled as agents. Static or dynamic obstacles (e.g. human operators) may be present in the environment. Human workers or opera- tors, with whom AIV agents can communicate to carry out cooperative activities, are also modelled as agents.
AIV agents, grouped in the same agent community, perform missions defined by paths on the traffic map. They are equipped with a radar that allows them to evolve in a partially known environment, and can thus avoid obstacles and collisions between them.

**Simulator architecture**: dynamic elements in red, static in green, and not related to the environment in purple. ![architecture](/images/architecture.png)

## Architecture diagram vs Code

| Classes in <br> | In the code | Link to the <br> class documentation |
|----------------------------------------------------------------------|-----------------------------------|-------------|
| Infrastructure | Infrastructure | [Infrastructure](#Infrastructure) |
| Circuit | Warehouse_Circuit | [Warehouse_Circuit](#Warehouse_Circuit) |
| Environment | Environment | [Environment](#Environment) |
| StaticObstacle | Obstacle | [Obstacle](#Obstacle) |
| DynamicObstacle | N/A | - |
| Beacon | N/A | - |
| Tag | Tag | [Tag](#Tag) |
| Camera | Camera | [Camera](#Camera) |
| Station | N/A | - |
| Knowledge | N/A | - |
| Agent | Agent | [Agent](#Agent) |
| Message with classes <br> CAM, DENM, <br> CPM, MCM, CTM et CRM | - | [Message](#Message) |
| Path | Dijkstra | [Dijsktra](#Dijkstra) |
| AIV_Mission | VA_Mission | [VA_Mission](#VA_Mission) |
| AIV | VA | [VA](#VA) |
| Radar | Radar | [Radar](#Radar) |
| Worker_Mission | Worker_Mission | [Worker_Mission](#Worker_Mission) |
| Worker | Worker | [Worker](#Worker) |
| - | Console | [Console](#Console) |
| - | Experiment | [Experiment](#Experiment) |
| - | HMI | [Hmi](#Hmi) |
| - | Map | [Map](#Map) |
| - | Supervisor | [Supervisor](#Supervisor) |
| - | Task| [Task](#Task) |
| - | Warehouse_Application | [Warehouse_Application](#Warehouse_Application)|
