# AIV Simulator

The simulator is designed in a generic way to integrate different types of traffic plans. It is implemented in Python with the Qt framework. We are using and developing this simulator for various laboratory experiments, and for teaching engineering students.
The documentation of the simulator is available at : [https://aiv-simulator.readthedocs.io/en/latest/readme.html](https://aiv-simulator.readthedocs.io/en/latest/readme.html) 

![appli](docs/images/appli.png)

# Install the environment

-  Download all the repository and follow the subsection **Create the conda environment** to install the required modules. 

After the installation, just follow the section **Launch the application**.

## Create the conda environment 

The required modules of the simulator are in the *.devcontainer/* folder: **simulator_env.yml** file. 
- `conda env create -f simulator_env.yml`

# Launch the application

To launch the simulator_aiv: 
- in *src/* folder: `python Warehouse_Application`

# Sphinx documentation 

To create the sphinx documentation: 
- in *docs/* folder: `make html`
- in *docs/* folder: modify the **readme.md** file 
- in *src/* folder: create docstrings in modules in the application code 