#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Radar class for the VIA simulation application with the Warehouse circuit
# Radar as an object to start with

import time
from Warehouse_Circuit import *
import numpy as np

#Classe Radar
class Radar (object):
	"""
    Radar class used by VA to detect obstacles on the circuit.

    Attributes:
    	__VA (VA): VA who has this radar.
    	circuit (Circuit): Circuit where the VA circulates.
    	__coordVA (list[int]): position of the VA as a list of two integers [x, y].
    	__active (bool): Indicates whether the radar is active or not.
	"""

	__VA = None
	__coordVA = [-1, -1]
	__active = False
	
	def __init__(self, circuit, _VA, _coordVA):
		"""
        Constructor method for the Radar class.

        Args:
        	circuit (Circuit): Circuit where the VA circulates.
        	_VA (VA): VA who has this radar.
        	_coordVA (list[int]): position of the VA as a list of two integers [x, y].
        """
		self.__VA = _VA				# VA who has this radar
		self.circuit = circuit # Circuit where the VA circulates
		self.__coordVA = _coordVA	# localisation of the VA
		self.__active = True			#Radar in activity
	
	def setActive(self, _active):
		"""
        Set the active state of the radar.

        Args:
        	_active (bool): Indicates whether the radar is active or not.
        """
		self.__active = _active
	
	def getActive(self):
		"""getActive(self) -> (bool) Return the active state of the radar."""
		return self.__active
	
	def calculateDistance(self, origine, destination):
		"""
        Calculate the distance between two points.

        Args:
        	origine (list[int]): Starting point as a list of two integers [x1, y1].
        	destination (list[int]): End point as a list of two integers [x2, y2].

        Returns:
        	int: The distance between the two points.
        """
		x1, y1 = origine[0], origine[1]
		x2, y2 = destination[0], destination[1]
		d = np.abs(x1 - x2) + np.abs(y1 - y2)
		return d
	
	def detection(self, _direction, _coordVA):
		"""
        Perform a radar detection for obstacles in a certain direction.

        Args:
        	_direction (int): The direction in which to detect obstacles. Values:
            	1: Up
            	2: Down
            	3: Right
            	4: Left
        	_coordVA (list[int]): The current position of the VA as a list of two integers [x, y].

        Returns:
        	list[int]: A list with the following elements:
            	0: The type of obstacle detected. 0 if no obstacle.
            	1: The x coordinate of the detected obstacle.
            	2: The y coordinate of the detected obstacle.
            	3: The distance between the VA and the detected obstacle.
        """
		self.__coordVA = _coordVA
		xObstacle, yObstacle = -1, -1
		window = 15	#fenetre du radar
		typeObstacle = 0
		distance = d = window*2
		pointInterest = False
		zone = self.__VA.map.getZoom(self.__coordVA[0], self.__coordVA[1], window)
		for i in range(len(zone)):
			for j in range(len(zone)):
				#down,up,right,left = 1,2,3,4
				if zone[i][j] > 0 :
					if   _direction == 1 and j > window: 
						pointInterest = True
						d = self.calculateDistance([i,j],[window,window])
						xObstacle, yObstacle = self.__coordVA[0], self.__coordVA[1]+d
					elif _direction == 2 and j < window: 
						pointInterest = True
						d = self.calculateDistance([i,j],[window,window])
						xObstacle, yObstacle = self.__coordVA[0], self.__coordVA[1]-d
					elif _direction == 3 and i > window: 
						pointInterest = True
						d = self.calculateDistance([i,j],[window,window])
						xObstacle, yObstacle = self.__coordVA[0]+d, self.__coordVA[1]
					elif _direction == 4 and i < window: 
						pointInterest = True
						d = self.calculateDistance([i,j],[window,window])
						xObstacle, yObstacle = self.__coordVA[0]-d, self.__coordVA[1]
					else : pass

					if pointInterest == True and d < distance:
						distance = d
						typeObstacle = zone[i][j]
						
					else: pass
					pointInterest = False
				else: pass
		return [typeObstacle, xObstacle, yObstacle, distance]
