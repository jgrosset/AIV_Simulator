import csv 
import os
from datetime import datetime
import pandas as pd
import ast
from Console import *
class Experiment():
    """
    A class for managing and logging the results of an experiment.

    Attributes:
        nbVA (int): The number of agents in the experiment.
        numScenario (int): The number of the scenario being run.
        saveData (tuple): Whether to save the experiment data or not - (True/False, folder_name).
        date (str): The date when the experiment was run.
        experiment_folder (str): The folder where the experiment data will be saved.
        path_experiment_directory (str): The full path of the folder where the experiment data will be saved.
        tasks_to_be_done (list): The tasks that need to be done by thes agents.
    """
    
    def __init__(self, nbVA, numScenario, saveData):
        """
        Constructor: Create the different experiment files: config, results, resume.

        Args:
            nbVa (int): The number of the VA in the simulation.
            numScenario (int): The number of the scenario.
            saveData (tuple): Save or not the data of the experiment in files - (True/False, folder_name)
        """
        self.nbVA = nbVA
        self.numScenario = numScenario
        self.saveData = saveData
        if self.saveData[0]:
            # Directory - filename: date
            self.date = datetime.now().strftime(saveData[1] + '_%Y-%m-%d-%H-%M')
            self.experiment_folder = os.path.dirname(os.getcwd()) + '/experiments'
            if not os.path.exists( self.experiment_folder):
                os.makedirs( self.experiment_folder)
            # Path
            self.path_experiment_directory = os.path.join(self.experiment_folder, self.date)
            os.mkdir(self.path_experiment_directory)
            self.createFileConfig(numScenario)
            self.createFileResults()
            self.createFileResume()

    def saveOrNot(self):
        """saveOrNot(self) -> bool: Return the value of the saveData[0] attribute."""
        return self.saveData[0] 

    def createFileConfig(self, numScenario):
        """Create a CSV file to store the configuration of the experiment."""
        with open(self.path_experiment_directory + '/config.csv', "w") as f_output:
            csv_output = csv.writer(f_output)
            csv_output.writerow(["Scenario Number", numScenario])
            csv_output.writerow(["Type Obstacle", "Position Obstacle"])
    
    def writeObstacle(self, type, position):
        """Write the type and position of an obstacle to the configuration CSV file."""
        with open(self.path_experiment_directory + '/config.csv', "a") as config_file:
            csv_output = csv.writer(config_file)
            csv_output.writerow([type, position])

    def createFileResults(self):
        """Create a CSV file to store the results of the experiment."""
        with open(self.path_experiment_directory + '/results.csv', "w") as result_file:
            csv_output = csv.writer(result_file)
            csv_output.writerow(["Agent", "Distance", "Mission", "Task", "TaskCompleted", "Path"])

    def writeResult(self, nameAgent, distance, completedMission, task, taskCompleted, path):
        """Write the results of a virtual agent to the results CSV file."""
        data = [
        {
            'nameAgent': nameAgent,
            'distance': distance,
            'completedMission': completedMission,
            'task': task,
            'taskCompleted': taskCompleted,
            'path': path
        }
        ]
        df = pd.DataFrame(data)
        df.to_csv(self.path_experiment_directory + '/results.csv', mode='a', index=False, header=not os.path.exists(self.path_experiment_directory + '/results.csv'))

    def createFileResume(self):
        """createFileResume(self) -> Create a new resume file in the experiment directory."""
        with open(self.path_experiment_directory + '/resume.csv', "w") as f_output:
            csv_output = csv.writer(f_output)

    def read_results_data(self):
        """Reads the existing data from the results file and returns it as a DataFrame."""
        results_data = pd.read_csv(self.path_experiment_directory + '/results.csv', header=0)
        results_data = results_data.reset_index(drop=True)
        return results_data

    def calculate_summary(self, results_data, agent_type):
        """Calculate a summary DataFrame for the specified agent type."""
        agent_results_data = results_data[results_data['Agent'].str.startswith(agent_type)]
        
        # Calculate total distance
        total_distance = agent_results_data.groupby('Agent')['Distance'].sum().rename('TotalDistance')
        
        # Calculate completed tasks
        completed_tasks = agent_results_data.loc[(agent_results_data['TaskCompleted']) & ~(agent_results_data['Agent'].str.startswith('Worker')), ['Agent', 'Task']].groupby('Agent')['Task'].apply(list).rename('TasksCompleted')
        completed_tasks = completed_tasks.apply(lambda x: ', '.join([str(task) for task in x if task]))
        
        # Calculate number of tasks completed
        num_tasks_completed = agent_results_data[agent_results_data['TaskCompleted']].groupby('Agent')['TaskCompleted'].sum().rename('NumberTasksCompleted')
        num_tasks_completed = num_tasks_completed.astype(int)
        
        # Calculate tasks to do
        tasks_to_do = agent_results_data.groupby('Agent')['Task'].apply(list).rename('TaskToDo')
        tasks_to_do = tasks_to_do.apply(lambda x: [task for task in x if task])
        
        # Calculate number of tasks to do
        nb_tasks_to_do = agent_results_data.groupby('Agent')['Task'].apply(list).apply(len).rename('NumberTasksToDo')
        
        # Combine the results into a single DataFrame
        summary = pd.concat([tasks_to_do, nb_tasks_to_do, completed_tasks, num_tasks_completed, total_distance], axis=1)
        
        # Add a prefix to the agent names
        summary.index.name = "Agent"
        summary.index = summary.index.astype(str)
        
        return summary

    def writeResume(self):
        """Write a summary of the experiment results to a resume file."""
        with open(self.path_experiment_directory + '/resume.csv', "a") as resume_file:

            # Read the existing data from file if it exists
            results_data = self.read_results_data()

            # Calculate summary for VA agents
            va_results_data = results_data[results_data['Agent'].str.startswith('VA')]
            va_summary = self.calculate_summary(va_results_data, "VA")

            # Calculate summary for Worker agents
            worker_results_data = results_data[results_data['Agent'].str.startswith('Worker')]
            worker_summary = self.calculate_summary(worker_results_data, "Worker")

            # Add a row for total VA
            #TODO: the number of tasks to do have to depend of the number of tasks given by the supervisor
            va_summary.loc['Total VA'] = [self.tasks_to_be_done, len(self.tasks_to_be_done),  ', '.join(va_summary['TasksCompleted'].dropna().astype(str)), va_summary['NumberTasksCompleted'].sum(), va_summary['TotalDistance'].sum()]
            va_summary['NumberTasksCompleted'] = va_summary['NumberTasksCompleted'].fillna(0).astype(int)

            # Combine the results into a single DataFrame
            resume = pd.concat([va_summary, worker_summary])

            # Write the results to a CSV file
            resume.to_csv(resume_file)

            eventTrace(resume, simulationTrace)

    def getNumStrategy(self):
        """getNumStrategy(self) -> int: Return the number of the strategy."""
        return self.numStrategy
    
    def getTasksToBeDone(self):
        """getTasksToBeDone(self) -> lest: Return the list of tasks to be done."""
        return self.tasks_to_be_done

    def setTasksToBeDone(self, tasks):
        """
        Set the tasks to be done in the environment.

        Args:
            tasks (list): The list of tasks to be done.
        """
        self.tasks_to_be_done = tasks