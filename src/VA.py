#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Class VA for the VA simulation application with the Warehouse circuit

import time, random
import re
from Agent import *
from Radar import *
from Message import *
from VA_Mission import *
from Warehouse_Circuit import *
from Console import *

from PyQt6.QtCore import QObject, QThread, QWaitCondition, QMutex, pyqtSignal, pyqtSlot

# VA States
STOP, CIRCULATES, DECELERATE, ACCELERATE, FAILURE, TERMINATE = 0, 1, 2, 3, 4, 5
# Situations in relation to intersections
OUT, IN, CENTER = 0, 1, 2

class VA(QObject):
	"""
	A class representing a Virtual Agent (VA) in a simulation.

	VA States:
		- STOP: The VA is stationary
		- CIRCULATES: The VA is moving within the circuit
		- DECELERATE: The VA is slowing down
		- ACCELERATE: The VA is accelerating
		- FAILURE: The VA has experienced a failure
		- TERMINATE: The VA is inactive

	Situations in relation to intersections:
		- OUT: The VA is not at an intersection
		- IN: The VA has entered an intersection
		- CENTER: The VA is in the middle of an intersection

	Attributes:
		update_position (pyqtSignal): A pyqtSignal indicating that the position of the VA has been updated
		update_listAgents (pyqtSignal): A pyqtSignal indicating that the list of agents has been updated
		__parking (int): An integer representing the index of the parking node that the VA starts at
		__segment (list): A list representing the segment that the VA is currently on
		__status (int): An integer representing the current state of the VA
		__intersection (int): An integer representing the VA's situation in relation to intersections
		__freePassage (list): A list representing the free passage of the VA at intersections
		__radarStatus (int): An integer representing the VA's status in relation to the radar
		__tagReaderStatus (int): An integer representing the VA's status in relation to the tag reader
		__coord (list): A list representing the current coordinates of the VA
		__missionAssigned (boolean): A boolean indicating whether a mission has been assigned to the VA
		num_VA (int): A class variable representing the number of VAs instantiated
		agent (Agent): An Agent object representing the VA
		va_name (str): A string representing the name of the VA
		interface (Hmi): An object representing the interface of the simulation
		env (Environment): An object representing the environment of the simulation
		__thread (QThread): A QThread object representing the thread on which the VA runs
		infrastructure (Infrastructure): An object representing the infrastructure of the simulation
		radar (Radar): A Radar object representing the radar of the VA
		map (Map): An object representing the map of the simulation
		available (boolean): A boolean indicating whether the VA is available
		mission (Mission): An object representing the current mission of the VA
		missionCompleted (boolean): A boolean indicating whether the current mission has been completed
		pendingMissions (list): A list representing the pending missions of the VA
		nbTasksValidated (int): An integer representing the number of tasks validated by the VA
		bidsMissions (dict): A dictionary representing the bids on missions made by the VA
		memoryObstacles (list(list)): A list of information about obstacles present in the environment.
		experiment (Experiment): An object representing the experiment of the simulation
		_running (boolean): A boolean attribute that is initially set to True. This attribute is used to control the execution of the thread. When it is set to False, the thread will exit its run method and stop.
		cond (QWaitCondition): Object which provides a way for threads to synchronize their execution. Threads can wait for a signal from another thread before proceeding, which can be useful in situations where data needs to be shared between threads.
		mutex (QMutex): Object which is used to protect shared resources from concurrent access. When a thread acquires a mutex, no other thread can access the protected resource until the mutex is released.

	Signals:
		update_listAgents (pyqtSignal(int, list)): emitted when the list of VAs in the simulation needs to be updated.
		update_position (pyqtSignal(str)): emitted when the position of the VA on the map needs to be updated.
	
	"""
	__parking = -1
	__segment = []
	__status = STOP
	__intersection = OUT
	__freePassage = []
	__radarStatus = IN
	__tagReaderStatus = IN
	__coord = [-1, -1]
	__missionAssigned = False
	
	num_VA = 0

	update_position = pyqtSignal(int, list)
	update_listAgents = pyqtSignal(str)

	def __init__(self, _env, _infrastructure, _map, _experiment):
		"""Constructor method for the VA class."""
		VA.num_VA +=1
		self.__num = VA.num_VA # number of the VA
		
		self.agent = Agent("VA", VA.num_VA)
		self.agent.va = self # add a reference to the VA in the Agent 

		# launch of the run() function when start() is activate
		super().__init__()
		self.__thread = QThread()
		self.moveToThread(self.__thread)
		self.__thread.started.connect(self.run)
		
		self.va_name = "VA"+str(self.__num)
		self.interface = _env.interface
		self.env = _env

		# connect the pyqtsignal update_position to the pyqtslot movingVA of the interface
		self.update_position.connect(self.interface.movingVA)
		# connect the pyqtsignal update_position to the pyqtslot updateListAgents of the interface
		self.update_listAgents.connect(self.interface.updateListAgents)

		self.__parking = self.interface.circuit.getParkingNode(self.__num-1) # parking as source point
		self.__coord = self.interface.circuit.getSpecificLoc(self.__parking)

		txt = "Launch of VA" + str(self.__num) + " at parking " + str(self.__parking)
		appliTrace(txt)

		#self.supervisor = _superviseur	#to communicate with the supervisor
		self.infrastructure = _infrastructure # to communicate with the infrastructure
		self.radar = Radar(self.interface.circuit, self, self.__coord)
		self.map = _map		# map shared by all the VAs
		self.map.initCoordAgent(self.__coord)
		for i in range(len(self.interface.circuit.getLoc())):
			self.__freePassage.append(-1)

		self.available = True # VA is available if it can be the robot master : battery is ok for example

		self.setMissionAssigned(False)
		self.missionCompleted = False
		self.pendingMissions = []
		self.blockedMissions = []

		# To hold back information about obstacles
		self.memoryObstacles = []
		# Number tasks validated and update interface
		self.nbTasksValidated = 0
		self.interface.updateVADataLabels(self.getNameVA(), "nbValidatedTasks", self.nbTasksValidated)

		# If the VA is the master to supervise bids on missions
		# It stores all the bids on missions on this attribute
		self.bidsMissions = {} 

		self.experiment = _experiment

		# Attribute use when the VA is stop and have to circulates again
		self._running = True
		self.cond = QWaitCondition()
		self.mutex = QMutex()
		self.__thread.start()


	@staticmethod
	def resetNumVA():
		"""Reset the class attribute `num_VA` to 0."""
		VA.num_VA = 0
	
	@pyqtSlot(str)
	def add_VA_to_list_agents(self):
		"""Update the list of agents `listAgents` to simulate failures in the interface."""
		self.update_listAgents.emit(self.va_name)
	
	def getNameVA(self):
		"""getNameVA(self) -> (str) Return the name of the VA."""
		return self.va_name

	def getStatus(self):
		"""getStatus(self) -> (int) Return the status of the VA."""
		return self.__status

	
	def getNbTasksValidated(self):
		"""getNbTasksValidated(self) -> (int) Return the number of tasks validated by the VA."""
		return self.nbTasksValidated

	def notValidateTask(self):
		"""Notify that a task is not validated in the experiment"""
		self.saveInfoTask(False)

	def validateTask(self):
		"""
		Notify that a task is validated in the experiment.
		
		Update the VA data labels in the interface.
		"""
		self.nbTasksValidated += 1
		self.interface.updateVADataLabels(self.getNameVA(), "nbValidatedTasks", self.nbTasksValidated)
		self.saveInfoTask(True)

	def saveInfoTask(self, validate):
		"""
		Save the information about the completed task, and update the experiment results data saved.

    	Args:
    		validate (bool): True if the task is validated, False otherwise.
		"""

		txt = "VA{} - info {} - {}".format(self.__num, validate, self.mission.getCurrentPartMission())
		eventTrace(txt, simulationTrace)

		task = None
		if self.mission.getCurrentPartMission() == 1:
			task = self.mission.getTask().getFirstTask() 
			if validate:
				self.env.addCompletedTask(task)
				# update the interface	
				self.updateSupervisionTask("validated", True)

				if self.experiment.saveOrNot():
					firstPartTravelledDistance = self.mission.getTrackingTravelledDistance()[:-1].copy() # delete the last track distance node which corresponds to the next move

					self.mission.setFirstPartTrackingTravelledDistance(firstPartTravelledDistance)
					# write results concerning the task in csv file
					self.experiment.writeResult(self.getNameVA(), self.mission.calculateTravelledDistance(), self.mission.getShortVersionPathMission(), task, validate, firstPartTravelledDistance)

				# To obtain information about a part of task finished
				txt = "VA{} travelled a distance of {}".format(self.__num, self.mission.calculateTravelledDistance())
				eventTrace(txt, simulationTrace)
				txt = "VA{} followed the path : {}".format(self.__num, self.mission.getTrackingTravelledDistance())
				eventTrace(txt, simulationTrace)

		
			else : # if not validate : add the second part (second task) of the mission to the value: False for TaskCompleted
				task = self.mission.getTask().getSecondTask() 
				if self.experiment.saveOrNot():

					firstPartTravelledDistance = self.mission.getTrackingTravelledDistance().copy() # not delete the last track distance node which corresponds to the current segment that is counted when VA is stopped for the experiment results

					self.mission.setFirstPartTrackingTravelledDistance(firstPartTravelledDistance)

					# write results concerning the task in csv file
					self.experiment.writeResult(self.getNameVA(), self.mission.calculateTravelledDistance(path_stopped=True), self.mission.getShortVersionPathMission(), task, validate, firstPartTravelledDistance)

					self.experiment.writeResult(self.getNameVA(), 0, self.mission.getShortVersionPathMission(), task, validate, [])

					# To obtain information about a part of task finished
					txt = "VA{} travelled a distance of {}".format(self.__num, self.mission.calculateTravelledDistance(path_stopped=True))
					eventTrace(txt, simulationTrace)
					txt = "VA{} followed the path : {}".format(self.__num, firstPartTravelledDistance)
					eventTrace(txt, simulationTrace)

		else : 
			task = self.mission.getTask().getSecondTask() 
			if validate:
				self.env.addCompletedTask(task)	
				# update the interface
				self.updateSupervisionTask("validated", True)
			if self.experiment.saveOrNot():
				secondPartTravelledDistance = self.mission.getSecondPartTrackingTravelledDistance()
				txt = "VA{} travelled a secondPartTravelledDistance of {}".format(self.__num, secondPartTravelledDistance)
				eventTrace(txt, simulationTrace)

				distanceTravelled = self.mission.calculateDistanceFromTrackingTravelledDistance(secondPartTravelledDistance)

				# write results concerning the task in csv file
				self.experiment.writeResult(self.getNameVA(), distanceTravelled, self.mission.getShortVersionPathMission(), task, validate, secondPartTravelledDistance)
			# save the info that the task is completed in the environment instance

			# To obtain information about a part of task finished
			txt = "VA{} travelled a distance of {}".format(self.__num, self.mission.calculateTravelledDistance())
			eventTrace(txt, simulationTrace)
			txt = "VA{} followed the path : {}".format(self.__num, self.mission.getTrackingTravelledDistance())
			eventTrace(txt, simulationTrace)


	def getCoord(self):
		"""getCoord(self) -> tuple(float): Return the coordinates of the VA."""
		return self.__coord

	def getParking(self):
		"""getParking(self) -> tuple(float): Return the parking position of the VA."""
		return self.__parking

	def getAvailable(self):
		"""getAvailable(self) -> bool: True if the VA is available, False otherwise."""
		return self.available
	
	def setAvailable(self, value):
		"""
		Set the availability status of the VA, and update the VA  data labels in the interface.

		Args: 
			value (bool): The availability status of the VA.
		"""
		self.available = value
		self.interface.updateVADataLabels(self.getNameVA(), "available", value)
	
	def setStatus(self, value):
		"""
		Set the status of the VA, and update the VA data labels in the interface.

    	Args:
    		value (int): The status of the VA.
		"""
		self.__status = value

		if value == STOP:
			self.interface.updateVADataLabels(self.getNameVA(), "status", "stop")
		elif value == CIRCULATES:
			self.interface.updateVADataLabels(self.getNameVA(), "status", "circulates")
		elif value == DECELERATE:
			self.interface.updateVADataLabels(self.getNameVA(), "status", "decelerate")
		elif value == ACCELERATE :
			self.interface.updateVADataLabels(self.getNameVA(), "status", "accelerate")
		elif value == FAILURE:
			self.interface.updateVADataLabels(self.getNameVA(), "status", "failure")
			txt = "VA{} failure : {}".format(self.__num, self.getCurrentSegment())
			eventTrace(txt, simulationTrace)
		elif value == TERMINATE:
			self.interface.updateVADataLabels(self.getNameVA(), "status", "terminate")
		else :
			txt = "VA status unknown"
			eventTrace(txt, simulationTrace) 

	def getMissionAssigned(self):
		"""getMissionAssigned(self) -> int: Return the mission assigned to the VA."""
		return self.__missionAssigned

	def setMissionAssigned(self, value):
		"""
		Set the mission assigned to the VA, and update the VA data labels.

    	Args:
    		value (int): The mission assigned to the VA.
		"""
		
		self.__missionAssigned = value
		if value == False:
			self.mission = None
		else :
			self.updateMission()
			# update the interface for missionAssigned and shortVersionPathMission for this VA
			self.interface.updateVADataLabels(self.getNameVA(), "missionAssigned", value)
			self.interface.updateVADataLabels(self.va_name, "shortVersionPathMission", self.mission.getShortVersionPathMission())

	def getMission(self):
		"""getMission(self) -> Mission: Return the mission of the VA."""
		return self.mission

	def setMission(self, m):
		"""Set the current mission for the VA.

    	Args:
        	m (Mission): Mission object to be set as the current mission.
		"""	
		self.mission = m

	def getPendingMissions(self):
		"""getPendingMissions(self) -> (list[Mission]) Return a list of Mission objects representing the pending missions."""
		return self.pendingMissions
	
	def addPendingMissions(self, mission):
		"""Add a mission to the list of pending missions.

    	Args:
        	mission (Mission): Mission object to be added to the list of pending missions.
		"""
		self.pendingMissions.append(mission)
		
		# to update the interface with all pending missions
		shortPendingMissionsDisplay = []
		for m in self.pendingMissions:
			text = str(m.getShortVersionPathMission()) + "- part: " +  str(m.getCurrentPartMission())
			shortPendingMissionsDisplay.append(text)
		
		self.interface.updateVADataLabels(self.getNameVA(), "pendingMissions", str(shortPendingMissionsDisplay))
	
	def removePendingMissions(self, mission):
		"""
		Remove a mission from the list of pending missions.

    	Args:
        	mission (Mission): Mission object to be removed from the list of pending missions.
		"""
		self.pendingMissions.remove(mission)	

		# to update the interface with all pending missions
		shortPendingMissionsDisplay = []
		for m in self.pendingMissions:
			shortPendingMissionsDisplay.append(m.getShortVersionPathMission())
		if shortPendingMissionsDisplay == []:
			shortPendingMissionsDisplay = ""
		self.interface.updateVADataLabels(self.getNameVA(), "pendingMissions", str(shortPendingMissionsDisplay))
	
	def addObstacleToMemory(self, type_obstacle, pos_obstacle):
		"""
		Add an obstacle's information to the memory: self.memoryObstacles attribute

		:param type_obstacle (str): A string representing the type of obstacle.
		:param pos_obstacle (list): A list [x, y] representing the position of the obstacle.
		"""
		self.memoryObstacles.append([type_obstacle, pos_obstacle])
	
	def removeObstacleFromMemory(self, pos_obstacle):
		"""
		Remove an obstacle's information from the memory (self.memoryObstacles attribute) based on its position.

		:param pos_obstacle (list): A list [x, y] representing the position of the obstacle to remove.
		"""
		for info_obstacle in self.memoryObstacles:
			if pos_obstacle == info_obstacle[1]:
				type_obstacle = info_obstacle[0]
				self.memoryObstacles.remove([type_obstacle, pos_obstacle])
				break

	def getCurrentNode(self):
		"""getCurrentNode(self) -> (str) Return the string representing the current node of the VA."""
		# corresponds to the last node validate
		return self.__segment[0]

	def getCurrentSegment(self):
		"""getCurrentSegment(self) -> (list[str]) Return a list of two strings representing the current segment of the VA.
    	"""
		return self.__segment

	def setCurrentSegment(self, newSegment):
		"""Set the current segment of the VA.

    	Args:
        	newSegment (list[str]): A list of two strings representing the new segment to be set.
		"""
		self.__segment = newSegment
		self.interface.updateVADataLabels(self.getNameVA(), "currentSegment", newSegment)

	def getMissionCompleted(self):
		"""getMissionCompleted(self) -> (boolean) Return if the current mission has been completed or not by the VA."""
		return self.missionCompleted

	def setMissionCompleted(self, value):
		"""
		Set whether the current mission has been completed by the VA.

    	Args:
        	value (boolean): A boolean value representing whether the current mission has been completed by the VA.
		"""
		self.missionCompleted = value
	
	def updateMission(self):
		"""Update the interface with the current mission information."""
		if self.mission != None:
			self.interface.updateVADataLabels(self.getNameVA(), "targetValidated", self.mission.getTargetValidated())
			self.interface.updateVADataLabels(self.getNameVA(), "targetNode", self.mission.getTargetPoint())
			self.interface.updateVADataLabels(self.getNameVA(), "nbCycles", self.mission.getNbCycles())
			self.interface.updateVADataLabels(self.getNameVA(), "missionOnGoingPart",self.mission.getCurrentPartMission())
	
	def updateSupervisionTask(self, label, content):
		"""
		Update the task status and labels on the interface based on the current state of the mission.

		Args:
			label (str): The label to update, can be one of ["status", "validated"].
			content (str): The new content for the given label.
		"""
		if label == "status" and content == "in progress": 
			if self.mission.getCurrentPartTask() == self.mission.getTask().getFirstTask():
				task_id = self.interface.find_task_id_by_name(self.mission.getCurrentPartTask())
				self.interface.updateTaskDataLabels(task_id, "status", "in progress")
				task_id = self.interface.find_task_id_by_name(self.mission.getTask().getSecondTask())
				self.interface.updateTaskDataLabels(task_id, "status", "attribute")
			else :
				task_id = self.interface.find_task_id_by_name(self.mission.getTask().getSecondTask())
				self.interface.updateTaskDataLabels(task_id, "status", "in progress")

		elif label == "status" and content == "pending": 
			mission = self.pendingMissions[-1] # last task adding
			print(mission)
			if self.mission.getTargetValidated():
				task_id = self.interface.find_task_id_by_name(mission.getTask().getSecondTask())
				self.interface.updateTaskDataLabels(task_id, "status", "pending")
			else :
				task_id = self.interface.find_task_id_by_name(mission.getTask().getFirstTask())
				self.interface.updateTaskDataLabels(task_id, "status", "pending")
		elif label == "status" and content == "blocked":
			task_id = self.interface.find_task_id_by_name(self.mission.getCurrentPartTask())
			self.interface.updateTaskDataLabels(task_id, "status", "blocked")

		elif label == "validated":
			task_id = self.interface.find_task_id_by_name(self.mission.getCurrentPartTask())
			self.interface.updateTaskDataLabels(task_id, "validated", str(content))
			self.interface.updateTaskDataLabels(task_id, "status", "terminate")

		else : 
			txt = "Label supervision task unknown: {}".format(label)
			eventTrace(txt, simulationTrace)
	
	def treatTAG(self, _infoTag, _msgSend):
		"""
		Treat a TAG received by the VA. This function updates the intersection state of the vehicle agent and 
		sends an MCM message to the vehicle agents within the intersection area.

		Args:
			_infoTag (list): A list of integers representing the information contained in the received TAG.
			_msgSend (bool): A boolean representing whether a message has already been sent or not.
			
		Returns:
			bool: A boolean representing whether a message has been sent or not.
		"""
		source, dest = self.__segment[0], self.__segment[1]
		n, x, y, t = _infoTag[0], _infoTag[1], _infoTag[2], _infoTag[3]
		msgSend = _msgSend
		# Is an intersectional TAG need to be treated?
		if (t == 2 and self.__intersection == OUT):			# Entering in an intersection
			txt = str(self.__num) + " arrives at an intersection area " + str(dest)
			eventTrace(txt, TAGtrace)
			while self.__freePassage[dest] != -1:			#Attente que l'intersection soit libre
				time.sleep(0.5)
				
			self.__intersection = IN
			if msgSend == False:	# to avoid sending the message n times
				msg = MCM(self.agent.getIdentity(), dest, self.__intersection)  # 1 to ask the intention to cross an intersection
				self.env.broadcastMsgToVAs(self.agent.getIdentity(), msg)
				msgSend = True
			txt = str(self.__num) + " is entering an intersection " + str(dest)
			eventTrace(txt, TAGtrace)
		elif (t == 2 and self.__intersection == IN):		# in the middle of an intersection
			txt = str(self.__num) + " in in the intersection " + str(dest)
			eventTrace(txt, TAGtrace)
		elif (t == 1 and self.__intersection == IN):		# in the middle of an intersection
			txt = str(self.__num) + " is in the intersection " + str(dest)
			eventTrace(txt, TAGtrace)
			self.__intersection = CENTER
		elif (t == 2 and self.__intersection == CENTER):	# exit of an intersection
			self.__intersection = OUT
			if msgSend == False:	# to avoid sending the message n times
				oldDest = source	# attention the segment has just changed with the center TAG of the intersection
				msg = MCM(self.agent.getIdentity(), oldDest, self.__intersection)
				self.env.broadcastMsgToVAs(self.agent.getIdentity(), msg)
				msgSend = True
			txt = str(self.__num) + " is leaving the intersection " + str(dest)
			eventTrace(txt, TAGtrace)
		else :			# for other cases
			pass
		return msgSend
	
	def moveToDestination(self, _speed):
		"""
    	Move the VA towards its destination while handling different types of obstacles and reading tags.

    	Args:
        	_speed (float): The speed of the VA.
		"""
		source, dest = self.__segment[0], self.__segment[1]
		msgSend = False
		speed = _speed
		direction = self.interface.circuit.determineDirection(self.__segment)
		
		vitesseSegment = self.interface.circuit.getSegmentSpeed(source, dest)
		infoTagOLD = [-1, -1, -1, -1]
		while (self.interface.circuit.arriveToDestination(self.__segment, self.__coord) == False and self.__status != STOP and self.__status != FAILURE):
			# check if the simulation is running
			self.mutex.lock()
			if not self._running:
				self.cond.wait(self.mutex)
			self.mutex.unlock()

			# VA circulates
			self.circulate(speed)

			if self.__tagReaderStatus == IN:
				# The VA checks whether there is a TAG nearbys
				infoTag = self.infrastructure.readingTags(self.__num, self.__coord, infoTagOLD)
				if infoTag[0] != -1:
					msgSend = self.treatTAG(infoTag, msgSend)
				else:
					pass
			else:
				pass
			if self.__radarStatus == IN:
				# The VA checks if there is an obstacle nearby
				[type_obstacle, xObstacle, yObstacle, distance] = self.radar.detection(direction, self.__coord)
				# for the moment we are only concerned with the speed to be adapted
				if type_obstacle == 1:
					speed = self.adjustSpeed(type_obstacle, speed, vitesseSegment)
					#self.env.map.displayStatusMap()
				elif type_obstacle == 2 or type_obstacle == 3:	# a static obstacle of size 2 or 3 (obstructs the alley)
					txt= "VA{} -> Obstacle ({}) at distance {} with position [{}, {}]".format(self.__num, type_obstacle, distance, xObstacle, yObstacle)
					eventTrace(txt, obstacleTrace)
					msg = CPM(self.agent.getIdentity(), [xObstacle, yObstacle], type_obstacle)
					self.env.broadcastMsgToVAs(self.agent.getIdentity(), msg)
				elif type_obstacle == 4:		# a static obstacle of size 4 (obstructs the alley)
					txt= "VA{} -> Obstacle ({}) at distance = {} with position [{}, {}]".format(self.__num, type_obstacle, distance, xObstacle, yObstacle)
					eventTrace(txt, obstacleTrace)

					# strategy : path planning + communication CPM
					pathObstacle = self.mission.obstacleOnPath([xObstacle, yObstacle])
					if (pathObstacle != None):
						newSegment = self.mission.recalculatePath(self.getCurrentNode(), pathObstacle)
						self.setCurrentSegment(newSegment)
						txt= "VA{} -> current segment {}]".format(self.__num, newSegment)
						eventTrace(txt, obstacleTrace)
					msg = CPM(self.agent.getIdentity(), [xObstacle, yObstacle], type_obstacle)
					self.env.broadcastMsgToVAs(self.agent.getIdentity(), msg)
						
				elif type_obstacle == 5:		# human obstacle
					txt= "VA{} -> Human at distance {}".format(self.__num, distance)
					eventTrace(txt, obstacleTrace)
					msg =CPM(self.agent.getIdentity(), [xObstacle, yObstacle], type_obstacle)
					self.env.broadcastMsgToVAs(self.agent.getIdentity(), msg)
					self.stopMoving()
				else:
					if speed != vitesseSegment : self.adjustSpeed(type_obstacle, speed, vitesseSegment)
				
				# add the obstacle's information in memory
				self.addObstacleToMemory(type_obstacle, [xObstacle, yObstacle])
			else:
				pass
			infoTagOLD = infoTag
		if self.mission == None : 
			#Case: task's mission is blocked, no mission attributed for the moment
			#TODO: attribute a mission to come back to an available parking place ?
			return
		if (self.__segment[1] == self.mission.getTargetPoint()) :
			#simulates stopping at the mission point
			#go = random.randint(0,10)
			go = 3
			time.sleep(go)

	def adjustSpeed(self, _obstacle, _speed, _vitesseSegment):
		"""
		Adjust the VA's speed based on the presence of an obstacle and the current speed.

		Args:
			_obstacle (int): The distance to the closest obstacle ahead of the VA.
			_speed (int): The current speed of the VA.
			_vitesseSegment (int): The maximum speed allowed on the current segment of the track.

		Returns:
			speed (int): The updated speed of the VA after adjusting for the obstacle and segment speed limits.
		"""
		speed = _speed
		if _obstacle > 0 and speed > 0: speed-=1
		elif _obstacle == 0 and speed < _vitesseSegment : speed+=1
		else : pass
		return speed
	
	#TODO : Dans la classe Mission ???? en tout cas plus utile ici
	def possibleDestination(self, segment):
		"""
		Determine the possible destinations the VA can move to from its current segment.

		Args:
			segment (list): A list representing the current segment of the track.

		Returns:
			list[int]: A list of integers representing the possible destinations the vehicle can move to from the current segment.
		"""
		position = segment[1]
		oldPosition = segment[0]
		voisins = []
		for i in range(len(self.plan)):
			if self.plan[position][i] == 1 and i < 20:
				voisins.append(i)
			elif self.plan[position][i] == 1 and i == self.__parking:
				voisins.append(i)
			else :
				pass
		return voisins
	
	#TODO : Dans la classe Mission ???? en tout cas plus utile ici
	def findNewDestinationAlea(self):
		"""
		Randomly selects a new destination for the vehicle to move to.

		Returns:
			list[int]: A list of two integers representing the old and new destinations of the vehicle.
		"""
		destinations = self.possibleDestination(self.__segment)
		oldDestination = self.__segment[1]
		nbChoix = 3		#pour 3 fois moins de retour en arrière
		newDestination = random.choice(destinations)
		nbChoix = nbChoix-1
		while newDestination == self.__segment[0] and nbChoix != 0:
			newDestination = random.choice(destinations)
			nbChoix = nbChoix-1	
		return [oldDestination, newDestination]

	def getNum(self):
		"""getNum(self) -> int: The unique identifier number of the VA."""
		return self.__num
	
	def getBidMissions(self):
		"""getBidMissions(self) -> list: A list of BidMission objects associated with the vehicle."""
		return self.bidsMissions

	def setBidMissions(self, numVa, bids):
		"""
		Set the bids for the given VA number.

		Args:
			numVa (int): The number of the VA.
			bids (list): The bids for the VA.
		"""
		nameVA = "VA" + str(numVa)
		self.bidsMissions[nameVA] = bids

	def receive(self, _sender_identity, _msg):
		"""
		Receive a message from the given sender and process it.

		Args:
			_sender_identity (dict): The identity of the sender.
			_msg (Message): The message to receive.

		Returns:
			str: The type of message received.
		"""
		msgT, msgV =  _msg.getTypeMsg(), _msg.getValueMsg()
		sender_identity = _sender_identity
		txt= "{} -> {} : message ({}, {})".format(sender_identity,self.agent.getIdentity(),msgT,msgV)
		eventTrace(txt, messageTrace)
		self.processMsg(_msg, _sender_identity)
		return msgT

	def send(self, _e, _r, _msg):
		"""
		Send a message to a receiving agent and returns the response received.

		Args:
			_e (str): The identity of the sending agent.
			_r (object): The receiving agent.
			_msg (str): The message to be sent.

		Returns:
			The response received from the receiving agent.
		"""
		txt= "VA" + str(self.__num) + " envoi une demande :" + _msg
		eventTrace(txt, messageTrace)
		reponse = _r.receive(_r,_e,_msg)
		return reponse

	def sendMsgToSpecificVA(self, num_VA, _msg):
		"""
		Send a message to a specific VA identified by its number.

		Args:
			num_VA (int): The number of the VA to which the message is being sent.
			_msg (str): The message to be sent.
   	 	"""
		# send a message to specific VA 
		# num_VA-1 : because index on __tabVa begins at 0, and numerotation of VA begins at 1
		tabVA = self.env.getTabVA()
		dest = tabVA[num_VA-1] 
		msg = dest.receive(self.agent.getIdentity(), _msg)
		txt = "Message {} from {} send to {}".format(msg, self.getNameVA(),tabVA[num_VA-1].getNameVA())
		eventTrace(txt, messageTrace)

	def sendMsgToSupervisor(self, _msg): 
		"""
		Send a message to the Supervisor agent.

		Args:
			_msg (str): The message to be sent.
		"""
		msg = self.env.supervisor.receive(self.agent.getIdentity(), _msg)
		txt = "Message {} from {} send to the Supervisor".format(_msg,self.getNameVA())
		eventTrace(txt, msg)

	def receiveTasksFromSupervisor(self, ctm_msg):
		"""
		Receive a task message from the Supervisor agent and sends a cooperation feedback message.

		Args:
			ctm_msg (CTM): The task message received.

		Returns:
			crm_msg (CRM): The cooperation feedback message sent.
		"""
		#txt = "I am {} and I receive message {} \n".format(self.getNameVA(), ctm_msg)
		#eventTrace(txt, missionTrace)
		#TODO: send Cooperation Feedback Message
		response = True # Positive feedback
		crm_msg = CRM(self.agent.getIdentity(), ctm_msg.getIdentitySender(), ctm_msg, response)
		return crm_msg 


	def clusteredTasksInMissions(self, _tasks):
		"""
		Cluster tasks together to form missions, based on the shared nodes of the tasks.

		Args:
			_tasks (list): The list of tasks to be clustered.

		Returns:
			misisons (list[Mission]): The list of missions formed by clustering the tasks.
		"""
		# Associate 2 tasks together if they have a node in common
		#txt = "I am VA{} and I clusterised tasks {}".format(self.getNum(), _tasks)
		#eventTrace(txt, missionTrace)
		missions = []
		tasks_to_remove = []
		for task in _tasks : 
			if task in tasks_to_remove:
				continue
			source_node, destination_node = task[0], task[1]
			for second_task in _tasks:
				if task in tasks_to_remove:
					break
				if second_task in tasks_to_remove:
					continue

				if source_node == second_task[1] and source_node in self.interface.circuit.getTargetsNode():
					mission = [second_task[0], source_node, destination_node]
					missions.append(mission)
					tasks_to_remove.append(task)
					tasks_to_remove.append(second_task)
				elif destination_node == second_task[0] and destination_node in self.interface.circuit.getTargetsNode():
					mission = [source_node, destination_node, second_task[1]]
					missions.append(mission)
					tasks_to_remove.append(task)
					tasks_to_remove.append(second_task)
				else : 
					# this 2 list of tasks are not associated to create a mission 
					pass
		return missions
		
	def bidOnSingleTask(self, task, id_mission, part_mission = 0):
		"""
		Place a bid on a single task, which is part of a mission.

		Args:
			task (Task): The task being bid on.
			id_mission (int): The ID of the mission to which the task belongs.
			part_mission (int): The part of the mission that has already been accomplished.

		Returns:
			dict: A bid on the given task, containing the short version path of the mission, distance to the destination, the task path, and the percentage of the task that has already been accomplished.
		"""
		currentMission = self.getMission()
		bid = {}
		bid[id_mission] = {}
		if self.getMission() != None:
			# the VA is on move, so it is not on a parking node
			m = VA_Mission(self.interface.circuit, self.getNum(), currentMission.getEndPoint(), task, part_mission)
		else : 
			# the VA has no mission yet assigned, so it is on a parking node
			m = VA_Mission(self.interface.circuit, self.getNum(), self.getParking(), task, part_mission)

		bid[id_mission]["shortVersionPathMission"] = m.getShortVersionPathMission() # before mission
		bid[id_mission]["distance"]= m.getTotalDistancePath()
		bid[id_mission]["task"] = task.getPath()
		bid[id_mission]["partTaskAccomplished"] = part_mission
		return bid

		
	def bidOnMissions(self, missions):
		"""
		Place bids on a list of missions.
	    
		Args:
			missions (list): The list of missions being bid on.

		Returns:
			dict: A dictionary of bids on each mission, containing the short version path of the mission, distance to the destination, the task path, and the part of the task's mission that has already been accomplished.
		"""
		bids = {}
		for i, mission in enumerate(missions) : 
			task = Task(mission)
			bid_mission = self.bidOnSingleTask(task, id_mission=i)
			bids[i] = {}
			bids[i] = bid_mission[i]
		return bids
	
	def allocMissionsToVA(self):
		"""
		Allocate missions to the VAs based on their bids.

		Returns:
			(dict): A dictionary containing the allocated missions, with the ID of the mission as the key and the VA assigned to the mission, the short version path of the mission, distance to the destination, task path, and the part of the task's mission that has already been accomplished as the values.
		"""
		# Allocation of missions
		#TODO: Find an algorithm optimized
		missions_allocated = {}
		va_assigned = [] # list of VA already assigned to a mission

		for va in self.bidsMissions: 
			for i in self.bidsMissions[va]: # i represents id of mission 
				if va in va_assigned:
					break
				if i not in missions_allocated.keys():
					missions_allocated[i] = {}
					missions_allocated[i]['distance'] = 1000
					missions_allocated[i]['task'] = self.bidsMissions[va][i]['task']
					missions_allocated[i]['partTaskAccomplished']= self.bidsMissions[va][i]['partTaskAccomplished']
					
				va_distance = self.bidsMissions[va][i]['distance'] 
				if va_distance < missions_allocated[i]['distance'] : 
					missions_allocated[i]['distance'] = va_distance
					missions_allocated[i]['shortVersionPathMission'] = self.bidsMissions[va][i]['shortVersionPathMission']
					missions_allocated[i]['num_va'] = int(re.findall(r'\d+', va)[0]) # to extract just the number id of the VA
		
		return missions_allocated

	def sendMissionsAllocated(self, missions_allocated): 
		"""
		Sends the missions allocated to other VAs.
		
		Args:
			missions_allocated (list[Mission]): list of dictionaries representing missions that will be allocate to VA. Each dictionary contains the following keys: 
			- num_va: an integer representing the number of the VA to which the mission is assigned
			- task: a dictionary representing the task assigned to the VA. The dictionary has the following keys:
				- id: a string representing the id of the task
				- priority: an integer representing the priority of the task
				- loc_start: a tuple representing the start location of the task
				- loc_end: a tuple representing the end location of the task
				- distance: a float representing the distance of the task
				- list_loc: a list of tuples representing the locations that should be visited during the task
			- partTaskAccomplished: a float representing the percentage of the task already accomplished by the VA
        """
		for i in range(len(missions_allocated)): 
			ctm_msg = CTM(self.agent.getIdentity(), True, True, "tasks", missions_allocated[i]['task'],missions_allocated[i]['partTaskAccomplished'] )
			self.sendMsgToSpecificVA(missions_allocated[i]['num_va'], ctm_msg)
			txt = 'Message CTM from {} to VA{} : {} with {} part task accomplished'.format(self.getNameVA(), missions_allocated[i]['num_va'], missions_allocated[i]['shortVersionPathMission'], missions_allocated[i]['partTaskAccomplished'])
			eventTrace(txt, missionTrace)
	
	def getBlockedMission(self):
		"""getBlockedMission(self) -> list(Mission): Return the list of VA's missions that are currently blocked."""
		return self.blockedMissions

	def addBlockedMission(self, mission):
		"""
		Append the given mission to the list of blocked missions for this VA instance.
		Append tasks blocked to the list of blocked tasks for the Environment instance.

		Args:
			mission (Mission): The Mission object to be added to the blocked missions list.
		"""
		self.blockedMissions.append(mission)

		second_task = self.mission.getTask().getSecondTask()
		if self.mission.getCurrentPartMission() != 2: # part 0 or 1 of the mission
			first_task = self.mission.getTask().getFirstTask() 
			self.env.addBlockedTask(first_task)

		self.env.addBlockedTask(second_task)

	def removeBlockedMission(self, mission):
		"""
		Remove the given mission to the list of blocked missions for this VA instance.

		Args:
			mission (Mission): The Mission object to be remove to the blocked missions list.
		""" 
		self.blockedMissions.remove(mission)

		second_task = mission.getTask().getSecondTask()
		if mission.getCurrentPartMission() != 2: # part 0 or 1 of the mission
			first_task = mission.getTask().getFirstTask() 
			self.env.removeBlockedTask(first_task)

		self.env.removeBlockedTask(second_task)

	def takeNextMission(self):
		"""
		Take the next pending mission from the VA instance's list of pending missions, sets it as the current mission. Update the supervision task status to 'in progress'. Set the mission assigned flag to True, and return a string representation of the mission path.
		"""
		next_mission = self.getPendingMissions()[0]
		self.removePendingMissions(next_mission)
		self.setMission(next_mission)
		self.updateSupervisionTask("status", "in progress")
		self.setMissionAssigned(True)
		txt = "VA{} : path of the mission = {}".format(self.__num, self.mission.getPath())
		eventTrace(txt, missionTrace)
		
	def processMsg(self, _msg, _sender):
		"""
		Processes the messages received by the VA.
		
		Args:
			_msg (Message): Message received by the VA
			_sender (dict): Agent identity that sent the message
		"""

		type_msg = _msg.getTypeMsg() #type of message

		if type_msg == "MCM": 
			if _msg.getEntryOrExitValue() == "entry": # Message of a VA wanting to cross an intersection
				num_intersection = _msg.getNumIntersection()
				self.__freePassage[num_intersection] = _sender.getNum()
			else:	# #Message sortie intersection
				for i in range(len(self.__freePassage)):
					if self.__freePassage[i] == _sender.getNum():
						self.__freePassage[i] = -1
		
		elif type_msg == "CPM": #Message to warn about an obstacle and a mission is underway
			size = int(_msg.getSizeObstacle())
			pos_obstacle =_msg.getPosObstacle()
			node_obstacle = self.interface.circuit.getNodeOfPosition(pos_obstacle) # value: None if the obstacle is not on a edge of the graph, not precisely on a node
			
			txt = "VA{} received {} : obstacle with size {} at position {} - node obstacle : {}".format(self.getNum(), type_msg, size, pos_obstacle, node_obstacle)
			eventTrace(txt, messageTrace)
			
			# add the obstacle to the memory
			if size > 0 and self.mission != None: 
				self.addObstacleToMemory(size, pos_obstacle)
				
				# check if the position of the obstacle corresponds to the target node of the mission or parking node of the mission 
				if node_obstacle == self.mission.getTargetPoint() or node_obstacle == self.mission.getEndPoint():
						
					#TODO: Deactivate mission: task blocked 
					self.mission.deactivateGraphNode(node_obstacle)
					self.updateSupervisionTask("status", "blocked")
					txt = "Current task's mission : {} are blocked, because of the obstacle in position: {}".format(self.mission.getCurrentPartTask(), pos_obstacle)
					eventTrace(txt, missionTrace)

					#TODO: if no worker in the scenario to unblock the situation
					if self.env.getNbWorker()==0:
						self.notValidateTask()
					
					self.addBlockedMission(self.mission)
					# The VA takes the next mission it has in hold 
					if self.getPendingMissions() != []:
							self.takeNextMission()
					else : 
						self.setMissionAssigned(False)
						self.stopMoving()
						#TODO: create mission to come back to a place of parking available
						# The moveToDestination() function will process and take into account that no mission is assigned
						#txt = "No mission possible to do available. VA{} will go to the parking to recharge.".format(self.getNum())
						txt = "No mission possible to do available. VA{} stops.".format(self.getNum())
						eventTrace(txt, missionTrace)
						
						if _sender["type"] != 'Camera': # because if it is a camera, the camera already sent a message to VAs
							denm_content = {"type" : "obstacle", "position": pos_obstacle, "size" : size}
							denm_msg = DENM(self.agent.getIdentity(), denm_content)
							txt = "{} send DENM: {}\n".format(self.getNameVA(), denm_msg.getContent())
							eventTrace(txt, missionTrace)
							self.env.broadcastMsgToWorkers(self.agent.getIdentity(), denm_msg)

				else : # to check on each edge the obstacle is present and its size. If size is too important, recalculate path
					pathObstacle = self.mission.obstacleOnPath(pos_obstacle) 

					if (pathObstacle != None):
						#if (size == 1):
						if (size == 2):
							txt = "{}{} -> {} : attention, obstacle of size {} !".format(_sender['type'], _sender['num'], self.agent.getIdentity(), size)
							eventTrace(txt, obstacleTrace)
						elif (size == 4 or size == 3):
							txt = "{}{} -> {} : attention, obstacle of size {} !".format(_sender['type'], _sender['num'], self.agent.getIdentity(), size)
							eventTrace(txt, obstacleTrace)

							newSegment = self.mission.recalculatePath(self.getCurrentNode(), pathObstacle)
							# update the value and the interface
							self.setCurrentSegment(newSegment) 
						elif (size == 5):
							self.stopMoving()	#for the moment, stop the VA
						else :
							self.stopMoving()	#for the moment, stop the VA
					else:
						txt = "{}{} sent to -> {} which is not concerned for this obstacle of size {} on its current mission !".format(_sender['type'], _sender['num'], self.agent.getIdentity(), size)
						eventTrace(txt, obstacleTrace)	

			# CPM to indicate that no obstacle is present at this position
			# Use case: a worker can send a CPM to indicates that the obstacle was removed 
			else : 
				self.removeObstacleFromMemory(pos_obstacle)
				txt= "VA{} removes the obstacle at position {} from my memory".format(self.__num, pos_obstacle)
				eventTrace(txt, missionTrace)
				#TODO: change the adjency matrix of the mission

				#TODO: continue mission if it is possible
				if self.mission == None:
					if self.getBlockedMission() != []:
						#TODO: Mise aux enchères
						# pour le moment prendre la mission
						self.mission = self.getBlockedMission()[0]
						self.removeBlockedMission(self.mission)
						#TODO: changer la matrice adjacence de la mission
						self.mission.reactivateGraphNode(node_obstacle)
						#TODO: recalculate path mission with the obstacle known in the memory
						self.setMissionAssigned(True)

				elif self.mission.getTargetPoint() == node_obstacle or self.mission.getEndPoint() == node_obstacle:
						txt = "VA{} continues its mission, and recalculate the path".format(self.getNum())
						eventTrace(txt, missionTrace)
						#TODO: changer la matrice adjacence de la mission
						self.mission.reactivateGraphNode(node_obstacle)
						#TODO: recalculate path mission
				else : 
					pass
		
		elif type_msg == "CTM":
			clustered = _msg.getClustered()
			content = _msg.getContent()
			type = _msg.getType()
			assigned = _msg.getAssigned()
			partTaskAccomplished = _msg.getPartTaskAccomplished()
			if not clustered and type == "tasks": # type = "tasks"

				#TODO: Send Feedback to the supervisor because the VA is available ot be the auctioneer
				#response = True # Positive feedback
				#crm_msg = CRM(self.agent.getIdentity(), _sender, _msg, response)
				self.receiveTasksFromSupervisor(_msg)

				missions = self.clusteredTasksInMissions(content)
				txt = "{} clustered tasks and create missions {}\n".format(self.getNameVA(), missions)
				eventTrace(txt, missionTrace)

				# bid on tasks clustered if the VA is active, not in failure
				if self.agent.active:
					own_bids = self.bidOnMissions(missions)
					txt = "{} own_bids {}\n".format(self.getNameVA(), own_bids)
					eventTrace(txt, missionTrace)
					# save the bids of the VA master (choosed by the supervisor because of its 	availability) on the missions to be allocated
					self.setBidMissions(self.getNum(), own_bids)

				ctm_msg = CTM(self.agent.getIdentity(), True, False, "tasks", missions)
				txt = "{} send ctm_msg {}\n".format(self.getNameVA(), ctm_msg)
				eventTrace(txt, missionTrace)
				# send tasks clustered in missions to others VA
				self.env.broadcastMsgToVAs(self.agent.getIdentity(), ctm_msg)

			elif type == "tasks" and clustered and not assigned:
					if all(isinstance(x, list) for x in content) :
						bids = self.bidOnMissions(content)
					else : # bid on single Mission for a task shared 
						bids = self.bidOnSingleTask(content, id_mission = 0)

					txt = "{} bid on mission {} : {} to {}\n".format(self.getNameVA(), content, bids, _sender["num"])
					eventTrace(txt, missionTrace)	
					ctm_msg = CTM(self.agent.getIdentity(), True, False, "bids", bids)
					self.sendMsgToSpecificVA(_sender["num"], ctm_msg)

			
			elif type == "tasks" and clustered and assigned: 
					# content received is the task
					# mission is assigned
					VA_node_pos = self.interface.circuit.getNodeOfPosition(self.__coord)

					if self.getMissionAssigned() == False: 
						task = Task(content)
						mission = VA_Mission(self.interface.circuit, self.getNum(), VA_node_pos, task, partTaskAccomplished)
						self.setMission(mission)
						self.setMissionAssigned(True)

						# send CRM to auctioneer
						crp_msg = CRM(self.agent.getIdentity(), _sender, _msg, True)
						self.sendMsgToSpecificVA(_sender["num"], crp_msg)

					else : # recovering a shared mission
						task = Task(content)
						current_mission = self.getMission()
						mission = VA_Mission(self.interface.circuit, self.getNum(), current_mission.getEndPoint(), task, partTaskAccomplished)
						if self.getMissionAssigned() == True: 
							self.addPendingMissions(mission)
							self.updateSupervisionTask("status", "pending")
						else : 
							self.setMission(mission)

			elif type == "mission" and clustered and not assigned :
				current_task = content.getTask()
				# need to find the path mission to the old target point of the mission validated by the VA who shared the mission
				part_mission = content.getCurrentPartMission()
				#TODO : manage if the failed VA has other missions to share besides the current one
				#pending_tasks = content.getPendingMissions()
				"""
				if all(isinstance(x, list) for x in content) :
					bids = self.bidOnMissions(content)
				else : # bid on single Mission for a task shared 
				"""
				# bid on single Mission for a task shared
				bids = self.bidOnSingleTask(current_task, 0, part_mission) # current_task, id_mission, part_mission
					
				ctm_msg = CTM(self.agent.getIdentity(), True, False, "bids", bids)
				self.sendMsgToSpecificVA(_sender["num"], ctm_msg)

			else : # type = "bids"
				self.setBidMissions(_sender["num"], content) # save the bids of a VA
				if len(self.getBidMissions()) == len(self.interface.getAvailableVA()):
					missions_allocated = self.allocMissionsToVA()
					self.sendMissionsAllocated(missions_allocated)
					# Reset the bids on missions, if the VA becomes autioneer an other time
					self.bidsMissions = {} 

		elif type_msg == "CRM":
			if  _msg.getResponse() != True : 
				txt = "{} send CRM message {} with a negative feedback to the message {}".format(_sender, self.getNameVA(), _msg)
				appliTrace()
		else :
			txt = "Type of message " + str(type_msg) + " is unknown !"
			errorTrace(txt)
	
	@pyqtSlot(float)
	def circulate(self, _speed):
		"""
		Move the VA at the given speed along its current segment and emit the updated position to the interface.
 
		Args:
			_speed (float): the speed at which the VA moves, expressed as a float between 0 and 10.
		"""
		# The VA runs at speed (simulation with the sleep() method)
		time.sleep(0.5-(_speed/11))
		distance = 2
		direction = self.interface.circuit.determineDirection(self.__segment)
		coordOLD = self.__coord
		self.__coord = self.map.updateCoord(coordOLD, direction, distance)
		# update the position in the interface
		self.update_position.emit(self.__num, self.__coord)
		self.map.changeCoordAgent(coordOLD, self.__coord)			
	
	def stopThread(self):
		"""
		Stop the thread that is running the simulation.

		This function sets the `_running` flag to False, and wakes up one thread that is waiting on the `cond` condition variable. This will cause the thread to exit.

		Use case: when the user clicks on the "stop" button of the simulation.
		"""
		self.mutex.lock()
		self._running = False
		self.cond.wakeOne()
		self.mutex.unlock()

	def resume(self):
		"""
		Resume a stopped simulation.

		This function sets the `_running` flag to True, and wakes up one thread that is waiting on the `cond` condition variable. This will cause the simulation to resume.
		"""
		self.mutex.lock()
		self._running = True
		self.cond.wakeOne()
		self.mutex.unlock()
	
	def stopMoving(self):
		"""Stop the VA moving."""

		self.setStatus(STOP)
		txt = "VA{} stops !".format(self.__num)
		eventTrace(txt, missionTrace)

	def displayAttributes(self):
		"""Display the attributes of the VA to the console."""
		print("---------------------------------------")
		print("VA",self.__num)
		print("\t__parking = ", self.__parking)
		print("\t__nbCycles = ", self.mission.getNbCycles())
		print("\t__segment = ", self.__segment)
		print("\t__status = ", self.__status)
		print("\t__intersection = ", self.__intersection)
		#print("\t__passageLibre = ", self.__freePassage)
		print("\t__radarStatus = ", self.__radarStatus)
		print("\t__tagReaderStatus = ", self.__tagReaderStatus)
		print("\t__coord = ", self.__coord)
		print("\tmission.numPos = ", self.mission.getNumPos())
		print("\tmission.path = ", self.mission.getPath())
		print("\tmission.targetValidated = ", self.mission.getTargetValidate())
		print("\t__missionStatus = ", self.__missionAssigned)
		print("---------------------------------------")


	def run(self):
		"""The activity function for the VA thread. Display the VA in the interface and wait for assignment missions from the `Supervisor`."""

		self.update_listAgents.emit(self.va_name)
		# VA is available : to receive task from the supervisor, and it is not broken down
		# attribute : self.available = True at the initialization

		#self.interface.displayVA(self.getNum(), self.getCoord())
		self.stopMoving()
		
		# waiting to receive an assignment
		while(self.__missionAssigned == False):
			tempo = 0.2
			time.sleep(tempo)
		self.setStatus(CIRCULATES)

		txt = "VA{} : path of the mission = {}".format(self.__num, self.mission.getPath())
		eventTrace(txt, missionTrace)
		# random departure
		go = random.randint(0,5)
		time.sleep(go)
		
		# parking exit
		txt = "VA{} begin its mission".format(self.__num)
		eventTrace(txt, missionTrace)
		# determination of the first mission's  destination
		destination = self.mission.newDestination()

		# update the value and the interface
		self.setCurrentSegment([self.__parking, destination])
		speed = self.interface.circuit.getSpeed(self.__segment)

		# update the interface after a tempo (necessary) with the 2 task of the mission
		tempo = 0.2
		time.sleep(tempo)
		self.updateSupervisionTask("status", "in progress")

		self.moveToDestination(speed)

		while (self.__status != TERMINATE):
			while self.__status == STOP: 
				# Stop the VA : but continue to be available
				#tempo = 0.2
				#time.sleep(tempo)

				# New mission assigned by sharing, the VA has to circulate again
				if self.getMissionAssigned() == True:
					self.setStatus(CIRCULATES)
					self.updateSupervisionTask("status", "in progress")
					break

				#TERMINATE just if all other VA agents have no task to be done
				elif self.env.allTasksAreCompleted():
					self.setStatus(TERMINATE)
					self.updateMission()	
					break
				
				#elif self.env.getNbWorker()== 0 and self.env.allTasksAreCompletedOrBlocked():
				elif self.env.hasActiveWorker() == False and self.env.allTasksAreCompletedOrBlocked():
					self.setStatus(TERMINATE)
					self.updateMission()
					break
				else :
					pass

			if self.__status == TERMINATE:
				break

			if self.__status == FAILURE :
				if self.mission != None :
					txt = "{} has a failure but it can communicate. It is on segment {}".format(self.getNameVA(), self.getCurrentSegment())
					eventTrace(txt, simulationTrace)

					#TODO : stop the speed of the VA
					self.moveToDestination(0) # stop the VA 

					mission_to_share = self.mission
					ctm_msg = CTM(self.agent.getIdentity(), True, False, "mission", mission_to_share)
					txt = "mission share : {} from {}".format(mission_to_share, self.getNameVA())
					eventTrace(txt, missionTrace)

					# become the auctioneer
					self.interface.displayAuctioneerVA(self.getNum())
					# send tasks clustered in missions to others VA
					self.env.broadcastMsgToVAs(self.agent.getIdentity(), ctm_msg)

					# to save the task not validate in the experiment files if the user asked to save
					self.notValidateTask()

					#TODO: None at the moment because it can't take a pending mission, it has a failure
					self.setMissionAssigned(False)
					# Inactive because no posibility to repare the VA for the moment
					self.agent.active = False


			else :
				if destination == None : # because node of the target task is not accessible for example
					print("destination = None")
					# to save the task not validate in the experiment files if the user asked to save
					self.notValidateTask()
					#TODO: None at the moment because it can't take a pending mission, it has a failure
					self.setMissionAssigned(False)
					# Save the resume of the data
					self.stopMoving()
					continue
				 
				oldDestination = self.__segment[1]
				speed = self.interface.circuit.getSpeed(self.__segment)
				self.moveToDestination(speed)
				#1) the VA is heading towards its destination at a random speed
				txt = str(self.__num) + " : " + str(self.__segment)
				eventTrace(txt, moveTrace)

				if self.mission == None:
					# Use Case: if the VA has his task's mission blocked and no pending missions
					# In the Next iteration of the while loop, the status of the VA is "STOP" now.
					continue

				if destination == self.mission.getEndPoint() and self.mission.getCurrentPartMission() == 2:
					nbCycles = self.mission.getNbCycles()
					self.validateTask()
					# update the interface for nbCycles of this VA
					self.interface.updateVADataLabels(self.getNameVA(), "nbCycles", nbCycles)

					txt = "VA" + str(self.__num) +  " completed " + str(nbCycles) +"nb of cycles"
					eventTrace(txt, missionTrace)

					txt = "VA{} has pending missions : {}".format(self.getNum(), self.getPendingMissions())
					eventTrace(txt, missionTrace)

					if nbCycles > 1: #TODO when are we in this case ?
						# determination of the first destination of the new cycle
						destination = self.mission.newDestination()
						# update the segment value and the interface
						self.setCurrentSegment([oldDestination, destination])
						# update interface values for targetNode and nbCycles for this VA
						self.updateMission()
					
					# The VA takes the next mission it has in hold 
					elif self.getPendingMissions() != []:
						self.takeNextMission()

					else:
						# Number of cycles performed -> loop output
						# Stop the VA : but continue to be available 
						self.setMissionCompleted(True)
						self.setMissionAssigned(False)
						self.stopMoving()
						pass # skip the current iteration of while loop
						#TODO: Take the next task of the supervisor

				elif destination == self.mission.getTargetPoint(): # Target Validate

					destination = self.mission.newDestination()
					# update the segment value and the interface
					self.setCurrentSegment([oldDestination, destination])

					# update nb_tasks_validated and the interface
					if self.mission.getCurrentPartMission() == 2:
						# correspond to a mission shared where the target is already validated
						# no need to validate the first part task
						# the first part task has already be done 
						pass 
					else :
						self.validateTask()
						# last part of the mission
						self.mission.setCurrentPartMission(2)
						# update the supervisor interface for the new task
						self.updateSupervisionTask("status", "in progress")
						
					# update the mission
					self.updateMission()

				elif destination == self.mission.getStartPoint(): # Start Validate
					destination = self.mission.newDestination()
					# update the segment value and the interface
					self.setCurrentSegment([oldDestination, destination])

					# last part of the mission
					self.mission.setCurrentPartMission(1)
					# update the mission
					self.updateMission()
					
				else:
					if self.__status == FAILURE: 
						# avoid that a VA is in failure, and update the segment of its mission, while it is in failure
						pass
					else:
						destination = self.mission.newDestination()
						# update the segment value and the interface
						self.setCurrentSegment([oldDestination, destination])
		
		self.agent.terminate()
		
