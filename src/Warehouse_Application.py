#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# VA simulation application for the Warehouse circuit

from Environment import *
from Hmi import *
from PyQt6.QtWidgets import QApplication
		
class Warehouse_Application(object):
	"""
	This class will be used to create VA simulation application for the Warehouse circuit.

	Attributes:
		__MaxVA (int): The maximum number of VAs that can be created in the simulation.
		__nbVA (int): The current number of VAs created in the simulation.
    """
	__MaxVA = 10
	__nbVA = 0
	
	def __init__(self):	
		"""
		Initialize the Warehouse_Application object by creating instances of the Hmi, Supervisor, and Environment classes.
		
		Run the PyQt6 application event loop.
		"""
		self.app = QApplication([])
		
		self.interface = Hmi(self)
		self.supervisor = Supervisor(self)
		
		self.env = Environment(self)

		#print("End of the application (v1) ...")
		self.app.exec()
	
	def getMaxVA(self):
		"""getMaxVA(self) -> int: Return the maximum number of VAs that can be created in the simulation."""
		return self.__MaxVA
	
	def getNbVA(self):
		"""getNbVA(self) -> int: Return the current number of VAs created in the simulation."""
		return self.__nbVA
	
	def setNbVA(self, _nbVA):
		"""
		Set the current number of VAs created in the simulation to _nbVA.

   		:param _nbVA: number of VA.
		:type _nbVA: int
		"""
		self.__nbVA = _nbVA

	def cleanup(self):
		"""Deletes all instances created in the constructor: interface, supervisor, environment"""
		del self.interface
		del self.supervisor
		del self.env

# Main program
def main():
    appli = Warehouse_Application()

if __name__ == '__main__':
    main()