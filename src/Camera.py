from threading import Thread
from PyQt6.QtCore import QThread, QObject, pyqtSignal, pyqtSlot
from Agent import Agent
from Console import *
from Message import CPM
from Obstacle import Obstacle
import gc
import random

# Camera States
STOP, WORKING, FAILURE = 0, 1, 2

class Camera(QObject):
    """
    Camera class for the VA simulation application.

    Attributes:
        update_listAgents (pyqtSignal): A signal used to update the list of agents in the user interface.
        num_camera (int): A static variable that keeps track of the number of cameras created.
        agent (Agent): An instance of the Agent class that represents the camera.
        name_camera (str): The name of the camera.
        env (Environment): An instance of the Environment class that represents the simulation environment.
        id (int): The unique ID of the camera.
        status (str): The status of the camera (available, working, or failure).
        position (tuple): The coordinates position of the camera in the simulation environment.
        area_size (int): The area size of the camera.
        detected_obstacles (list): The obstacles positions detected by the camera.
        interface (Hmi) : The instance of the Hmi class that represents the graphical user interface.
    """

    update_listAgents = pyqtSignal(str)

    num_camera = 0 # static variable
    def __init__(self, interface, env, id, position, area_size):
        """
        Constructor: initializes a Camera object.

        Args:
            interface (Hmi): An instance of the Hmi class that represents the graphical user interface.
            env (Environment): An instance of the Environment class that represents the simulation environment.
            id (int): The unique ID of the camera.
            position (tuple): The coordinates position of the camera in the simulation environment.
            area_size (int): The area size of the camera.
        """

        Camera.num_camera +=1
        self.num_camera = int(Camera.num_camera)
        
        self.agent = Agent("Camera", self.num_camera)
        self.agent.camera = self # add a reference to the Camera in the Agent 
        super().__init__()
        self.name_camera = "Camera" + str(self.num_camera)
        self.env = env
        self.id = id
        self.status = WORKING
        self.position = position
        self.area_size = area_size

        self.detected_obstacles = []

        self.interface = interface
        #self.interface.updateListAgents(self.name_camera)

		# connect the pyqtsignal update_position to the pyqtslot updateListAgents of the interface
        self.update_listAgents.connect(self.interface.updateListAgents)

        # display in the interface
        self.interface.displayCamera(position, area_size)
        # display information about the camera in the supervising frame
        self.interface.updateCameraDataLabels(self.getNameCamera(), "status", self.getState())
        self.interface.updateCameraDataLabels(self.getNameCamera(), "position", self.getPosition())
        self.interface.updateCameraDataLabels(self.getNameCamera(), "detection", self.getDetectedObstacles())

        self.detected_obstacles = []
        #Thread.__init__(self)	# Launch of the run() function

        self.__thread = QThread()
        self.moveToThread(self.__thread)
        self.__thread.started.connect(self.run)
        self.__thread.start()

    @pyqtSlot(str)
    def add_Camera_to_list_agents(self):
        """Update listAgents to simulate failures in the interface."""
        self.update_listAgents.emit(self.name_camera)

    def getNumCamera(self):
        """getNumCamera(self) -> int: Return the number of the camera."""
        return self.num_camera

    def getNameCamera(self):
        """getNameCamera(self) -> str: Return the name of the camera."""
        return self.name_camera

    def getStatus(self):
        """getStatus(self) -> int: Return the status of the camera.
        1: WORKING
        2: FAILURE
        0: STOP"""
        return self.status

    def getState(self):
        """getState(self) -> str: Return the state of the camera.
        """
        if self.status == WORKING: return "working"
        elif self.status == FAILURE: return "failure"
        else : return "stop"
    
    def getPosition(self):
        """getPosition(self) -> list: Return the position of the camera."""
        return self.position
    
    def getAreaSize(self):
        """getAreaSize(self) -> int: Return the area size of the camera."""
        return self.area_size

    def getDetectedObstacles(self):
        """getDetectedObstacles(self) -> list: Return the list positions of the detected obstacles."""
        return self.detected_obstacles

    def run(self):
        """
        Start the Camera object, which continuously searches for obstacles within its area of coverage.
        
        If an obstacle is detected, it sends a CPM message to all visible agents with the obstacle's location.
        
        The Camera object will continue running until its status is set to FAILURE or STOP.
        """
    
        appliTrace("Camera n°" + str(self.id) + " active")
        self.update_listAgents.emit(self.name_camera)

        while(self.status != FAILURE and self.status != STOP):
            for obj in gc.get_objects():
                if isinstance(obj, Obstacle) and obj not in self.detected_obstacles:

                    if self.detectObstacle(obj.getLocSpecificObstacle()):
                        txt= "Camera n°{0} detects obstacle: {1}".format(self.id, obj.getLocSpecificObstacle())
                        #eventTrace(txt, obstacleTrace)
                        eventTrace(simulationTrace, txt)
                        # communication CPM
                        msg = CPM(self.agent.getIdentity(), obj.getLocSpecificObstacle(), 4)
                        self.env.broadcastMsgToVAs(self.agent.getIdentity(), msg)
                        self.interface.displayCamera(self.position, self.area_size, detection=True, failure=False)
                        self.interface.displayCameraDetectedObstacle(self.getNameCamera(), obj.getLocSpecificObstacle())

                        if self.env.getNbWorker() != 0: 
                            self.env.broadcastMsgToWorkers(self.agent.getIdentity(), msg)

                    self.detected_obstacles.append(obj)
        self.agent.terminate()
    
    def detectObstacle(self, pos_obstacle):
        """
        Check if the given obstacle position is within the area of coverage of the Camera object.

        Args:
            pos_obstacle (list): position (x,y) of the obstacle.

        Returns:
            True if the obstacle is within the Camera's area of coverage, False otherwise.
        """ 
        #TODO : use the Map class ?
        if pos_obstacle[0] >= self.position[0]-self.area_size and pos_obstacle[0] <= self.position[0]+ self.area_size and pos_obstacle[1] >= self.position[1]-self.area_size and pos_obstacle[1] <= self.position[1]+ self.area_size : 
            return True
        else : 
            return False
        
    def detectFakeObstacle(self):
        """
        Simulate obstacle detection by the Camera in the infrastructure.
        The Camera broadcasts a message to VAs for communication purposes (CPM).

        Returns:
        - list: The coordinates of the camera's position. The fake obstacle is on a node position. 
        (In future version) The randomly generated coordinates [x, y] where the fake obstacle is detected.
        """

        #x = int(random.uniform(self.position[0] - self.area_size, self.position[0] + self.area_size))
        #y = int(random.uniform(self.position[1] - self.area_size, self.position[1] + self.area_size))
        
        # Fake obstacle is detected at the node position: same position at the camera's position
        [x, y] = self.position[0], self.position[1] 
        txt= "Camera n°{0} detects obstacle: {1}".format(self.id, [x,y])
        #eventTrace(txt, obstacleTrace)
        eventTrace(txt, simulationTrace)
        # communication CPM
        msg = CPM(self.agent.getIdentity(), [x,y], 4)
        self.env.broadcastMsgToVAs(self.agent.getIdentity(), msg)
        self.env.broadcastMsgToWorkers(self.agent.getIdentity(), msg)
        
        return [x,y]


    def setStatus(self, value):
        """
        Set the status of the Camera object to the given value.
        
        Update the Camera's status label in the simulation interface.

        Args:
            value (str): The new status of the Camera object.
        """
        self.status = value
        # display information about the camera in the supervising frame
        self.interface.updateCameraDataLabels(self.getNameCamera(), "status", self.getState())