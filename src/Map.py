#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Map class for the VA simulation application with the Warehouse circuit

from Console import *
import numpy as np

class Map(object):
	
	def __init__(self, _appli, _width, _height):
		"""
        Constructor for Map class.

        :param _appli: The application instance to which the map belongs.
        :param _width: The width of the map.
        :param _height: The height of the map.
        """
		self.appli = _appli
		self.__line = _height
		self.__column = _width
		self.__map = self.initMap(self.__line, self.__column)

	def initMap(self, _l, _c):
		"""
        Initialize a map of zeros of the given size.

        :param _l: The number of rows in the map.
        :param _c: The number of columns in the map.
        :return: A numpy array of size (_l, _c) initialized with zeros.
        """
		m = np.zeros( (_l, _c) ) # table of zeros
		return m
	
	def getZoom(self, _l, _c, _f):
		"""
        Get a zoomed-in window of the map around a given point.

        :param _l: The row index of the point around which to zoom.
        :param _c: The column index of the point around which to zoom.
        :param _f: The zoom factor (the size of the window is 2*_f + 1).
        :return: A numpy array of size (2*_f+1, 2*_f+1) centered on (_l, _c).
        """
		dim = _f*2 + 1
		window = np.zeros((dim, dim)) 	# window of zeros
		for i in range(dim):
			for j in range(dim):
				i1, j1 = _l-_f+i, _c-_f+j
				if i1 < 0 or j1 < 0 or i1 > self.__line-1 or j1 > self.__column-1:
					window[i][j] = -1
				elif i == _f and j == _f: window[i][j] = -2
				else : window[i][j] = self.__map[i1][j1]
		return window
		
				
	def majMap(self, _l, _c, _v):
		"""
        Update the value of a cell in the map.

        :param _l: The row index of the cell to update.
        :param _c: The column index of the cell to update.
        :param _v: The new value for the cell.
        """
		self.__map[_l][_c] = _v
	
	#def changeVal(self, _lOLD, _cOLD, _lNEW, _cNEW, _val):
	#	self.majMap(_lOLD, _cOLD, 0)
	#	self.majMap(_lNEW, _cNEW, _val)

	def changeCoordAgent(self, _coordOLD, _coordNEW):
		"""
        Change the value of a cell from 1 to 0 at _coordOLD, and from 0 to 1 at _coordNEW.

        :param _coordOLD: The coordinates of the cell to set to 0.
        :param _coordNEW: The coordinates of the cell to set to 1.
        """
		lOLD, cOLD = _coordOLD[0], _coordOLD[1]
		lNEW, cNEW = _coordNEW[0], _coordNEW[1]
		self.majMap(lOLD, cOLD, 0)
		self.majMap(lNEW, cNEW, 1)
	
	def initCoordAgent(self, _coord):
		"""
        Initialize the coordinates of a VA in the map.

        :param _coord: The coordinates of the VA to initialize.
        """
		l, c = _coord[0], _coord[1]
		self.majMap(l, c, 1)
	
	def initCoordObstacle(self, _coord, _t):
		"""
        Initialize the coordinates of an obstacle in the map.

        :param _coord: The coordinates of the obstacle to initialize.
        :param _t: The value of the obstacle.
        """
		l, c = _coord[0], _coord[1]
		self.majMap(l, c, _t)

	def removeCoordObstacle(self, _coord):
		"""
        Initialize the coordinates of an obstacle in the map.

        :param _coord: The coordinates of the obstacle to initialize.
        :param _t: The value of the obstacle.
        """
		l, c = _coord[0], _coord[1]
		self.majMap(l, c, 0)
	
	def displayStatusMap(self):
		"""Display in the terminal the non-zero cells of the map in the format "(row, column) -> value"."""
		txt ="Specific points of the map:"
		eventTrace(txt, supervisorTrace)
		for i in range(self.__line):
			for j in range(self.__column):
				val = self.__map[i][j]
				if val != 0:
					txt = "({},{}) -> {}".format(i,j,val)
					eventTrace(txt, supervisorTrace)

	def updateCoord(self, coordOLD, direction, distance):
		"""
		Compute the new coordinates of the Agent based on its current coordinates, the direction in which it is moving, and the distance it is traveling.

		Args:
			coordOLD (list): a list containing the current x and y coordinates of the Agent.
			direction (int): an integer representing the direction in which the Agent is moving. 1 means bottom, 2 means top, 3 means right, 4 means left.
			distance (int): the distance the Agent is traveling.

		Returns:
			list[int]: A list containing the new x and y coordinates of the VA.
   		"""
		[x, y] = coordOLD
		if (direction == 1): # bottom
			y = y+distance
		elif (direction == 2): # top
			y = y-distance
		elif (direction == 3): # right
			x = x+distance
		elif (direction == 4): # left
			x = x-distance
		else:
			txt = "Direction" + str(direction) + " not planned !"
			eventTrace(txt, simulationTrace)
		return [x,y]
	

'''
#Tests de la classe Map
appli = []
nbLignes, nbColonnes = 20, 20
m = Map(appli, nbLignes, nbColonnes)
m.majMap(10,10,5)
m.displayStatusMap()

m1 = m.getZoom(10,10,5)
print("m1 :\n",m1)

m2 = m.getZoom(3,3,5)
print("m2 :\n",m2)

m3 = m.getZoom(18,18,5)
print("m3 :\n",m3)

m4 = m.getZoom(3,18,5)
print("m4 :\n",m4)
'''
