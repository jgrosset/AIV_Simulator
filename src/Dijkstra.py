from Task import *
import numpy as np

##############################
# Path Planning with Dijkstra
##############################
def unvisitedNodeMinimumDistance(unvisited_nodes, min_distance):
	"""
    Return the unvisited node with minimum distance from source node.

    Args:
    	unvisited_nodes (list[int]): List of unvisited nodes in the graph
    	min_distance (list[int]): List of minimum distances from source node to each node in the graph
    
    Returns: 
		res (int): Unvisited node with minimum distance from source node
    """
	minimum, res = INF, INF
	for i in range(len(unvisited_nodes)):
		node = unvisited_nodes[i]
		if min_distance[node] < minimum and min_distance[node] > 0 :
			minimum = min_distance [node]
			res = node
	return res

def listeNoeudAdj(G, _noeud):
	"""
    Return a list of adjacent nodes to the given node in the graph.

    Args:
    	G (np.ndarray): Adjacency matrix representing the graph
    	_noeud (int): The node whose adjacent nodes are to be found
    
   	Returns: 
   		adj (list[int]): List of adjacent nodes to the given node
    """
	adj =[]
	for i in range(len(G)):
		if (G[_noeud][i] > 0 and G[_noeud][i] < INF):
			adj.append(i)
	return adj

# Implementation of the Dijkstra algorithm in Python.
# Arguments are:	G : the graph's adjacency matrix
#						s : the source summit   
#   					d : the destination summit
def Dijkstra(G, s, d):
	"""
	Implementation of the Dijkstra algorithm to find the shortest path from source node s to destination node d.

	Args:
		G (np.ndarray): Adjacency matrix representing the graph
		s (int): Source node in the graph
		d (int): Destination node in the graph
		
	Returns: 
		min_distance (list[int]): List of minimum distances from source node to each node in the graph
		pred (list[int]): List of predecessor nodes in the shortest path from source node to each node in the graph
	"""
	visited_nodes = []
	unvisited_nodes = []
	min_distance = []
	pred = []
	n = np.size(G,0)
	for i in range(n):
		unvisited_nodes.append(i)
		pred.append(-1)

	for i in range(n):
		min_distance.append(G[s][i])
		if G[s][i] != INF: 
			pred[i] = s
	min_distance[s] = 0	# if G[i][i] !=0, INF for example
	visited_nodes.append(s)
	unvisited_nodes.remove(s)
	pred[s] = s
	# as long as visit_nodes does not contain all the nodes of G do:
	while(len(visited_nodes) != n):
		#current_node ← unvisited node of minimum distance
		current_node = unvisitedNodeMinimumDistance(unvisited_nodes, min_distance)
		if (current_node == INF): 
			break
		visited_nodes.append(current_node)                              
		unvisited_nodes.remove(current_node)
		'''
		# to optimise the stop when you have reached your destination
		if current_node == d:		# arrival at destination
			#quit loop
			break	#TODO: modify
		'''
		# for each arc leaving current_node do:
		adj = listeNoeudAdj(G, current_node)
		for i in range(len(adj)):
			successor = adj[i]
			arc_length = G[current_node][successor] 
			distance = min_distance[current_node] + arc_length
			if distance < min_distance[successor]:
				min_distance[successor] = distance
				pred[successor] = current_node
	return min_distance, pred

def shortestPath(s, d, p):
	"""
    Return the shortest path from source node s to destination node d using predecessor list p.

    Args:
    	s (int): Source node in the graph
    	d (int): Destination node in the graph
    	p (list[int]): List of predecessor nodes in the shortest path from source node to each node in the graph
    
    Returns: 
		path (list[int]): List of nodes representing the shortest path from source node to destination node
    """
	path = [d]
	x = d
	while x != s:
		x = p[x]
		path.insert(0, x)
	return path