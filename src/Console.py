#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Console module for the VIA simulation application
# Console seen as a simple module, not necessarily an object class to begin with

"""
Console module for the VIA simulation application.

This module provides functionality for tracing and logging various events that occur during the simulation.

Attributes:
    missionTrace (bool): Whether to enable tracing of mission-related events.
    moveTrace (bool): Whether to enable tracing of movements.
    obstacleTrace (bool): Whether to enable tracing of obstacles.
    messageTrace (bool): Whether to enable tracing of messages.
    radarTrace (bool): Whether to enable tracing of radar events.
    TAGtrace (bool): Whether to enable tracing of TAG events.
    supervisorTrace (bool): Whether to enable tracing of supervisor events.
    simulationTrace (bool): Whether to enable tracing of simulation events.
    numVATrace (int): The ID of the VA to trace.
"""

missionTrace = True
moveTrace = False
obstacleTrace = True
messageTrace =	True
radarTrace =	False
TAGtrace = False
supervisorTrace = False
simulationTrace = True

numVATrace = 2		# tracing a specific VA

def appliTrace(txt):
	"""
    Print the given text.

    Args:
        txt (str): The text to print.
    """
	print(txt)
	
def eventTrace(txt, typeTrace):
	"""
    Print the given text if typeTrace is True.

    Args:
        txt (str): The text to print.
        typeTrace (bool): Whether to print the text.
    """
	if typeTrace == True:
		print(txt)
	else:
		pass

def errorTrace(txt):
	"""
    Print the given text as an error message.

    Args:
        txt (str): The text to print.
    """
	print("\tError : ",txt)
	

