#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Hmi class for the VA simulation application with the Warehouse circuit

import re
from Console import *
from Warehouse_Circuit import *
from Agent import Agent
import random

from PyQt6.QtWidgets import QFrame, QMainWindow, QFrame, QGraphicsScene, QGraphicsView, QPushButton, QLabel, QComboBox, QMessageBox, QWidget, QGridLayout, QVBoxLayout, QInputDialog, QGraphicsRectItem, QGraphicsPolygonItem
from PyQt6.QtGui import  QColor, QPen, QFont, QPixmap, QBrush, QPolygonF
from PyQt6.QtCore import pyqtSlot, QPointF, QCoreApplication, pyqtSignal, QTimer

import time
import time

class Hmi(QMainWindow):
	"""The Hmi class is a QMainWindow that represents the Human Machine Interface of the application."""
	__MaxVA = 5
	__MaxWorker = 1
	__nbVA = 0
	__stopValue = False

	def __init__(self, _application):
		"""
		Initializes a new Hmi object.
		Sets up the main window with the different frames:
		- the scene with the circuit
		- console frame
		- supervising frame of the VA
		- supervising frame of the Cameras
		- supervising frame of the tasks given by the Supervisor

		:param _application (Warehouse_Application): the instance of the Warehouse_Application class of the application.
		"""
		
		super().__init__()

		self.circuit = Warehouse_Circuit()
		self.width, self.height = 520, 740

		print("Launch the application: ", self.circuit.getNameCircuit())
		self.appli = _application

		self.simulationEnded = pyqtSignal()

		# Set up the main window
		self.setWindowTitle(self.circuit.getNameCircuit())
		#self.setWindowTitle("Warehouse_Circuit")
		self.setGeometry(0, 100, self.width, self.height)

		# A representation of the circuit is not visible in a window at the beginning
		self.circuitWindowOpened = False

		# Drawing of the Warehouse circuit
		self.canvas = QGraphicsView(self)
		self.canvas.setGeometry(0, 0, 400, 300)
		self.canvas.setStyleSheet('background-color: grey;')

		self.scene = QGraphicsScene(self.canvas)
		self.canvas.setScene(self.scene)

		carre_bord = self.scene.addRect(10, 5, 300, 320)

		circuit_HG = self.scene.addRect(35, 15, 100, 200, pen=QPen(QColor(200, 200, 200), 10))
		circuit_HC = self.scene.addRect(135, 15, 100, 200, pen=QPen(QColor(200, 200, 200), 10))
		circuit_HD = self.scene.addRect(235, 15, 50, 200, pen=QPen(QColor(200, 200, 200), 10))
		circuit_BC = self.scene.addRect(35, 215, 250, 100, pen=QPen(QColor(173, 216, 230), 15))

		lineStorage1 = self.scene.addLine(35, 215, 35, 115, pen=QPen(QColor(173, 216, 230), 15))
		lineStorage2 = self.scene.addLine(135, 215, 135, 115, pen=QPen(QColor(173, 216, 230), 15))
		lineStorage3 = self.scene.addLine(235, 215, 235, 115, pen=QPen(QColor(173, 216, 230), 15))
		linePark1 = self.scene.addLine(60, 315, 60, 265, pen=QPen(QColor(173, 216, 230), 10))
		linePark2 = self.scene.addLine(85, 315, 85, 265, pen=QPen(QColor(173, 216, 230), 10))
		linePark3 = self.scene.addLine(110, 315, 110, 265, pen=QPen(QColor(173, 216, 230), 10))
		linePark4 = self.scene.addLine(135, 315, 135, 265, pen=QPen(QColor(173, 216, 230), 10))
		linePark5 = self.scene.addLine(160, 315, 160, 265, pen=QPen(QColor(173, 216, 230), 10))
		lineBG = self.scene.addLine(5, 315, 35, 315, pen=QPen(QColor(173, 216, 230), 15))
		lineBD = self.scene.addLine(285, 315, 315, 315, pen=QPen(QColor(173, 216, 230), 15))

		#Drawing the intersections with dotted red squares 
		loc = self.circuit.getLoc()
		loc_intersections = self.circuit.getLocIntersections()
		for point in loc_intersections:
			# Define the rectangle intersection size and location : draw it in red (255, 0, 0)
			area_size = 10
			x = loc[point][0]
			y = loc[point][1]
			self.scene.addRect(x - area_size, y - area_size, area_size*2, area_size*2, pen = QPen(QColor(0, 0, 0)))
			
		# RFID Tags
		self.tabVisuTAG = []
		nbTAG=self.circuit.getNbTAG()
		for i in range (nbTAG): 
			size = 2
			x = loc[i][0]
			y = loc[i][1]
			c = self.scene.addRect(x-size, y-size, size*2, size*2, pen = QPen(QColor(0, 0, 0)), brush = QBrush(QColor(0, 0, 0)))
			self.tabVisuTAG.append(c)
		
		# VA
		self.initVA()
		# Worker
		self.initWorkers()

		# Obstacles items of the graphicsScene
		self.listObstaclesItem = []

		# CrossLinesFailure items of the graphicsScene 
		self.crossLinesFailure = []
	
		################################################################ 
		# Console Frame with Scenario Buttons and ComboBox for obstacles
		################################################################ 
		self.consoleFrame = QFrame(self)
		self.consoleFrame.setObjectName("Console")
		self.consoleFrame.setStyleSheet("background-color: rgb(229, 229, 229);")

		# Create a label and set its text to the frame's name
		self.consoleLabel = QLabel(self.consoleFrame)
		self.consoleLabel.setText(self.consoleFrame.objectName())
		self.consoleLabel.setGeometry(5, 5, 400, 400) # Set the label's position and size
		self.consoleLabel.setStyleSheet("font-weight: bold;")
		
		# Scenario Simulation
		self.buttonStart_simulation=QPushButton("Simulation", self.consoleFrame)
		self.buttonStart_simulation.setFont(QFont('helvetica', 10))
		self.buttonStart_simulation.setStyleSheet("background-color: green;")
		self.buttonStart_simulation.clicked.connect(self.start_simulation)

		# Scenario Random Button
		self.buttonStart_random=QPushButton("Random Scenario", self.consoleFrame)
		self.buttonStart_random.setFont(QFont('helvetica', 10))
		self.buttonStart_random.setStyleSheet("background-color: green;")
		self.buttonStart_random.clicked.connect(self.start_random)
		
		# Scenario 0 Button
		self.buttonStart_0=QPushButton("Scenario 0", self.consoleFrame)
		self.buttonStart_0.setFont(QFont('helvetica', 10))
		self.buttonStart_0.setStyleSheet("background-color: green;")
		self.buttonStart_0.clicked.connect(self.start_0)
		
		# Scenario 1 Button
		self.buttonStart_1=QPushButton("Scenario 1", self.consoleFrame)
		self.buttonStart_1.setFont(QFont('helvetica', 10))
		self.buttonStart_1.setStyleSheet("background-color: green;")
		self.buttonStart_1.clicked.connect(self.start_1)
		
		# Scenario 2 Button
		self.buttonStart_2=QPushButton("Scenario 2", self.consoleFrame)
		self.buttonStart_2.setFont(QFont('helvetica', 10))
		self.buttonStart_2.setStyleSheet("background-color: green;")
		self.buttonStart_2.clicked.connect(self.start_2)

		# Scenario 3 Button
		self.buttonStart_3=QPushButton("Scenario 3", self.consoleFrame)
		self.buttonStart_3.setFont(QFont('helvetica', 10))
		self.buttonStart_3.setStyleSheet("background-color: green;")
		self.buttonStart_3.clicked.connect(self.start_3)

		# Scenario 4 Button
		self.buttonStart_4=QPushButton("Scenario 4", self.consoleFrame)
		self.buttonStart_4.setFont(QFont('helvetica', 10))
		self.buttonStart_4.setStyleSheet("background-color: green;")
		self.buttonStart_4.clicked.connect(self.start_4)

		# Stop Button
		self.buttonStop=QPushButton("STOP", self.consoleFrame)
		self.buttonStop.setFont(QFont('helvetica', 10))
		self.buttonStop.setStyleSheet("background-color: red;")
		self.buttonStop.clicked.connect(self.stop)		

		# Obstacle Part
		self.labelObstacle = QLabel("Generate obstacle", self.consoleFrame)
		self.labelObstacle.setStyleSheet("background-color: rgb(229, 229, 229);")

		self.labelSizeObstacle = QLabel("Size obstacle", self.consoleFrame)
		self.labelSizeObstacle.setStyleSheet("background-color: rgb(229, 229, 229);")

		self.obstacleSizeCombo = QComboBox(self.consoleFrame)
		self.obstacleSizeCombo.addItems(["1", "2", "3", "4"])  # Add size options here
		
		listObstacles=["", "Obstacle S1","Obstacle S2","Obstacle S3","Obstacle P1","Obstacle P2", "Obstacle P3", "Obstacle P4", "Obstacle P5", "Random Obstacle", "Human"] # "": to select nothing at the beginning 
		self.listObstaclesCombo = QComboBox(self.consoleFrame)
		self.listObstaclesCombo.addItems(listObstacles)

		# Failure Part
		self.labelSimulateFailure = QLabel("Simulate failures", self.consoleFrame)
		self.labelSimulateFailure.setStyleSheet("background-color: rgb(229, 229, 229);")
		
		self.listAgents = [""] # to do not select any agents at the beginning
		self.listAgentsCombo = QComboBox(self.consoleFrame)
		self.listAgentsCombo.addItems(self.listAgents)

		# Circuit Part
		self.circuitRepresentation = QLabel("Circuit representation", self.consoleFrame)
		self.circuitRepresentation.setStyleSheet("background-color: rgb(229, 229, 229);")
		
		self.buttonCircuit = QPushButton("Circuit model", self.consoleFrame)
		self.buttonCircuit.setFont(QFont('helvetica', 10))
		self.buttonCircuit.setStyleSheet("background-color: blue;")
		self.buttonCircuit.clicked.connect(self.openCircuitImage)

		#######################################
		# Supervising VAs Frame with all labels
		#######################################
		self.supervisingVAsFrame = QFrame(self)
		self.supervisingVAsFrame.setObjectName("Supervising VA")
		self.supervisingVAsFrame.setStyleSheet("background-color: rgb(229, 229, 229);")

		# Create a label and set its text to the frame's name
		self.supervisingVAsLabel = QLabel(self.supervisingVAsFrame)
		self.supervisingVAsLabel.setText(self.supervisingVAsFrame.objectName())
		self.supervisingVAsLabel.setStyleSheet("font-weight: bold;")

		self.va_name_label = QLabel("VA Name", parent=self.supervisingVAsFrame)
		self.available_label = QLabel("Available", parent=self.supervisingVAsFrame)
		self.pending_missions_label = QLabel("Pending Missions", parent=self.supervisingVAsFrame)
		self.nb_validated_tasks_label = QLabel("Number Tasks Validated", parent=self.supervisingVAsFrame)
		self.status_label = QLabel("Status", parent=self.supervisingVAsFrame)
		self.missionAssigned_label =QLabel("Mission Assigned", parent=self.supervisingVAsFrame)
		self.missionOnGoingPart_label =QLabel("Mission On Going Part", parent=self.supervisingVAsFrame)
		self.targetNode_label = QLabel("Target Node", parent=self.supervisingVAsFrame)
		self.targetValidated_label =QLabel("Target Validated", parent=self.supervisingVAsFrame)
		self.currentSegment_label = QLabel("Current Segment", parent=self.supervisingVAsFrame)
		self.entirePath_label = QLabel("Short Version Path Mission", parent=self.supervisingVAsFrame)
		self.nbCycle_label = QLabel("Number Mission Cycles", parent=self.supervisingVAsFrame)
		
		# Create all the Text widget corresponding for each va 
		self.va_data_labels = {}
		for i in range(len(self.tabVA)):
			va_name = "VA" + str(i+1)
			self.va_data_labels[va_name] = {}
			self.va_data_labels[va_name]["va_name"] = QLabel(self.supervisingVAsFrame)
			self.va_data_labels[va_name]["available"] = QLabel(self.supervisingVAsFrame)
			self.va_data_labels[va_name]["pendingMissions"] = QLabel(self.supervisingVAsFrame)
			self.va_data_labels[va_name]["nbValidatedTasks"] = QLabel(self.supervisingVAsFrame)
			self.va_data_labels[va_name]["status"] = QLabel(self.supervisingVAsFrame)
			self.va_data_labels[va_name]["missionAssigned"] = QLabel(self.supervisingVAsFrame)
			self.va_data_labels[va_name]["missionOnGoingPart"] = QLabel(self.supervisingVAsFrame)
			self.va_data_labels[va_name]["targetNode"] = QLabel(self.supervisingVAsFrame)
			self.va_data_labels[va_name]["targetValidated"] = QLabel(self.supervisingVAsFrame)
			self.va_data_labels[va_name]["currentSegment"] = QLabel(self.supervisingVAsFrame)
			self.va_data_labels[va_name]["shortVersionPathMission"] = QLabel(self.supervisingVAsFrame)
			self.va_data_labels[va_name]["nbCycles"] = QLabel(self.supervisingVAsFrame)

		############################################# 
		#Supervising Frame for Camera with all labels
		############################################# 
		self.supervisingCamerasFrame = QFrame(self)
		self.supervisingCamerasFrame.setObjectName("Supervising Camera")
		self.supervisingCamerasFrame.setStyleSheet("background-color: rgb(229, 229, 229);")

		# Create a label and set its text to the frame's name
		self.supervisingCamerasLabel = QLabel(self.supervisingCamerasFrame)
		self.supervisingCamerasLabel.setText(self.supervisingCamerasFrame.objectName())
		self.supervisingCamerasLabel.setStyleSheet("font-weight: bold; border-width: 3px; border-style: solid; border-color: rgb(0, 255, 0);")

		self.camera_name_label = QLabel("Camera Name", parent=self.supervisingCamerasFrame)
		self.status_camera_label = QLabel("Status", parent=self.supervisingCamerasFrame)
		self.position_camera_label = QLabel("Position", parent=self.supervisingCamerasFrame)
		self.detection_label = QLabel("Detection", parent=self.supervisingCamerasFrame)

		# Create all the Text widget corresponding for each camera
		self.camera_data_labels = {}
		self.tabCameras = self.circuit.getCameras()
		
		for i in range(len(self.tabCameras.keys())):
			camera_name = "Camera" + str(i+1)
			self.camera_data_labels[camera_name] = {}
			self.camera_data_labels[camera_name]["camera_name"] = QLabel(self.supervisingCamerasFrame)
			self.camera_data_labels[camera_name]["status"] = QLabel(self.supervisingCamerasFrame)
			self.camera_data_labels[camera_name]["position"] = QLabel(self.supervisingCamerasFrame)
			self.camera_data_labels[camera_name]["detection"] = QLabel(self.supervisingCamerasFrame)
		
		#######################################
		# Supervising Tasks Frame with all labels
		#######################################

		self.supervisingTasksFrame = QFrame(self)
		self.supervisingTasksFrame.setObjectName("Supervising Tasks")
		self.supervisingTasksFrame.setStyleSheet("background-color: rgb(229, 229, 229);")

		# Create a label and set its text to the frame's name
		self.supervisingTasksLabel = QLabel(self.supervisingTasksFrame)
		self.supervisingTasksLabel.setText(self.supervisingTasksFrame.objectName())
		self.supervisingTasksLabel.setStyleSheet("font-weight: bold;")

		self.task_label = QLabel("Task", parent=self.supervisingTasksFrame)
		self.task_id = QLabel("Task Id", parent=self.supervisingTasksFrame)
		self.validatedTask_label = QLabel("Validated Task", parent=self.supervisingTasksFrame)
		self.statusTask_label = QLabel("Status Task", parent=self.supervisingTasksFrame)
		
		# Create all the Text widget corresponding for each tasks
		
		self.tasks_data_labels = {}
		
		for i in range(len(self.tabVA)*2):
			task_id = str(i+1)
			self.tasks_data_labels[task_id] = {}
			self.tasks_data_labels[task_id]["task"] = QLabel(self.supervisingTasksFrame)
			self.tasks_data_labels[task_id]["id"] = QLabel(self.supervisingTasksFrame)
			self.tasks_data_labels[task_id]["validated"] = QLabel(self.supervisingTasksFrame)
			self.tasks_data_labels[task_id]["status"] = QLabel(self.supervisingTasksFrame)

		##############################
		# Specify color of each QLabel
		##############################
		# get a list of all widgets in the application
		all_widgets = self.appli.app.allWidgets()

		# filter the list to only include QLabel widgets
		label_widgets = [widget for widget in all_widgets if isinstance(widget, (QLabel, QComboBox))]
		self.set_labels_color(label_widgets, 'black')


		##############################################################################
		# Create a grid layout to hold the scenario buttons and ComboBox for obstacles
		##############################################################################
		layout_scenario = QGridLayout()

		# Add the scenario buttons and ComboBox to the layout
		layout_scenario.addWidget(self.consoleLabel, 0, 0)
		layout_scenario.addWidget(self.buttonStart_simulation, 1, 0)
		layout_scenario.addWidget(self.buttonStart_0, 2, 0)
		layout_scenario.addWidget(self.buttonStart_1, 3, 0)
		layout_scenario.addWidget(self.buttonStart_2, 4, 0)
		layout_scenario.addWidget(self.buttonStart_3, 5, 0)
		layout_scenario.addWidget(self.buttonStart_4, 6, 0)
		layout_scenario.addWidget(self.buttonStart_random, 7, 0)
		layout_scenario.addWidget(self.buttonStop, 1, 2)

		layout_scenario.addWidget(self.labelSizeObstacle, 1, 1)
		layout_scenario.addWidget(self.obstacleSizeCombo, 2, 1)
		layout_scenario.addWidget(self.labelObstacle, 3, 1)
		layout_scenario.addWidget(self.listObstaclesCombo, 4, 1)
		layout_scenario.addWidget(self.labelSimulateFailure, 5, 1)
		layout_scenario.addWidget(self.listAgentsCombo, 6, 1)
		layout_scenario.addWidget(self.circuitRepresentation, 3, 2)
		layout_scenario.addWidget(self.buttonCircuit,4, 2)

		# bind combobox
		self.listObstaclesCombo.currentIndexChanged.connect(self.selectPositionObstacle)
		#self.obstacleSizeCombo.currentIndexChanged.connect(self.changeSizeObstacle)
		self.listAgentsCombo.currentIndexChanged.connect(self.selectAgentFailure)

		# Set spacing between widgets
		layout_scenario.setSpacing(25)

		#####################################
		# Add widgets to layout_supervisingVA
		#####################################
		layout_supervisingVA = QGridLayout()

		layout_supervisingVA.addWidget(self.supervisingVAsLabel, 0, 0)
		layout_supervisingVA.addWidget(self.va_name_label, 1, 0)
		layout_supervisingVA.addWidget(self.available_label, 2, 0)
		layout_supervisingVA.addWidget(self.pending_missions_label, 3, 0)
		layout_supervisingVA.addWidget(self.nb_validated_tasks_label, 4, 0)
		layout_supervisingVA.addWidget(self.status_label, 5, 0)
		layout_supervisingVA.addWidget(self.missionAssigned_label, 6, 0)
		layout_supervisingVA.addWidget(self.missionOnGoingPart_label, 7, 0)
		layout_supervisingVA.addWidget(self.targetNode_label, 8, 0)
		layout_supervisingVA.addWidget(self.targetValidated_label, 9, 0)
		layout_supervisingVA.addWidget(self.currentSegment_label, 10, 0)
		layout_supervisingVA.addWidget(self.entirePath_label, 11, 0)
		layout_supervisingVA.addWidget(self.nbCycle_label, 12, 0)

		# Grid all labels for each VA corresponding to each principal labels
		for i, va_name in enumerate(self.va_data_labels):
			for j, data_label in enumerate(self.va_data_labels[va_name]): 
				# Grid all labels for this va_name 
				layout_supervisingVA.addWidget(self.va_data_labels[va_name][data_label], j+1, i+1)
				# Configure/Update labels of VA_name 
				self.updateVADataLabels(va_name, "va_name", va_name)


		##########################################
		# Add widgets to layout_supervisingCameras
		##########################################
		layout_supervisingCameras = QGridLayout()
		
		layout_supervisingCameras.addWidget(self.supervisingCamerasLabel, 0, 0)
		layout_supervisingCameras.addWidget(self.camera_name_label, 1, 0)
		layout_supervisingCameras.addWidget(self.status_camera_label, 2, 0)
		layout_supervisingCameras.addWidget(self.position_camera_label, 3, 0)
		layout_supervisingCameras.addWidget(self.detection_label, 4, 0)

		# Grid all labels for each Camera corresponding to each principal labels
		for i, camera_name in enumerate(self.camera_data_labels):
			for j, data_label in enumerate(self.camera_data_labels[camera_name]): 
				# Grid all labels for this va_name 
				layout_supervisingCameras.addWidget(self.camera_data_labels[camera_name][data_label], j+1, i+1)
				self.updateCameraDataLabels(camera_name, "camera_name", camera_name)

		##########################################
		# Add widgets to layout_supervisingTasks
		##########################################
		
		layout_supervisingTasks= QGridLayout()
		
		layout_supervisingTasks.addWidget(self.supervisingTasksLabel, 0, 0)
		layout_supervisingTasks.addWidget(self.task_label, 1, 0)
		layout_supervisingTasks.addWidget(self.task_id, 1, 1)
		layout_supervisingTasks.addWidget(self.validatedTask_label, 1, 2)
		layout_supervisingTasks.addWidget(self.statusTask_label, 1, 3)

		
		# Grid all labels for each Task corresponding to each principal labels
		time.sleep(1)
		for i, task_id in enumerate(self.tasks_data_labels):
			for j, data_label in enumerate(self.tasks_data_labels[task_id]): 
				# Grid all labels for this task_name
				layout_supervisingTasks.addWidget(self.tasks_data_labels[task_id][data_label], i+2, j)
				# Configure/Update labels of task_name 
				self.updateTaskDataLabels(task_id, "id", task_id)

		##########################################################
		# Set different Layout and grid widgets of the main Window
		##########################################################
		self.consoleFrame.setLayout(layout_scenario)
		self.supervisingVAsFrame.setLayout(layout_supervisingVA)
		self.supervisingCamerasFrame.setLayout(layout_supervisingCameras)
		self.supervisingTasksFrame.setLayout(layout_supervisingTasks)
		
		# General grid of the window
		grid_layout = QGridLayout(self)
		# Add the canvas to the layout
		grid_layout.addWidget(self.canvas, 0, 0)
		grid_layout.addWidget(self.consoleFrame, 1, 0)
		grid_layout.addWidget(self.supervisingVAsFrame,0,1)
		grid_layout.addWidget(self.supervisingCamerasFrame,1,1)
		grid_layout.addWidget(self.supervisingTasksFrame,0,2)

		# Set the layout for the main window
		main_widget = QWidget()
		main_widget.setLayout(grid_layout)
		self.setCentralWidget(main_widget)

		# show the main window off the application with layouts
		self.show()

	def show(self):
		"""Display the HMI window and show the canvas with the current scene."""
		super().show()
		self.canvas.setScene(self.scene)
		self.canvas.show()

	def set_labels_color(self, labels, color):
		"""
		Set the color of a list of QLabel objects by updating their style sheet.

		:param labels (list): A list of QLabel objects.
		:param color (str): The new color to apply to the labels, as a string.
		"""
		for label in labels:
			existing_style_sheet = label.styleSheet()
			new_style_sheet = existing_style_sheet + "color: " + str(color) + ";"
			label.setStyleSheet(new_style_sheet)

	@pyqtSlot(str)
	def updateListAgents(self, nameAgent):
		"""
    	Slot to update the list of agents displayed in the HMI.

    	:param nameAgent (str): The name of the agent to add to the list.
    	"""
		self.listAgents.append(nameAgent)
		# update listAgentsCombo values to simulate failure in the interface
		self.listAgentsCombo.addItem(nameAgent)
	
	def getVADataLabels(self):
		"""
		Get the current dictionary of visual agent (VA) data labels.

    	:return (dict): The dictionary of VA data labels, where each key is a VA name and each value is another dictionary of data labels.
    	"""
		return self.va_data_labels

	def updateVADataLabels(self,va_name, data_label, value):
		"""
		Update the value of a specific data label for a VA.

		:param va_name (str): The name of the VA to update.
		:param data_label (str): The name of the data label to update.
		:param value (str): The new value for the data label.
		"""
		self.va_data_labels[va_name][data_label].setText(str(value))

	def updateCameraDataLabels(self,camera_name, data_label, value):
		"""
		Update the value of a specific data label for a camera.

		:param camera_name (str): The name of the camera to update.
		:param data_label (str): The name of the data label to update.
		:param value (str): The new value for the data label.
		"""
		self.camera_data_labels[camera_name][data_label].setText(str(value))

	def setCameraBorderColorLabel(self, camera_name, rgb_color):
		"""
		Set the border color of the camera label widget corresponding to the camera_name.

		Parameters:
		- camera_name (str): The name of the camera label widget to modify.
		- rgb_color (str): The RGB color value in the format "rgb(x, y, z)", each in the range [0, 255].
		"""
		if rgb_color == None:
			existing_style_sheet = self.camera_data_labels[camera_name]["camera_name"].styleSheet()
			# Remove border-related properties from the existing style sheet
			style_lines = existing_style_sheet.split(';')
			filtered_style_lines = [line.strip() for line in style_lines if 'border' not in line]
			new_style_sheet = '; '.join(filtered_style_lines)
			self.camera_data_labels[camera_name]["camera_name"].setStyleSheet(new_style_sheet)
		else :
			new_border_color = rgb_color 
			existing_style_sheet = self.camera_data_labels[camera_name]["camera_name"].styleSheet()
			new_style_sheet = existing_style_sheet + f"border-width: 2px; border-style: solid; border-color: {new_border_color};"
			self.camera_data_labels[camera_name]["camera_name"].setStyleSheet(new_style_sheet)

	def updateTaskDataLabels(self,task_id, data_label, value):
		"""
		Update the value of a specific data label for a task.

		:param task_name (str): The task to update.
		:param data_label (str): The name of the data label to update.
		:param value (str): The new value for the data label.
    	"""
		try:
			self.tasks_data_labels[task_id][data_label].setText(str(value))
		except KeyError:
			print(f"Error: {task_id}-{data_label}: {value}")
			pass

	def displayInformationTasksGiven(self, tasks_given):
		"""
		Update the task data labels with information about the tasks given by the Supervisor.

		:param tasks_given (list): List of tasks given by the Supervisor.
   		"""
		# tasks_given: tasks given by the supervisor
		for i,task in enumerate(tasks_given):
			#id of task begins at 1
			id = str(i+1)
			self.updateTaskDataLabels(id, "task", task)
			self.updateTaskDataLabels(id, "validated", False)
			self.updateTaskDataLabels(id, "status", "auction")

	def find_task_id_by_name(self, task_name):
		"""
		Finds the task_id corresponding to the given task name in the `tasks_data_labels` dictionary.

		Parameters:
			task_name (str): The name of the task to search for.
		Returns:
			str: The task_id corresponding to the task name, or None if the task name is not found.
		
		"""
		for task_id, task_data in self.tasks_data_labels.items():
			if task_data["task"].text() == str(task_name):
				return task_id
		return None

	def setColorVA(self, _numVA):
		"""
		Set the color of a VA based on its number.

		:param _numVA (int): The number of the VA.
		"""
		if _numVA == 1:
			color = 'black'
		elif _numVA == 2: 
			color = 'orange'
		elif _numVA == 3: 
			color ='blue'
		elif _numVA == 4: 
			color = 'green'
		elif _numVA == 5: 
			color = 'maroon'
	
		else: print("setColorVA : Task definition error !")
		self.tabVA[_numVA-1].setPen(QPen(QColor(color)))
		self.tabVA[_numVA-1].setBrush(QColor(color))

		# to assign the color corresponding of each va_name label in the interface 
		va_name = "VA" + str(_numVA)
		self.updateColorVALabel(va_name, color)
		
	def updateColorVALabel(self, va_name, color):
		"""
		Update the color of a VA label with a given color.

		:param va_name (str): The name of the VA label to update.
		:param color (QColor): The color to set the VA label to.
		"""
		# remove the older stylesheet of va_data_labels[va_name]["va_name"]
		self.va_data_labels[va_name]["va_name"].setStyleSheet("")
		# add the new style sheet
		new_style_sheet = "font-weight: bold; color: " + str(color)
		self.va_data_labels[va_name]["va_name"].setStyleSheet(new_style_sheet)

	def displayPopUpScenario2(self):
		"""
		Display a pop-up message box to allow the user to choose between two experiment parts.

		This function creates a QMessageBox pop-up with two buttons, allowing the user to select
		either the "First Part" or the "Second Part" of the experiment in the Scenario 2. It waits for the user's choice and returns the selected part as an integer value.

		Returns:
			int: An integer representing the selected experiment part.
				- 1 for "First Part"
				- 2 for "Second Part"
		"""
		msg_box = QMessageBox(self)
		msg_box.setWindowTitle("Scenario 2 - Settings")
		msg_box.setText("Choose the experiment part of the mission where the VA2 will have a failure:")
		first_button = msg_box.addButton("First Part", QMessageBox.ButtonRole.YesRole)
		second_button = msg_box.addButton("Second Part", QMessageBox.ButtonRole.NoRole)

		# Show the message box and wait for a response
		msg_box.exec()

		# Get the user's choice
		settings = 1 if msg_box.clickedButton() == first_button else 2
		return settings

	def displayPopUpWorkerOrNot(self, scenario):
		"""
		Display a pop-up message box to allow the user to choose between two experiment settings.

		This method creates a QMessageBox with options for the user to activate the availability of one worker or not.
		It presents two buttons, "Yes" and "No," for the user to select their choice.

		Returns:
			bool: True if the user selects "Yes" indicating the activation of one worker; False if the user selects "No"
		"""
		msg_box = QMessageBox(self)
		msg_box.setWindowTitle(f"Scenario {scenario} - Settings")
		msg_box.setText("Activate availability of one worker")
		yes_button = msg_box.addButton("Yes", QMessageBox.ButtonRole.YesRole)
		no_button = msg_box.addButton("No", QMessageBox.ButtonRole.NoRole)

		# Show the message box and wait for a response
		msg_box.exec()

		# Get the user's choice
		settings = True if msg_box.clickedButton() == yes_button else False
		return settings


	def start(self, scenario):
		"""Start a simulation with a given scenario. 
		
		QMessagebox appears to ask if the user wants to save the data of the experiment.
		If user wants to save the data, another QMessageBox appears to ask the name of the folder. 

		:param scenario (int): The index of the scenario to run.
		"""
		# Create a QMessageBox
		self.__nbVA = 5 	
		self.__nbWorker = 0
		settings = None
		if scenario == 2:
			settings = self.displayPopUpScenario2()
		elif scenario == 3:
			worker = self.displayPopUpWorkerOrNot(scenario)
			if worker == True: 
				self.__nbWorker = 1
		elif scenario == 4:
			worker = self.displayPopUpWorkerOrNot(scenario)
			if worker == True: 
				self.__nbWorker = 1

		# Ask the user if they want to save the data
		response_saveData = QMessageBox.question(self, "Save the experiment data", "Do you want to save the experiment data?", QMessageBox.StandardButton.Yes | QMessageBox.StandardButton.No, QMessageBox.StandardButton.No)

		# Get the save data options
		if response_saveData == QMessageBox.StandardButton.Yes:
			folder_name, ok = QInputDialog.getText(self, "Save experiment data", "Enter folder name for experiment data:", text="")
			if ok:
				saveData = (True, folder_name)
			else:
				return
		else:
			saveData = (False, None)
		
		self.appli.setNbVA(self.__nbVA)
		
		self.appli.env.launchScenario(self.__nbVA, self.__nbWorker, scenario, saveData)
		if scenario != 6:
			# Create a QTimer to delay placing the obstacle after 5 seconds
			timer = QTimer(self)
			self.createTimerScenario(timer, scenario, settings)

		# connect the simulationEnded signal to the displayEndSimulationMsgBox slot
		self.appli.env.simulationEnded.connect(self.displayEndSimulationMsgBox)

	def createTimerScenario(self, timer, scenario, settings):
		"""
		Create and configure a timer to trigger the placement of an obstacle in a specific scenario.

		Args:
			timer (QTimer): The QTimer object to be configured for delayed obstacle placement.
			scenario (int): An integer representing the scenario. Example: if the scenario is 1, the obstacle placement will be scheduled after 28 seconds .
		"""
		if scenario == 0:
			# Use singleShot to trigger the timer only once
			timer.singleShot(5000, self.settingsScenario0) # 5s
		elif scenario == 1: 
			# Use singleShot to trigger the timer only once
			timer.singleShot(28000, self.settingsScenario1) # 28s
		elif scenario == 2:
			if settings == 1 : 
				# corresponding to a failure of VA2 during its firt part of its mission
				timer.singleShot(26000, self.settingsScenario2) #26s
			elif settings == 2 :
				# corresponding to a failure of VA2 during its second part of its mission 
				timer.singleShot(54000, self.settingsScenario2) #54s
		elif scenario == 3: 
			timer.singleShot(18500, self.settingsScenario3) #18.5s
		elif scenario == 4: 
			timer.singleShot(5000, self.settingsScenario4) #5s

	def settingsScenario0(self):
		"""
		Place obstacle corresponding to the Scenario 0.

		This function is called when the timer created by 'createTimerScenario'
		times out in scenario 0. It places an obstacle at a predefined location [x, y]
		with a specified size.
		"""
		[x, y, size] = [135,175,4]
		self.circuit.addSpecificObstacle([x,y])
		self.appli.env.createObstacle([x,y], size)

	def settingsScenario1(self):
		"""
		Place obstacle corresponding to the Scenario 1.

		This function is called when the timer created by 'createTimerScenario1'
		times out in scenario 1. It places an obstacle at a predefined location [x, y]
		with a specified size.
		"""
		[x, y, size] = [135,126,4]
		self.circuit.addSpecificObstacle([x,y])
		self.appli.env.createObstacle([x,y], size)
	
	def settingsScenario2(self):
		"""
		Create the failure of the robot VA2 for the Scenario2.

		This function is called when the timer created by 'createTimerObstacleScenario'
		times out in scenario 1. It places an obstacle at a predefined location [x, y]
		with a specified size.
		"""
		self.simulateFailureAgent("VA2")

	def settingsScenario3(self):
		"""
		Place obstacle corresponding to the Scenario 3.

		This function is called when the timer created by 'createTimerScenario1'
		times out in scenario 3. It places an obstacle at a predefined location [x, y]
		with a specified size.
		"""
		[x, y, size] = [235,115,4]
		self.circuit.addSpecificObstacle([x,y])
		self.appli.env.createObstacle([x,y], size)

	def settingsScenario4(self):
		self.simulateErrorFromCamera("Camera4")

	def start_random(self):
		"""Start a simulation with a random scenario."""
		scenario = 5
		self.start(scenario)

	def start_simulation(self):
		"""Start a simulation without predefined scenario."""
		scenario = 5
		self.start(scenario)

	def start_0(self):
		"""Start a simulation with scenario 0."""
		scenario = 0
		self.start(scenario)

	def start_1(self):
		"""Start a simulation with scenario 1."""
		scenario = 1
		self.start(scenario)
		
	def start_2(self):
		"""Start a simulation with scenario 2."""
		scenario = 2
		self.start(scenario)

	def start_3(self):
		"""Start a simulation with scenario 3."""
		scenario = 3
		self.start(scenario)
	
	def start_4(self):
		"""Start a simulation with scenario 4."""
		scenario = 4
		self.start(scenario)

	def stop(self):
		"""Stop all VA."""
		if not self.getStopValue():
			self.appli.env.stopVA()
			self.buttonStop.setText("Continue")
			self.buttonStop.setStyleSheet("background-color: green;")
			self.setStopValue(True)
		else :
			self.appli.env.startAgain()
			self.buttonStop.setText("Stop")
			self.buttonStop.setStyleSheet("background-color: red;")
			self.setStopValue(False)

	def getStopValue(self):
		return self.__stopValue
	
	def setStopValue(self, value):
		self.__stopValue = value

	def selectPositionObstacle(self, event):
		"""
		Select the position of the obstacle to display on the environment.
		
		:param event (QMouseEvent): The mouse event that triggered the function.
		"""
		select = self.listObstaclesCombo.currentText()
		size = int(self.obstacleSizeCombo.currentText())
		locObstacles = self.circuit.getLocSpecificObstacles()

		if (select == "Obstacle S1"):
			[x, y] = locObstacles[0]	
		elif (select == "Obstacle S2"):
			[x, y] = locObstacles[1]
		elif (select == "Obstacle S3"):
			[x, y] = locObstacles[2] 
		elif (select == "Obstacle P1"):
			[x, y] = locObstacles[3]
		elif (select == "Obstacle P2"):
			[x, y] = locObstacles[4]
		elif (select == "Obstacle P3"):
			[x, y] = locObstacles[5]
		elif (select == "Obstacle P4"):
			[x, y] = locObstacles[6]
		elif (select == "Obstacle P5"):
			[x, y] = locObstacles[7]
		elif (select == "Human"):
			# not implemented yet
			[x,y] = [0,0]
			size = 0
		elif (select == "Random Obstacle"):
			coords_plan = self.interface.circuit.createCoordsPlan()
			[x, y] = random.choice(coords_plan)
		else: 
			[x,y] = [0,0]
			size = 0
		self.appli.env.createObstacle([x,y], size)

	def displayObstacle(self, obstacle):
		"""
		Display an obstacle on the simulation scene.

		:param obstacle (list): [x,y, size] of the obstacle.
		"""
		[x, y, size] = obstacle
		size_to_draw = size * 3
		if [x,y,size]== [0,0,0]:
			# to prevent the 1st selection of an obstacle from drawing 2 obstacles, one of which is at point (0,0)
			pass
		else:
			# create a rectangle with a given size and position
			rect = QGraphicsRectItem(x-size_to_draw/2, y-size_to_draw/2, size_to_draw, size_to_draw)
			self.listObstaclesItem.append(rect)
			red = QColor(255, 0, 0)
			rect.setPen(QPen(red))
			rect.setBrush(QBrush(red))
			rect.setData(0, [x,y]) # Save the obstacle's position data
			# add the rectangle to the scene
			self.scene.addItem(rect)

			self.listObstaclesCombo.setCurrentText("")
			
			self.show()

	def removeObstacle(self, pos_obstacle):
		"""
		Remove a specific obstacle from the simulation scene based on its coordinates.

		:param pos_obstacle (list): The coordinates of the obstacle to remove.
		"""
		for obstacle in self.listObstaclesItem:
				if obstacle.data(0) == pos_obstacle: # data(0) corresponds to the coordinates
					self.scene.removeItem(obstacle)
					self.listObstaclesItem.remove(obstacle)
					del obstacle
					break
		self.show()	
	
	def selectAgentFailure(self, event):
		"""
		Select and simulate the failure for a given agent.

    	:param event (QMouseEvent): The mouse event that triggered the function.
   		"""
		agent_name = self.listAgentsCombo.currentText()
		print("agent_name selected {}".format(agent_name))
		while not agent_name:
			return  # nothing is selected, so do nothing

		self.simulateFailureAgent(agent_name)

	def simulateErrorFromCamera(self, agent_name):
		txt = "Simulate Error from {}".format(agent_name)
		eventTrace(txt, simulationTrace)
		camera = Agent.getSpecificAgent(agent_name).camera
		[x, y] = camera.detectFakeObstacle()		
		self.displayCamera(camera.getPosition(), camera.getAreaSize(), detection=True, failure=False)
		self.displayCameraDetectedObstacle(camera.getNameCamera(), [x,y])

	def simulateFailureAgent(self, agent_name):
		"""
		Simulate a failure for a given agent.

    	:agent_name (str): The agent name. i.e: VA1, Camera2
   		"""
		num = int(re.findall(r'\d+', agent_name)[0]) # to extract just the number id of the agent

		for obj in Agent.getAgents():
			if obj.getAgentName() == str(agent_name) and "VA" in agent_name:
				obj.va.setAvailable(False)
				obj.va.setStatus(4) # Failure VA
				coord = obj.va.getCoord()
				self.displayVA(num, coord,failure=True) 
				txt = "Failure for {}".format(agent_name)
				eventTrace(txt, simulationTrace)
			elif obj.getAgentName() == str(agent_name) and "Camera" in agent_name:
				#obj.camera.setAvailable(False)
				obj.camera.setStatus(2) # Failure Camera
				pos = obj.camera.getPosition()
				area_size = obj.camera.getAreaSize()
				self.displayCamera(pos, area_size,failure=True) # failure
				self.setCameraBorderColorLabel(agent_name, "rgb(255, 165, 0)")
				txt = "Failure for {}".format(agent_name)
				eventTrace(txt, simulationTrace)		

				# Remove any obstacle position detected by the camera because the camera is in failure
				self.updateCameraDataLabels(agent_name, "detection", "[]")
				#TODO mettre en orange le label 
		else : pass

	def openCircuitImage(self):
		"""Open the circuit image window if it is not already opened. Closes the window if it is already opened."""
		if self.getCircuitWindowOpened() == False:
			# Create an instance of a circuit image in a window
			self.circuit_image_window = CircuitImageWindow(self)
			# show the window of the circuit image
			self.circuit_image_window.show()
			self.setCircuitWindowOpened(True)
		else : 
			self.circuit_image_window.close()
			self.setCircuitWindowOpened(False)

	def initVA(self):
		"""Initialize the visual of VA objects for the simulation.
		
		This function creates a set of visual objects for the VA simulation and adds them to the scene. VA are represented as ellipses, and are stored in a list for later use.
		"""
		self.tabVA = []
		for i in range (self.__MaxVA):
			va = self.scene.addEllipse(0, 0, 0, 0)
			self.tabVA.append(va)

	def initWorkers(self):
		"""Initialize the visual of worker objects for the simulation.

		This function creates a set of visual objects for the Worker simulation and adds them to the scene. Workers are represented as triangles, and are stored in a list for later use.
		"""
		self.tabWorkers = []
		for i in range(self.__MaxWorker):
			worker = self.scene.addPolygon(QPolygonF([QPointF(0, 0), QPointF(0, 0), QPointF(0, 0)]), pen=QPen(QColor(0, 135, 255), 2))
			self.tabWorkers.append(worker)

	def resetVA(self):
		"""Reset the visual objects and associated data for the simulation.
		
		This function removes the visual objects from the scene, resets the associated data labels, and initializes new visual objects for the next simulation.
		"""
		for va in self.tabVA:
			self.scene.removeItem(va)
		# Reset
		for i, va_name in enumerate(self.va_data_labels):
			for j, data_label in enumerate(self.va_data_labels[va_name]): 
				self.va_data_labels[va_name]["va_name"].setStyleSheet("")
				self.updateVADataLabels(va_name, data_label, "")
				self.updateVADataLabels(va_name, "va_name", va_name)
		# Need to init again VA for the next simulation

		self.initVA()

	def resetWorker(self): 
		"""Reset the visual objects associated to workers."""
		for worker in self.tabWorkers:
			self.scene.removeItem(worker)

	def resetTaskDataLabels(self):
		"""
		Reset the task data labels for all tasks in the mission : "task", "validated", and "status" labels to empty strings.
		"""
		for i, task_id in enumerate(self.tasks_data_labels):
			self.updateTaskDataLabels(task_id, "task", "")
			self.updateTaskDataLabels(task_id, "validated", "")
			self.updateTaskDataLabels(task_id, "status", "")
	
	def resetCameraInterface(self):
		"""
		Resets the camera interface by performing the following actions:

		1. Sets the border color label for each camera to default or None (no detection, no failure).
		2. Resets the status of each camera agent to working (status = 0).
		"""
		for camera_name in self.camera_data_labels:
			self.setCameraBorderColorLabel(camera_name, rgb_color=None)

		for obj in Agent.getAgents():
			if "Camera" in obj.getAgentName():
				obj.camera.setStatus(1) # Reset Camera to working
				pos = obj.camera.getPosition()
				area_size = obj.camera.getAreaSize()
				self.displayCamera(pos, area_size,detection=False,failure=False) # working

	def displayVA(self, num, coord, failure=False):
		"""
		Display the VA of the given number at the position given.
		
		:param num (int): Number of the VA.
		:param coord (list): List containing the x and y coordinates (position) of the VA.
		:param failure (boolean): A flag indicating whether the VA display should indicate a failure condition.
		"""
		self.setColorVA(num)
		r = 3
		x,y = coord[0], coord[1]
		if not failure:
			self.tabVA[num-1].setRect(x-r, y-r, 2*r, 2*r)
		else:
			pen = QPen(QColor(255, 0, 0))  # red pen
			cross1 = self.scene.addLine(x-r, y-r, x+r, y+r, pen)
			cross2 = self.scene.addLine(x+r, y-r, x-r, y+r, pen)
			self.crossLinesFailure.extend([cross1, cross2])

	def displayCamera(self, position, area_size, detection = False, failure = False):
		"""
		Display a camera with the given position and area size.

		:param position (list): List containing the x and y coordinates of the Camera.
		:param area_size (int): Size of the area covered by the Camera.
		:param failure (boolean): A flag indicating whether the Camera display should indicate a failure condition.
		"""
		x = position[0]
		y = position[1]
		if not failure and not detection: 
			self.scene.addRect(x - area_size, y - area_size, area_size*2, area_size*2, pen = QPen(QColor(0, 255, 0), 3))
		elif detection:
			self.scene.addRect(x - area_size, y - area_size, area_size*2, area_size*2, pen = QPen(QColor(255, 0, 0), 3))
		else: # failure
			self.scene.addRect(x - area_size, y - area_size, area_size*2, area_size*2, pen = QPen(QColor(255, 165, 0), 3))
			

	def displayWorker(self, num, position):
		"""
		Display a Worker with the given position.

		:param position (list): List containing the x and y coordinates of the worker.
		"""
		[x, y] = position
		area_size = 4
		# Define the vertices of the triangle
		triangle_vertices = [QPointF(x, y - area_size),
							QPointF(x - area_size, y + area_size),
							QPointF(x + area_size, y + area_size)]
		# Create a QPolygonF object with the triangle vertices
		triangle_polygon = QPolygonF(triangle_vertices)
		triangle_item = QGraphicsPolygonItem(triangle_polygon)
		# Set the pen color and width of the triangle item
		triangle_item.setPen(QPen(QColor(0, 0, 0), 2))
		# Add the triangle item to the scene
		self.scene.addItem(triangle_item)
		# Add the triangle item to the list of workers
		self.tabWorkers[num - 1] = triangle_item


	# Slot to update VA position in HMI
	@pyqtSlot(int, list)
	def movingVA(self,num,coord):
		"""
		Update the position of the VA of the given number at the coordinates given.

		:param num (int): Number of the VA.
		:param coord (list): List containing the x and y coordinates of the VA.
		"""
		x, y = coord[0], coord[1]
		rect = self.tabVA[num-1].rect()
		rect.moveCenter(QPointF(x, y))
		self.tabVA[num-1].setRect(rect)
		self.update()

	@pyqtSlot(int, list)
	def movingWorker(self, num, coord):
		"""
		Update the position of the Worker of the given number at the coordinates given.

		:param num (int): Number of the Worker.
		:param coord (list): List containing the x and y coordinates of the Worker.
		"""
		r = 4
		x, y = coord[0], coord[1]
		triangle_vertices = [QPointF(x, y - r), QPointF(x - r, y + r), QPointF(x + r, y + r)]
		self.tabWorkers[num - 1].setPolygon(QPolygonF(triangle_vertices))
		self.update()

	def changeColorTag(self, _num, _color):
		"""
		Change the color of the tag of the given number to the given color.

		:param _num (int): Number of the tag.
		:param _color (str): Color to set the tag to.
		"""
		self.tabVisuTAG[_num].setBrush(QBrush(QColor(_color)))

	def displayAuctioneerVA(self, _num):
		"""
		Highlight the VA of the given number to indicate that it is the auctioneer.

		:param _num (int): Number of the auctioneer VA.
		"""
		va_name = "VA" + str(_num)
		for key in self.va_data_labels.keys():
			if key == va_name:
				existing_style_sheet = self.va_data_labels[key]["va_name"].styleSheet()
				new_style_sheet = existing_style_sheet + "; border-width: 2px; border-style: solid;"
				self.va_data_labels[key]["va_name"].setStyleSheet(new_style_sheet)
			else:
				existing_style_sheet = self.va_data_labels[key]["va_name"].styleSheet()
				new_style_sheet = existing_style_sheet.replace("border-width: 2px; border-style: solid;", "")
				self.va_data_labels[key]["va_name"].setStyleSheet(new_style_sheet)

	def displayCameraDetectedObstacle(self, camera_name, pos_obstacle):
		"""
		Update the camera data labels with information about the obstacle detected by the camera with the given name.

		:param camera_name (str): Name of the camera that detected the obstacle.
		:param pos_obstacle (list): List containing the x and y coordinates of the detected obstacle.
		"""
		self.setCameraBorderColorLabel(camera_name, "rgb(255, 0, 0)")
		self.updateCameraDataLabels(camera_name, "detection", pos_obstacle)

	def getAvailableVA(self):
		"""
		Return a list of available VA numbers that can be assigned to tasks.
		An available VA is a VA that is not currently stopped or with a failure.

		:return available_VA (list): A list of available VA numbers.
		"""
		available_VA = []
		for i in range(len(self.tabVA)):
			va_name = "VA" + str(i+1)
			# At the beginning the VA are created but their text label available is empty
			if self.va_data_labels[va_name]["available"].text() == "True" or self.va_data_labels[va_name]["available"].text() == "":
				available_VA.append(i+1)
		return available_VA

	def getCircuitWindowOpened(self):
		"""
		Return whether the circuit window is currently open or not.

		:return circuitWindowOpened (bool): True if the circuit window is open, False otherwise.
		"""
		return self.circuitWindowOpened

	def setCircuitWindowOpened(self, value):
		"""
		Set the state of the circuit window.
		 
		:param value (bool): True to open the circuit window, False to close it.
		"""
		self.circuitWindowOpened = value

	@pyqtSlot()
	def displayEndSimulationMsgBox(self):
		"""Display a QMessageBox to indicate the end of the simulation.

		This function creates a QMessageBox with a message indicating that the simulation has ended. It adds custom buttons for "Reset" and "Close", and connects these buttons to the appropriate functions. The QMessageBox is then displayed on the main thread using `QCoreApplication.processEvents()`.
		"""
		self.endSimulation_msgBox = QMessageBox()
		self.endSimulation_msgBox.setText("Simulation is Done.")

		# Add custom buttons for Reset and Quit
		reset_button = self.endSimulation_msgBox.addButton("Reset", QMessageBox.ButtonRole.ResetRole)
		quit_button = self.endSimulation_msgBox.addButton("Close", QMessageBox.ButtonRole.RejectRole)

		# Connect the Reset and Quit buttons to functions
		reset_button.clicked.connect(self.reset)
		quit_button.clicked.connect(self.terminate)

		self.endSimulation_msgBox.show()
		# Process all pending events to ensure that the message box is displayed on the main thread
		QCoreApplication.processEvents()

	def reset(self):
		"""Reset the simulation to have the possibility to launch an other simulation, scenario. """
		print("Resetting simulation...")
		self.endSimulation_msgBox.hide()  # hide the message box after resetting the simulation
		# Reset VAs supervision for another scenario simulation
		self.resetVA()
		# Reset Workers supervision for another scenario simulation
		self.resetWorker()
		# After reset : the next simulation is not done 
		self.appli.env.setSimulationIsDone(False)
		# Delete all items of listObstacles corresponding to the previous simulation
		for item in self.listObstaclesItem:
			self.scene.removeItem(item)
		self.listObstaclesItem.clear()	
		for item in self.crossLinesFailure:
			self.scene.removeItem(item)
		self.crossLinesFailure.clear()
		# Reset the supervising tasks part interface
		self.resetTaskDataLabels()
		# Reset the supervising camera part interface
		self.resetCameraInterface()
		# Reset the supervising camera part interface
		self.resetCameraInterface()
		
		# Remove all VA names from the combo box of simulateFailure
		self.listAgents = [agent for agent in self.listAgents if not agent.startswith("VA")]
		self.listAgentsCombo.clear()
		self.listAgentsCombo.addItems(self.listAgents)

	def terminate(self):
		"""Terminate the program and stop the execution of the application."""
		print("Terminate simulation...")
		self.endSimulation_msgBox.hide()  # hide the message box before terminating the application
		self.appli.cleanup()
		self.appli.app.quit()

class CircuitImageWindow(QMainWindow):
	"""Class to create an instance of a new window representing the circuit."""

	def __init__(self, parent):
		"""
        Initialize a new instance of the CircuitImageWindow class.

        :param parent (QWidget): The parent widget of the CircuitImageWindow instance.
        """
		super().__init__(parent)

		self.setWindowTitle("Circuit Window")

		label = QLabel()
		pixmap = QPixmap("../images/circuit-representation.png")
		pixmap = pixmap.scaled(713, 822)
		label.setPixmap(pixmap)

		layout = QVBoxLayout()
		layout.addWidget(label)

		widget = QWidget()
		widget.setLayout(layout)

		self.setCentralWidget(widget)
		
