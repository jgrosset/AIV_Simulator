import os

from Console import errorTrace 
class Warehouse_Circuit(object):
	"""
    A class representing the circuit of a warehouse.
	"""

	def __init__(self):
		"""
		Attributes:
			nameCircuit (str): The name of the circuit.

			img_dijkstra_path (str): The path to the image of the circuit with Dijkstra's shortest path.
			
			img_circuit_path (str): The path to the image of the circuit.
			
			INF (int): A large value to simulate an infinity distance between two nodes.
			
			NbNodes (int): The number of nodes in the circuit.
			
			circuit (list of lists): The circuit edges, each sublist having three elements: the starting node, the ending node, and the distance between them.
			
			direction (list of int): The directions corresponding to ['down', 'up', 'right', 'left'].
			
			targetsNode (list of int): The nodes corresponding to the targets for the missions.
			
			parkings (list of int): The nodes corresponding to the parkings for the VAs.

			workersPlace (list of int): The nodes corresponding to the init place for the Workers.
    	"""
	
		self.nameCircuit = "Warehouse - Wholesaler"
		self.img_dijkstra_path = "images/circuit-dijsktra.png"
		self.img_circuit_path = os.path.join("images", "circuit.png")
		self.INF = 1000
		self.NbNodes = 26

		self.circuit = [
		[0, 1, self.INF],
		[1, 2, 3],
		[2, 1, 3],
		[2, 3, 10],
		[3, 5, 10],
		[3, 13, 10],
		[4, 6, 10],
		[4, 13, 10], # add for obstacle simulation
		[5, 3, 10],
		[5, 7, 10],
		[5, 14, 10],
		[6, 8, 10],
		[6, 14, 10], # add for obstacle simulation
		[7, 5, 10],
		[7, 10, 5],
		[7, 15, 10],
		[8, 9, 5],
		[8, 15, 10], # add for obstacle simulation
		[9, 10, 20],
		[10, 11, 10],
		[11, 12, 3],
		[11, 16, 12],
		[12, 11, 3],
		[13, 3, 40], # to promote the path [13, 4]
		[13, 4, 10],
		[14, 5, 40], # to promote the path [13, 4] [14, 6]
		[14, 6, 10],
		[15, 7, 40], # to promote the path [13, 4] [15, 8]
		[15, 8, 10],
		[16, 11, 2],
		[16, 17, 2],
		[16, 21, 3],
		[17, 16, 2],
		[17, 18, 2],
		[17, 22, 3],
		[18, 17, 2],
		[18, 19, 2],
		[18, 23, 3],
		[19, 18, 2],
		[19, 20, 2],
		[19, 24, 3],
		[20, 19, 2],
		[20, 2, 5],
		[20, 25, 3],
		[21, 16, 3],
		[22, 17, 3],
		[23, 18, 3],
		[24, 19, 3],
		[25, 20, 3]]

		self.direction = [1,2,3,4] 	#corresponding to à ['down','up','right','left']

		# Circuit nodes
		A0, A1, A2, A3, A4, A5 = 0, 1, 2, 3, 4, 5
		A6, A7, A8, A9, A10, A11 = 6, 7, 8, 9, 10, 11
		A12 = 12
		#targetPoints nodes corresponding to misions
		Target_1, Target_2, Target_3 = 13, 14, 15
		self.targetsNode = [Target_1, Target_2, Target_3]
		# startPoints nodes
		S1, S2, S3, S4, S5 = 16, 17, 18, 19, 20
		startPoints = [S1, S2, S3, S4, S5]
		# parking nodes for the VAs
		P1, P2, P3, P4, P5 = 21, 22, 23, 24, 25
		self.parkings = [P1, P2, P3, P4, P5]

		W1 = 1
		self.workersPlace = [W1]

		#3, 5, 6, 7, 8, 10
		#INtersection tags : A3 = 3, A5 = 5, A6 = 6, A7 = 7, A8 = 8, A10 = 10
		T3_b, T3_h, T3_d = 26, 27, 28
		T5_g, T5_d, T5_h = 29, 30, 31
		T6_g, T6_d, T6_b = 32, 33, 34
		T7_g, T7_d, T7_h = 35, 36, 37
		T8_g, T8_d, T8_b = 38, 39, 40
		T10_g, T10_h, T10_b = 41, 42, 43

		"""
		self.plan = [
		#1 2 3 4 5 6 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25
		[0,1,0,0,0,0,0,0,0,0 ,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
		[1,0,1,0,0,0,0,0,0,0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0],
		[0,0,0,0,1,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,1,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,1,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
		[0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1,0,0,0,1,0,0,0,0], # node 16 
		[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,1,0,0,0],
		[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,1,0,0],
		[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,1,0,0,0,1,0], # node 19
		[0,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,1],
		[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0],
		[0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0]]
		"""

		self.speeds = [
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4],
		[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4]]


		self.loc = [[0,315],#A0
			[5,315],	#A1
			[35,315],	#A2
			[35,215],	#A3
			[35,15],	#A4
			[135,215],	#A5
			[135,15],	#A6
			[235,215],	#A7
			[235,15],	#A8
			[285,15],	#A9
			[285,215],	#A10
			[285,315],	#A11
			[315,315],	#A12
			[35,115],	#M1
			[135,115],	#M2
			[235,115],	#M3
			[160,315],	#S1
			[135,315],	#S2
			[110,315],	#S3
			[85,315],	#S4
			[60,315],	#S5	
			[160,265],	#P1
			[135,265],	#P2
			[110,265],	#P3
			[85,265],	#P4
			[60,265]]	#P5

		self.nbTAG = len(self.loc)

		self.loc_specific_obstacles = [[35,115], [135,115], [235,115], [160, 265], [135, 265], [110, 265], [85, 265], [60, 265]]

		self.loc_intersections = [A3, A5, A6, A7, A8, A10]
		self.cameras = {1: {"location": A10, "area_size":20}, 
						2: {"location": Target_1, "area_size":20}, 
						3 : {"location": Target_2, "area_size":20}, 
						4 : {"location": Target_3, "area_size":20}}
		
		self.typeTAG = self.defineTAG()
		self.coords_plan = self.createCoordsPlan()

		self.matAdj = self.initMatAdj()

	def getNameCircuit(self):
		"""getNameCircuit(self) -> (str) Name of the circuit."""
		return self.nameCircuit

	def getImgDijkstraPath(self):
		"""getImgDijkstraPath(self) -> (str) Path of the dijkstra image of the circuit."""
		return self.img_dijkstra_path
	
	def getImgCircuitPath(self):
		"""getImgCircuitPath(self) -> (str) Path of the circuit image."""
		return self.img_circuit_path

	def getLocIntersections(self):
		"""getLocIntersections(self) -> (list) Id number of intersection nodes."""
		return self.loc_intersections

	def getParkingNode(self, id):
		"""getParkingNode(self, id) -> (int) Return the parking node with the given id.
		
		Args:
			id (int): The id of the parking node to retrieve.
		"""
		return self.parkings[id]
	
	def getWorkerNode(self, id):
		"""getParkingNode(self, id) -> (int) Return the init worker place node with the given id.
		
		Args:
			id (int): The id of the worker node to retrieve.
		"""
		return self.workersPlace[id]

	def getParkingsNode(self):
		"""getParkingsNode(self) -> (list[int]) Return all the parking nodes in the graph."""
		return self.parkings
	
	def getTargetsNode(self):
		"""getTargetsNode(self) -> (list[int]) Return all the target nodes in the graph."""
		return self.targetsNode
	
	def getLocTargetsNode(self):
		"""getLocTargetsNode(self) -> (list[list[int]])Returns the locations of all the target nodes in the graph."""
		targets_locations = []
		for node in self.getTargetsNode():
			targets_locations.append(self.getSpecificLoc(node))
		return targets_locations

	"""	
	def getNodeOfPosition(self, pos):
		for node, pos_circuit in enumerate(self.loc): 
			if pos_circuit == pos:
				return node
	"""
	def getNodeOfPosition(self, pos):
		"""getNodeOfPosition(self, pos) -> (int) Return the node id that corresponds to the given position.

		Args:
			pos (list): The position to look up.
		"""
		for node, pos_circuit in enumerate(self.loc):
			# Calculate the absolute difference between pos and pos_circuit for each coordinate
			diff_x = abs(pos_circuit[0] - pos[0])
			diff_y = abs(pos_circuit[1] - pos[1])

			# Check if the differences are less than or equal to 2
			if diff_x <= 2 and diff_y <= 2:
				return node

		# If no matching node is found, return None
		return None


	def getSpecificLoc(self, id):
		"""getSpecificLoc(self, id) -> (list[int]) Return the location corresponding to the given node id.
		
		Args:
			id (int): The id of the node to look up.
		"""
		return self.loc[id]

	def getLocSpecificObstacles(self):
		"""getLocSpecificObstacles(self) -> (list[list[int]]) Return the locations of all the obstacles in the graph."""
		return self.loc_specific_obstacles

	def getLocSpecificObstacle(self, id):
		"""getLocSpecificObstacle(self, id) -> (list[int]) Return the location corresponding to the given obstacle id.
		
		Args:
			id (int): The id of the obstacle to look up.
		"""
		return self.loc_specific_obstacles[id]
	
	def addSpecificObstacle(self, obstacle):
		"""
		Add a specific obstacle to the list of location-specific obstacles.

		Args:
			obstacle (list[int]): A list representing the location-specific obstacle
				to be added. The list should contain integer values describing the
				obstacle's location.
		"""
		self.loc_specific_obstacles.append(obstacle)

	def getSpeed(self, segment):
		"""getSpeed(self, segment) -> (int) Return the speed limit of the given road segment.
		
		Args:
			segment (tuple): The road segment to look up.
		"""
		return self.speeds[segment[0]][segment[1]]

	def getSegmentSpeed(self, source, dest):
		"""getSegmentSpeed(self, source, dest) -> (int) Return the speed limit of the road segment between the given source and destination nodes.
		
		Args:
			source (int): The id of the source node.
			dest (int): The id of the destination node
		"""
		return self.speeds[source][dest]

	def getPositionCamera(self, id):
		"""getPositionCamera(self, id) -> (list[int]) Return the location of the camera with the given id.
		
		Args:
			id (int): The id of the camera to look up.
		"""
		return self.loc[self.cameras[id]["location"]]
	
	def getAreaSizeOfCamera(self, id):
		"""getAreaSizeOfCamera(self, id) -> (int) Return the area size of a camera with the given id.

		Args:
			id (int): The id of the camera whose area size to return.
		"""
		return self.cameras[id]["area_size"]
	
	def getCameras(self):
		"""getCameras(self) -> (dict) Return the list of all cameras."""
		return self.cameras

	def getLoc(self):
		"""getLoc(self) -> (list[list[]]) Returns the list of locations."""
		return self.loc

	def getNbTAG(self):
		"""getNbTAG(self) -> (int) Return the number of tags."""
		return self.nbTAG

	def getTypeTAG(self):
		"""getTypeTAG(self) -> (str) Return the type of tags."""
		return self.typeTAG

	def defineTAG(self):
		"""defineTAG(self) -> (str) Define the type of tags."""
		#types TAG : 0 -> not defined, 1 -> intersection, ... 
		typeTAG = []
		for i in range(len(self.loc)):
			if i in self.loc_intersections:
				typeTAG.append(1)
			else : 
				typeTAG.append(0)
		return typeTAG

	def createCoordsPlan(self):
		"""Creates a list of coordinates for a plan.

		Returns:
			coords_plan (list): A list of coordinates of nodes.
		"""
		coords_plan = []
		for x in range(35,285):
			coords_plan.append((x, 15))
			coords_plan.append((x, 215))
			coords_plan.append((x, 315))
		for y in range(15,215):
			coords_plan.append((135, y))
			coords_plan.append((235, y))
		for y in range(15, 315):
			coords_plan.append((35, y))
			coords_plan.append((285, y))
		return coords_plan

	def compareMat(self, m1, m2):
		"""Compare two matrices.

		Args:
			m1 (list): The first matrix to compare.
			m2 (list): The second matrix to compare.

		Returns:
			bool: True if the matrices are the same, False otherwise.
		"""
		m = len(m1)
		res = True
		for i in range(m):
			for j in range(m):
				if m1[i][j] != m2[i][j]:
					res = False
		return res
	
	def getMatAdj(self):
		"""getMatAdj(self) -> (list) A matrix of n x n representing the environment graph."""
		return self.matAdj
		
	def initMatAdj(self):
		"""Initialize a matrix of n x n to infinity.

		Returns:
			list: A matrix of n x n initialized to infinity.
		"""
		mat = [[self.INF]*self.NbNodes for i in range(self.NbNodes)] # matrix of nxn initialized to INF = 1000
		for i in range(self.NbNodes):
			mat[i][i] = 0
		for i in range(len(self.circuit)):
			x, y, z = self.circuit[i][0], self.circuit[i][1], self.circuit[i][2] 
			mat[x][y] = z
		return mat
	
	def getCostEdgeMatAdj(self, node1, node2):
		"""Get the cost of the edge [node1, node2] of the initial adjacence matrix of the circuit.

		Args:
			node1 (int): The first node of the edge.
			node2 (int): The second node of the edge.

		Returns:
			int: cost of the edge [node1, node2] of the initial adjacence matrix of the circuit.
		"""
		return self.matAdj[node1][node2]
	

	def determineDirection(self, _segment):
		"""
		Determine the direction of the Agent based on the given segment.

		Args:
			_segment (list[str]): A list of two strings representing the segment to determine the direction for.

		Returns:
			direction (int): The direction of the Agent (Below,Up,Right,Left = 1,2,3,4)
		"""

		source, dest = _segment[0], _segment[1]
		direction = 0
		
		loc_source = self.getSpecificLoc(source)
		loc_dest = self.getSpecificLoc(dest)
		if loc_source[0] == loc_dest[0] and loc_source[1] < loc_dest[1]:
			direction = 1
		elif loc_source[0] == loc_dest[0] and loc_source[1] > loc_dest[1]:
			direction = 2
		elif loc_source[1] == loc_dest[1] and loc_source[0] < loc_dest[0]:
			direction = 3
		elif loc_source[1] == loc_dest[1] and loc_source[0] > loc_dest[0]:
			direction = 4
		else:
			txt = "Error in the source and destination coordinates of the VA : for segment {} at position {}-{} ".format(_segment,loc_source,loc_dest)
			errorTrace(txt)
		return direction
	
	def arriveToDestination(self, segment, coord):
		"""
		Determine if the Agent has arrived at its destination.

		Args:
			segment (list): A list representing the current segment of the track.

		Returns:
			bool: True if the vehicle has arrived at its destination, False otherwise.
		"""
		#direction = [1,2,3,4] 	#correspond à ['b','h','d','g']
		direction = self.determineDirection(segment)
		d = segment[1]
		pos_dest = self.getSpecificLoc(d)
		if direction == 1 and coord[1] >= pos_dest[1]:
			return True
		elif direction == 2 and coord[1] <=pos_dest[1]:
			return True
		elif direction == 3 and coord[0] >= pos_dest[0]:
			return True
		elif direction == 4 and coord[0] <= pos_dest[0]:
			return True
		else : return False