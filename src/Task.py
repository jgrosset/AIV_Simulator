#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Task class for the VA simulation application with the Warehouse circuit
# Task seen as an object to start with

INF = 1000
#Task class
class Task(object):
	"""
    This class represents a task in the VA simulation application.
    A task is defined by its source, target, destination, duration, and cycle.

    Attributes:
    	__taskStatus (int): The status of the task.
	"""
	
	__taskState = 0
	
	def __init__(self, _content, _duration=INF, _cycle=1):
		"""
        Initializes a Task object with the given parameters.
	
        :param _content: A list containing the source, target, and destination of the task.
        :param _duration: An integer representing the duration of the task. Default value is INF.
        :param _cycle: An integer representing the number of repetitions (cycles) of the task. Default value is 1.
        """
		self.source = _content[0] # source of the Task
		self.target = _content[1] # target of the Task
		self.destination = _content[2]	# destination of the Task
		self.duration = _duration # duration of the Task: INF if not specified
		self.cycle = _cycle	# number of repetitions (cycles) of the task
		self.taskState = "Active"
                
	def getStateTask(self):
		"""getStateTask(self) -> int: Returns the state of the task."""
		return self.taskState

	def getSourceTask(self):
		"""getSourceTask(self) -> str: Returns the source of the task."""
		return self.source

	def getTargetTask(self):
		"""getTargetTask(self) -> str: Returns the target of the task."""
		return self.target

	def getDestinationTask(self):
		"""getDestinationTask(self) -> str: Returns the destination of the task."""
		return self.destination

	def getTypeTask(self):
		"""getTypeTask(self) -> str: Returns the type of the task."""
		return self.typeTask

	def setStateTask(self, _state):
		"""
		Sets the state of the task to the given value.

		:param _state: the new state of the task.
		:type _state: int
		"""
		self.taskState = _state

	def getNumVA(self):
		"""getNumVA(self) -> int: Returns the number of the VA associated with the task."""
		return self.numVA

	def getNbCycles(self):
		"""getNbCycles(self) -> int: Returns the number of repetitions (cycles) of the task."""
		return self.cycle

	def displayTask(self):
		"""
		Displays the task in a formatted string.
		"""
		print("[id, numVA, typeTask, source, dest, duration, cycles] = [", self.idTask, self.numVA, self.typeTask,
				self.source, self.destination, self.duration, self.cycle, "]")

	def getPath(self):
		"""getPath(self) -> list: Returns the path of the task [source, target, destination]."""
		return [self.source, self.target, self.destination]

	def getFirstTask(self):
		"""getFirstTask(self) -> list: Returns the first part of the task [source, target]."""
		return [self.source, self.target]
	
	def getSecondTask(self):
		"""getSecondTask(self) -> list: Returns the second part of the task [target, destination]."""
		return [self.target, self.destination]