#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Environment class for the VIA simulation application with the Warehouse circuit

from Agent import *
from Supervisor import *
from Infrastructure import *
from VA import *
from Warehouse_Circuit import *
from Map import *
from Obstacle import *
from Console import *
from Experiment import *
from Worker import Worker

from PyQt6.QtCore import QTimer
	
class Environment(QObject):
	"""
    Manages the simulation environment and scenarios.

    Attributes:
		__nbVA (int): The number of VAs in the simulation.
		__nbWorker (int): The number of Workers in the simulation.
		__tabVA (list): A list of VAs in the simulation.
		__nbObstacles (int): The number of obstacles in the simulation.
		__tabObstacles (list): A list of obstacles in the simulation.
		num_Environment (int): The id of the environment in the simulation.
		__completedTasks (list): The list of tasks already completed by VA agents.
		__simulationIsDone (bool): Flag indicating whether the experiment is done.
		simulationEnded (pyQtSignal): A signal emitted when the simulation has ended.
    """
	__nbVA = 0
	__nbWorker = 0
	__tabVA = []
	__tabWorker = []
	__nbOstacles = 0
	__tabObstacles = []
	num_Environment = 0
	__blockedTasks = []
	__completedTasks = []
	__simulationIsDone = False # Flag to keep track of whether the experiment is done
	simulationEnded = pyqtSignal()

	def __init__(self, _appli):
		"""
        Constructor: initializes the environment object.

        Args:
        	_appli (Warehouse_Application): The application object.
        """
		appliTrace("Launch of the environment")
		Environment.num_Environment +=1
		self.__num = Environment.num_Environment # number of the Environment (only 1)
		#Agent.__init__(self, "Environment", self.__num) # launch of the function run()

		self.agent = Agent("Environment", self.__num) # launch of the function run()

		self.interface = _appli.interface
		self.supervisor = _appli.supervisor
		self.infrastructure = Infrastructure(self, self.interface)
		#self.infrastructure.start()
		self.map = Map(self, self.interface.width, self.interface.height)
		
		# launch of the run() function when start() is activate
		super().__init__()
		self.__thread = QThread()
		self.moveToThread(self.__thread)
		self.__thread.started.connect(self.run)
		self.__thread.start()

	def setSimulationIsDone(self, value):
		"""Set the value of the simulationIsDone attribute.

   		Args:
        	value (bool): The value to set the simulationIsDone attribute to.
    	"""
		self.__simulationIsDone = value
		
	def run(self):
		"""
		Run the simulation while the agent is active.
		"""
		while (self.agent.getActive()):
			if self.getTabVA() != []:
				self.continue_experiment(self.getTabVA())
				self.__simulationIsDone = True
			else:
				#alea = random.randint(0, 10)
				#time.sleep(alea)
				pass

	@pyqtSlot()
	def continue_experiment(self, VAs):
		"""
        Continues the simulation experiment.

        Args:
        	VAs (list): The list of VAs.
		
		This method runs the simulation experiment for all active VAs in the input list until they all become inactive. Once the simulation is complete, it saves the data if the user has opted to do so and clears all variables and data from the previous simulation. 
        """
		
		while any(va.agent.active for va in VAs):
        	# run experiment here
			pass


		if not any(va.agent.active for va in VAs) and not self.__simulationIsDone:
			if self.experiment.saveOrNot():
				self.experiment.writeResume()

			# delete the data of the simulation passed, and all the instances of VA
			self.experiment = None
			# delete all instances of VA of the previous simulation: corresponding to inactive VA
			Agent.deleteVAAgents()

			self.__tabVA = []
			self.__tabWorker = []
			self.__nbOstacles = 0
			self.__tabObstacles = []
			self.__completedTasks = []
			VA.resetNumVA()
			Worker.resetNumWorker()
			Supervisor.resetTabVA()
			Supervisor.resetTabWorker()

			self.simulationEnded.emit()


	def createVA(self, _nbVA):
		"""
		Create in the environment a specified number of VAs.

		Args:
   			_nbVA (int): The number of VAs to create.
		"""
		for i in range(_nbVA):
			num = i+1
			va = VA(self, self.infrastructure, self.map, self.experiment)
			#va.start()	# the start function launch run() functionn of va

			self.__tabVA.append(va)
			# VA initial coordinates: from Pi (Parking for all AIVs)
			Pi = self.interface.circuit.getParkingNode(num-1)
			coord = self.interface.circuit.getSpecificLoc(Pi)[0], self.interface.circuit.getSpecificLoc(Pi)[1]
			# display VA in the interface
			self.interface.displayVA(num, coord)
			self.supervisor.newVA(va)
		#self.map.displayStatusMap()

	def createWorker(self, _nbWorkers):
		"""
		Create in the environment a specified number of Workers.

		Args:
   			_nbWorkers (int): The number of Workers to create.
		"""
		for i in range(_nbWorkers):
			num = i+1
			worker = Worker(self, self.infrastructure, self.map, self.experiment)
			self.__tabWorker.append(worker)
			# Worker initial coordinates:
			init_place = self.interface.circuit.getWorkerNode(num-1)
			coord = self.interface.circuit.getSpecificLoc(init_place)[0], self.interface.circuit.getSpecificLoc(init_place)[1]
			# display VA in the interface
			self.interface.displayWorker(num, coord)
			self.supervisor.newWorker(worker)

	def launchScenario(self, _nbVA, __nbWorker, _scenario, saveData):
		"""
		Start the scenarios with the START buttons on the Hmi. 

		Launch a new simulation scenario with the specified number of VAs, scenario (allocate tasks), and save data option.

		Args:
			_nbVA (int): The number of VAs to launch the scenario with.
			_scenario (int): The scenario to launch.
			saveData (tuple): The save data option for the experiment - (True/False, folder_name)
		"""
		self.__nbVA = _nbVA
		self.__nbWorker = __nbWorker
		self.experiment = Experiment(_nbVA, _scenario, saveData)
		self.createVA(self.__nbVA)
		self.createWorker(self.__nbWorker)
		if _scenario == 5 : # Random Scenario
			self.supervisor.allocRandomTasks(_nbVA) 
		else : # Simulation Scenario 1, 2, 3, 4 or without predefined scenario 
			self.supervisor.allocTasksScenario(_nbVA) 
		# To save the number of tasks given by the supervisor to VAs
		tasks_to_be_done = self.supervisor.getScenarioTasksToBeDone()
		self.experiment.setTasksToBeDone(tasks_to_be_done)

		# display information about tasks given in the interface
		tasks_given = self.supervisor.getTasksGiven()
		self.interface.displayInformationTasksGiven(tasks_given)

	def stopVA(self):
		"""Stop all VA."""
		for va in self.__tabVA:
			va.stopThread()

	def startAgain(self):
		"""The simulation continues."""
		for va in self.__tabVA:
			va.resume()

	def createObstacle(self, obstacle, size=0):
		"""
		Create a new obstacle in the simulation environment and called the function displayObstacle().

		Write the position and the size of the environment in the experiment file if the user wants to save the scenario launched.
		
		Args:
			obstacle (list): The position of the obstacle as a list [x, y].
			size (int, optional): The size of the obstacle (default is 0, indicating no size).
		"""
		[x, y] = obstacle
		self.__nbOstacles += 1
		o = Obstacle(self, [x, y], size)
		#if t == 4: self.__tabObstacles.append(o)
		self.__tabObstacles.append(o) #TODO: all obstacles are now a problem
		self.map.initCoordObstacle([x, y], size)

		if self.experiment.saveOrNot(): # if the user ask to save the data
			self.experiment.writeObstacle(size, [x,y])
		self.interface.displayObstacle([x,y,size])	
	
	def removeObstacle(self, node_obstacle):
		pos_obstacle = self.interface.circuit.getSpecificLoc(node_obstacle)
		for obstacle in self.__tabObstacles: 
			if obstacle.getLocSpecificObstacle() == pos_obstacle:
				self.__tabObstacles.remove(obstacle)
				self.map.removeCoordObstacle(pos_obstacle)
				self.interface.removeObstacle(pos_obstacle)
				break

	def getTabObstacles(self):
		"""
		Retrieve the private attribute __tabObstacles.

		Returns:
		- list: A list containing the obstacles stored in the instance.
		"""
		return self.__tabObstacles

	def broadcastMsgToVAs(self, _sender, _msg):
		"""
		Broadcast a message to other VAs.

		Args: 
			_sender (dict): identity of the sender message.
			_msg (str): message broadcast.
		"""
		for i in range(len(self.__tabVA)):
			if _sender["type"] == "VA" : 
				if i != _sender["num"]-1 : 
					dest = self.__tabVA[i]
					msg = dest.receive(_sender, _msg)
			else : 
				dest = self.__tabVA[i]
				msg = dest.receive(_sender, _msg)
				txt = "Message from {} broadcast to VA{}".format(_sender, self.__tabVA[i].getNum())
				eventTrace(txt, simulationTrace)
		txt = "Message from {} broadcast to VAs".format(_sender)
		eventTrace(txt, messageTrace)

	def broadcastMsgToWorkers(self, _sender, _msg):
		"""
		Broadcast a message to other Workers.

		Args: 
			_sender (dict): identity of the sender message.
			_msg (str): message broadcast.
		"""
		for i in range(len(self.__tabWorker)):
			dest = self.__tabWorker[i]
			msg = dest.receive(_sender, _msg)
		txt = "Message from {} broadcast to Workers".format(_sender)
		eventTrace(txt, messageTrace)

	def getTabVA(self):
		"""getTabVA(self) -> list: Return the list of VA."""
		return self.__tabVA
	
	def getNbWorker(self):
		"""getNbWorker(self) -> int: Return the number of Workers."""
		return self.__nbWorker
	
	def hasActiveWorker(self):
		"""
		Check if any worker agent is active.

		Returns:
		bool: True if none of the worker agents are active, False otherwise.
		"""
		if not any(worker.agent.active for worker in self.__tabWorker):
			return True
		else:
			return False

	def allTasksAreCompleted(self):
		"""
		Returns True if all the tasks in the experiment have been completed, False otherwise.

		:return: bool
			Returns True if all the tasks in the experiment have been completed, False otherwise.
		"""
		# Check if all tasks are done
		if sorted(self.experiment.getTasksToBeDone()) == sorted(self.__completedTasks):
			txt = "All tasks are completed."
			eventTrace(missionTrace, txt)
			return True
		else:
			return False
			
	
	def allTasksAreCompletedOrBlocked(self):
		"""
		Returns True if all the tasks in the experiment have been completed orblocked, False otherwise.

		:return: bool
			Returns True if all the tasks in the experiment have been completed, False otherwise.
		"""
		total = 0
		for task in sorted(self.experiment.getTasksToBeDone()):
			if task in self.__completedTasks or task in self.__blockedTasks:
				total += 1
		if total == len((self.experiment.getTasksToBeDone())):
			#txt = "All tasks are completed or blocked."
			#eventTrace(txt, missionTrace)
			return True
		else:
			return False

	def addCompletedTask(self, task):
		"""
		Add task to the completedTasks list attribute.

		Args: 
			task (list): [source, target] or [target, destination] part of a Task - [source, target, destination].
		"""
		self.__completedTasks.append(task)

	def addBlockedTask(self, task):
		"""
		Add task to the blockedTasks list attribute.

		Args: 
			task (list): [source, target] or [target, destination] part of a Task - [source, target, destination].
		"""
		self.__blockedTasks.append(task)

	def removeBlockedTask(self, task):
		"""
		Remove task to the blockedTasks list attribute.

		Args: 
			task (list): [source, target] or [target, destination] part of a Task - [source, target, destination].
		"""
		self.__blockedTasks.remove(task)