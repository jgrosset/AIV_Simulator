#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Mission class for the VA simulation application with the Warehouse circuit

from Console import *
from Warehouse_Circuit import *
from Task import *
import Dijkstra
from sympy import Point, Segment


class VA_Mission(object):
	"""
    A class that represents a mission for a VA.

    Attributes:
    	circuit (Warehouse_Circuit): the circuit on which the mission is taking place
    	__numVA (int): the identifier of the VA assigned to the mission
    	task (Task): the task assigned to the VA for the mission
    	__startNodeVA (int): the starting node of the VA
    	__startPoint (int): the starting point of the task assigned to the VA
    	__targetPoint (int): the target point of the task assigned to the VA
    	__endPoint (int): the ending point of the task assigned to the VA
    	__shortVersionPathMission (list): the short versrion path to be followed by the VA for the mission
    	__nbCycles (int): the number of cycles required to complete the task assigned to the VA
    	_matAdj (list): the adjacency matrix of the circuit
	"""

	__numVA = 0
	__path = None
	__numPos = 0
	__startPoint = -1
	__targetPoint = -1
	__endPoint = -1
	__targetValidated = False
	__nbCycles = 0
	
	#######Parking!!!
	def __init__(self, circuit, _numVA, _startNode,  _t, _partMission = 0):
		"""
        Constructor: Initializes a new instance of the Mission class.

        Args:
        	circuit (Warehouse_Circuit): the circuit on which the mission is taking place
        	_numVA (int): the identifier of the vehicle agent assigned to the mission
        	_startNode (int): the starting node of the vehicle agent
        	_t (Task): the task assigned to the vehicle agent for the mission
        	_partMission (int): an optional argument indicating the part of the mission to which the task belongs
        """

		self.circuit = circuit
		self.__numVA = _numVA
		self.task = _t
		self.__startNodeVA = _startNode 
		self.__startPoint = self.task.getSourceTask()
		self.__targetPoint = self.task.getTargetTask()
		self.__endPoint = self.task.getDestinationTask()
		self.__shortVersionPathMission = self.createShortVersionPathMission()
		self.__nbCycles = self.task.getNbCycles()
		self._matAdj = self.circuit.initMatAdj().copy()

		# define the currentPartMission 
		if _partMission == 2 : # corresponds to a mission shared with the first part task validated
			self.__currentPartMission = _partMission
			self.setTargetValidated(True) # first part of the mission has already be done
		elif self.__startPoint == self.__startNodeVA:
			# no necessary path before beginning the mission
			self.__currentPartMission = 1
		else :
			# necessary path : currentNode to startPoint before beginning the mission
			# same if it is 
			self.__currentPartMission = 0

		self.__path, self.__total_distance = self.calculateEntirePath(self.__startNodeVA, self.__startPoint, self.__targetPoint, self.__endPoint)
		self.finPath = len(self.__path)-1
		self._trackingTravelledDistance = []

	def getNumVA(self):
		"""getNumVA(self) -> (int) Return the identifier of the VA assigned to the mission."""
		return self.__numVA

	def getTask(self):
		"""getTask(self) -> (Task) Return the task assigned to the VA for the mission."""
		return self.task

	def getstartNodeVA(self):
		"""getstartNodeVA(self) -> (int) Return the starting node of the VA."""
		return self.__startNodeVA

	def setstartNodeVA(self, value):
		"""
        Set the starting node of the VA to the given value.

        Args:
        	value (int): the new starting node of the VA
        """
		self.__startNodeVA = value

	def getStartPoint(self):
		"""getStartPoint(self) -> (int) Return the starting point of the task assigned to the VA."""
		return self.__startPoint

	def getTargetPoint(self):
		"""getTargetPoint(self) -> (int) Return the target point of the task assigned to the VA."""
		return self.__targetPoint

	def getEndPoint(self):
		"""getEndPoint(self) -> (int) Return the ending point of the task assigned to the vehicle agent."""
		return self.__endPoint

	def getPath(self):
		"""getPath(self) -> (list) Return the current value of the __path attribute."""
		return self.__path
	
	def getCurrentPartTask(self):
		if self.__currentPartMission == 2 :
			return [self.__targetPoint, self.__endPoint]
		else :
			return [self.__startPoint, self.__targetPoint]

	def getCurrentPartMission(self):
		"""getCurrentPartMission(self) -> (int) Return the current value of the __currentPartMission attribute."""
		return self.__currentPartMission

	def setCurrentPartMission(self, value):
		"""
        Set the value of the __currentPartMission attribute to the specified value.

        Args:
        	value (int): The value to set the __currentPartMission attribute to.
        """
		self.__currentPartMission = value 

	def getPartPathMission(self, part_value): 
		"""
        Return the path associated with the specified part of the mission.

        Args:
        	part_value (int): The index of the mission part to get the path for.

        Return: 
			part (list): The path associated with the specified mission part.
        """
		# First part mission corresponds to currrent_node to source_point (does not always exist) 
		if part_value == 0 : 
			return self.__firstPartMission
		# Second part mission corresponds to source_point to target_point 
		elif part_value == 1 : 
			return self.__secondPartMission
		# Third part mission corresponds to target_point to end_point 
		elif part_value == 2 :
			return self.__thirdPartMission
		else :
			return 0
	
	def setPartPathMission(self, part_value, path):
		"""
		Set the path for a specific part of the mission.

		Args:
			part_value (int): an integer that identifies the part of the mission (0, 1, or 2)
			path (list): a list of node IDs that make up the path for the specified part of the mission
		"""
		# First part mission corresponds to currrent_node to source_point (does not always exist)
		if part_value == 0 : 
			self.__firstPartMission = path
		# Second part mission corresponds to source_point to target_point 
		elif part_value == 1 : 
			self.__secondPartMission = path
		# Third part mission corresponds to target_point to end_point 
		elif part_value == 2 :
			self.__thirdPartMission = path
		else :
			pass

	def obstacleOnPath(self, pos_obstacle):
		"""
		Determine whether an obstacle is on the path of the mission, and return the path segment that the obstacle intersects.

		Args:
			pos_obstacle (tuple): a tuple representing the (x, y) position of the obstacle

		Returns:
			path_obstacle (list): a list of two node IDs that represent the start and end nodes of the path segment that the obstacle intersects, if any
			None if the obstacle is not on the path of the mission
		"""
		# function which returns the path [start_node,end_node] where the obstacle is present if it is in the path of the mission
		
		path_with_prev_checkpoint = self.getEndPathWithPreviousCheckPoint()

		for i in range(len(path_with_prev_checkpoint)-1):
			
			start_node = path_with_prev_checkpoint[i]
			pos_start_node = self.circuit.getSpecificLoc(start_node)
			end_node = path_with_prev_checkpoint[i+1]
			
			pos_end_node = self.circuit.getSpecificLoc(end_node)
			start_point = Point(tuple(pos_start_node))
			end_point = Point(tuple(pos_end_node))
			segment_nodes = Segment(start_point, end_point)
			# check if the obstacle is between 2 nodes following each other from the path 
			if segment_nodes.intersection(Point(tuple(pos_obstacle))):
				path_obstacle = [start_node,end_node]
				txt = "VA{} : obstacle in my path at position {} between nodes : {}!".format(self.__numVA, pos_obstacle, path_obstacle)
				eventTrace(txt, obstacleTrace)
			
				return path_obstacle # node where the VA can't access
			else : 
				#txt = "Obstacle with position {} is not on edge of the circuit".format(pos_obstacle)
				#eventTrace(txt, simulationTrace)
				pass

	def deactivateGraphNode(self, _n):
		"""
		Set the weight of all edges connected to the specified node to infinity, effectively removing it from the graph.

		Args:
			_n (int): the ID of the node to be removed
		"""
		for i in range(len(self._matAdj)):
			self._matAdj[i][_n] = INF
			self._matAdj[_n][i] = INF

		eventTrace(simulationTrace, "Node deactivate is {}".format(_n))

	def deactivatePathGraph(self, pathObstacle):
		"""
		Set the weight of the edge between the start and end nodes of the specified path segment to infinity, effectively removing it from the graph.

		Args:
			pathObstacle (list): a list of two node IDs that represent the start and end nodes of the path segment to be removed
		"""

		[node_1, node_2] = pathObstacle
		self._matAdj[node_1][node_2] = INF
		self._matAdj[node_2][node_1] = INF

	def reactivatePathGraph(self, pathObstacle):
		[node_1, node_2] = pathObstacle
		self._matAdj[node_1][node_2] = self.circuit.getCostEdgeMatAdj(node_1, node_2)
		self._matAdj[node_2][node_1] = self.circuit.getCostEdgeMatAdj(node_2, node_1)

	def reactivateGraphNode(self, nodeToActivate):
		txt = "Reactivate Graph Node: {}".format(nodeToActivate)
		eventTrace(txt, simulationTrace)
		for node in range(len(self._matAdj)):
			self._matAdj[nodeToActivate][node] = self.circuit.getCostEdgeMatAdj(nodeToActivate, node)
			self._matAdj[node][nodeToActivate] = self.circuit.getCostEdgeMatAdj(node, nodeToActivate)


	def calculateTaskPath(self, _s, _d):
		"""
		Calculate the shortest path between the start and end nodes of the mission using Dijkstra's algorithm.

		Args:
			_s (int): the ID of the start node
			_d (int): the ID of the end node

		Returns:
			path (list): a list of node IDs representing the shortest path between the start and end nodes
			min_distance (float): the length of the shortest path in terms of edge weights
		"""
		min_distance, pred = Dijkstra.Dijkstra(self._matAdj, _s, _d)
		path = Dijkstra.shortestPath(_s, _d, pred)
		return path, min_distance
	
	def calculateEntirePath(self, _currentNode, _s, _t, _d):
		"""
		Calculate the entire path of the mission. 

		Args:
			_s (int): the ID of the start node
			_t (int): the ID of the target node
			_d (int): the ID of the end node

		Returns:
			total_path (list): a list of node IDs representing the entire path of the mission
			total_distance (float): the length of the shortest path in terms of edge weights of the entire path
		"""
		#min_distance1 : all the min_distance from the source to all nodes of the circuit		
		path1, min_distance1 = self.calculateTaskPath(_s, _t)
		# set the path of the 1st part mission : source node to target node
		self.setPartPathMission(1, path1)
		
		distance_task1 = min_distance1[_t] # to obtain the min_distance from the source node to the _d : destination node
		#s, d = _t, path1[0]	# To avoid returning to node 0
		path2, min_distance2 = self.calculateTaskPath(_t,_d)
		#min_distance2 : all the min_distance from the source to all nodes of the circuit
		distance_task2 = min_distance2[_d]  # to obtain the min_distance from the source node to the d : destination node
		del path2[0]
		# set the path of the 2nd part mission : target node to end node
		self.setPartPathMission(2, path2)

		if _currentNode != _s and  self.getCurrentPartMission() == 0: 
			#min_distance1 : all the min_distance from the current node of the VA to all nodes of the circuit
			necessary_path_to_source, min_distance_necessary_to_source = self.calculateTaskPath(_currentNode, _s)
			# to obtain the min_distance from the current node to the source of the task : _s
			necessary_distance_to_source = min_distance_necessary_to_source[_s] 
			del path1[0]
			# set the path of the necessary part of the mission : current node to source node
			self.setPartPathMission(0, necessary_path_to_source)
		elif _currentNode != _t and  self.getCurrentPartMission() == 2: # case where a mission is recovered and the first part has already be done
			#min_distance1 : all the min_distance from the current node of the VA to all nodes of the circuit
			necessary_path_to_target, min_distance_necessary_to_target = self.calculateTaskPath(_currentNode, _t)
			# to obtain the min_distance from the current node to the source of the task : _t
			necessary_distance_to_target = min_distance_necessary_to_target[_t] 
			#del path2[0]
			# set the path of the necessary part of the mission : current node to source node
			self.setPartPathMission(1, necessary_path_to_target)
		elif _currentNode == _s: 
			# the VA is already on the source node, therefore there are no path necessary to go to the source node
			self.setCurrentPartMission(1)
		
		if self.getCurrentPartMission() == 0: 
			# A complete path is : necessary_path + path1 + path2
			total_path = necessary_path_to_source + path1 + path2
			total_distance = necessary_distance_to_source + distance_task1 + distance_task2 
			return total_path, total_distance
		if self.getCurrentPartMission() == 1: 
			# A complete path is : path1 + path2
			total_path = path1 + path2
			total_distance = distance_task1 + distance_task2 
			return total_path, total_distance
		elif self.getCurrentPartMission() == 2:
			# A complete path is : necessary_path + path2
			path = necessary_path_to_target + path2
			distance = necessary_distance_to_target + distance_task2
			return path, distance
		else: 
			txt= "Error calculateEntirePath()"
			eventTrace(txt, missionTrace)
	

	def recalculatePath(self, current_node, path_obstacle):
		"""
    	Recalculate the path of the VA if an obstacle is detected in the current path.

    	Args:
    		path_obstacle (list): the path segment containing the obstacle.
		
		Return: 
			path (list): the new path of the VA to accomplish the mission
		"""
		# update the pos node of the VA in the mission
		self.setstartNodeVA(current_node)

		cheminOLD = self.__path.copy()

		txt = "Old path of VA" + str(self.__numVA) + " : ", cheminOLD
		eventTrace(txt, missionTrace)
		txt = "Current Node : {} and Part of the mission : {}".format(current_node, self.getCurrentPartMission())
		eventTrace(txt, missionTrace)

		path_obstacle_in_cheminOLD = path_obstacle in (cheminOLD[i:i+len(path_obstacle)] for i in range(len(cheminOLD)-len(path_obstacle)+1))

		if (self.__targetValidated == True and cheminOLD[self.__numPos]==path_obstacle[1]):

			self.deactivatePathGraph(path_obstacle)
			txt = "VA" + str(self.__numVA) + "Modify his path - case 1 !"
			eventTrace(txt, missionTrace)

			for i in range (len(self.__path) - self.__numPos):
				del self.__path[self.__numPos]
			s, d = self.__path[self.__numPos-1], self.__endPoint
			path2, min_distance2 = self.calculateTaskPath(s, d)
			del path2[0]
			self.__path = self.__path + path2
			self.finPath = len(self.__path)-1

			txt = "New path of VA" + str(self.__numVA) + " : " + str(self.__path)
			eventTrace(txt, missionTrace)

			self.__numPos = self.__numPos - 1

			txt = "VA" + str(self.__numVA) + " returns : " + str([cheminOLD[self.__numPos + 1], cheminOLD[self.__numPos]])
			eventTrace(txt, missionTrace)
			# return the segment to retrace the path
			#return [cheminOLD[self.__numPos + 1], cheminOLD[self.__numPos]]


		elif (self.__targetValidated == True and path_obstacle_in_cheminOLD):
			txt = "VA" + str(self.__numVA) + " Modify his path - case 1 !!!"
			eventTrace(txt, missionTrace)

			self.deactivatePathGraph(path_obstacle)
			for i in range (len(self.__path) - self.__numPos):
				del self.__path[self.__numPos]
			s, d = self.__path[self.__numPos-1], self.__endPoint
			path2, min_distance2 = self.calculateTaskPath(s, d)
			del path2[0]
			self.__path = self.__path + path2
			self.finPath = len(self.__path)-1

			txt = "New path of VA" + str(self.__numVA) + " : " + str(self.__path)
			eventTrace(txt, missionTrace)

			self.__numPos = self.__numPos - 1

			txt = "VA" + str(self.__numVA) + " returns : " + str([cheminOLD[self.__numPos + 1], cheminOLD[self.__numPos]])
			eventTrace(txt, missionTrace)
			# return the segment to retrace the path
			#return [cheminOLD[self.__numPos + 1], cheminOLD[self.__numPos]]

		elif (self.__targetValidated == False and cheminOLD[self.__numPos]==path_obstacle[1]):
			txt = "VA" + str(self.__numVA) + " Modify his path case 3 !"
			eventTrace(txt, missionTrace)

			# add the return to the node before in his trackingTravelledDistance, because the agent detect an obstacle on the next node in his last path
			old_s, old_d = self.__path[self.__numPos-1], self.__path[self.__numPos]
			old_l = self._matAdj[old_s][old_d] 

			path_necessary, min_distance_necessary = self.calculateTaskPath(old_d, old_s)
			txt = "VA" + str(self.__numVA) + " : " + str(self.__path) + " will return back"
			eventTrace(txt, missionTrace)

			txt = "Deactivate path obstacle: {}".format(path_obstacle)
			eventTrace(txt, obstacleTrace)
			self.deactivatePathGraph(path_obstacle)
			
			for i in range (len(self.__path) - self.__numPos): 
				del self.__path[self.__numPos]

			s, d = self.__path[self.__numPos-1], self.__targetPoint
			path1, min_distance1 = self.calculateTaskPath(s, d)
			del path1[0]
			self.setPartPathMission(1, path1+path_necessary)
			s, d = self.__targetPoint, self.__endPoint
			path2, min_distance2 = self.calculateTaskPath(s, d)
			del path2[0]
			self.setPartPathMission(2, path2)
			self.__path = self.__path + path_necessary + path1 + path2

			txt = "New path of VA" + str(self.__numVA) + " : " + str(self.__path)
			eventTrace(txt, missionTrace)

			self.finPath = len(self.__path)-1
			#self.__numPos = self.__numPos - 1

			#txt = "VA" + str(self.__numVA) + " returns : " + str([cheminOLD[self.__numPos + 1], cheminOLD[self.__numPos]])
			#eventTrace(txt, missionTrace)

		elif(self.__targetValidated == False and path_obstacle_in_cheminOLD):
			txt = "VA" + str(self.__numVA) + " Modify his path - case 4 !"
			eventTrace(txt, missionTrace)
			
			if self.getCurrentPartMission() == 0:
				txt = "Deactivate path obstacle: {}".format(path_obstacle)
				eventTrace(txt, obstacleTrace)
				self.deactivatePathGraph(path_obstacle)

				self.__numPos+=1
				for i in range (len(self.__path) - self.__numPos):
					del self.__path[self.__numPos]

				path_necessary = []
				# path necessary to go the source node and the VA is on the path to the source node
				s = self.getstartNodeVA()
				d = self.getStartPoint() 
				path_necessary, min_distance_necessary = self.calculateTaskPath(s, d)

				self.setPartPathMission(0, path_necessary)
				s, d = self.getStartPoint(), self.getTargetPoint()
				path1, min_distance1 = self.calculateTaskPath(s, d)
				del path1[0]
				self.setPartPathMission(1, path1)
				s, d = self.getTargetPoint(),self.getEndPoint()
				path2, min_distance2 = self.calculateTaskPath(s, d)
				del path2[0]
				self.setPartPathMission(2, path2)
				self.__path = self.__path + path_necessary + path1 + path2

				txt = "New path of VA" + str(self.__numVA) + " : " + str(self.__path)
				eventTrace(txt, missionTrace)
				txt = "path_necessary" + str(path_necessary) + " - " + str(path1)
				eventTrace(txt, missionTrace)
				self.__numPos = self.__numPos -1
				
			else : # currentPartMission = 1
				txt = "I am {} and my Current Node : {} ".format(self.__numVA, current_node)
				eventTrace(txt, missionTrace)
			
				old_s, old_d = self.__path[self.__numPos-1], self.__path[self.__numPos]
				old_l = self._matAdj[old_s][old_d] 

				txt = "Deactivate path obstacle: {}".format(path_obstacle)
				eventTrace(txt, obstacleTrace)
				self.deactivatePathGraph(path_obstacle)
				for i in range (len(self.__path) - self.__numPos): 
					del self.__path[self.__numPos]

				s, d = self.__path[self.__numPos - 1], self.__targetPoint
				path1, min_distance1 = self.calculateTaskPath(s, d)
				#print("old_s : {} et old_d : {} et path : {} de longueur {}".format(old_s, old_d, path1, len(path1)))
				
				s_position_in_path1 = path1.index(s) 
				if path1[s_position_in_path1+1] == old_d: 
					# the VA continues its path
					txt = "VA" + str(self.__numVA) + " : " + str(self.__path) + " will not return back"
					eventTrace(txt, missionTrace)
					path_necessary = []
				
				else:
					# the VA is currently on the segment [old_s, old_d] we suppose the distance travelled correspond to go to the end of this segment, and come back because of the U-turn 
					path_necessary, min_distance_necessary = self.calculateTaskPath(old_d, old_s)
					txt = "VA" + str(self.__numVA) + " : " + str(self.__path) + " will return back"
					eventTrace(txt, missionTrace)

				del path1[0]
				self.setPartPathMission(1, path1+path_necessary)
				s, d = self.__targetPoint, self.__endPoint
				path2, min_distance2 = self.calculateTaskPath(s, d)
				del path2[0]
				self.setPartPathMission(2, path2)
				self.__path = self.__path + path_necessary + path1 + path2
			
				txt = "New path of VA" + str(self.__numVA) + " : " + str(self.__path)
				eventTrace(txt, missionTrace)

			self.finPath = len(self.__path)-1

		else:
			txt = "VA" + str(self.__numVA) + "Modify his path - case 5 ?"
			eventTrace(txt, missionTrace)
			self.deactivatePathGraph(path_obstacle)
			self.__numPos = self.__numPos - 1

			txt = "VA" + str(self.__numVA) + " returns : " + str([cheminOLD[self.__numPos + 1], cheminOLD[self.__numPos]])
			appliTrace(txt, missionTrace)

		# return the segment to retrace the path
		path = [cheminOLD[self.__numPos + 1], cheminOLD[self.__numPos]]
		return path

	def getStartPoint(self):
		"""getStartPoint(self) -> (int) Return the starting point of the mission."""
		return self.__startPoint
	
	def getEndPoint(self):
		"""getEndPoint(self) -> (int) Return the ending point of the mission."""
		return self.__endPoint
	
	def getTargetPoint(self):
		"""getTargetPoint(self) -> (int) Return the target point of the mission."""
		return self.__targetPoint
	
	def getNumPos(self):
		"""getNumPos(self) -> Return the number pos of the VA."""
		return self.__numPos

	def createShortVersionPathMission(self):
		"""createShortVersionPathMission(self) -> (list) Create and return a short version of the path mission."""
		shortVersionPathMission = []
		shortVersionPathMission.append(self.__startNodeVA)
		shortVersionPathMission.append(self.__startPoint) # source node of the path
		shortVersionPathMission.append(self.__targetPoint) # node of the target point 
		shortVersionPathMission.append(self.__endPoint) # desination node of the path
		return shortVersionPathMission
	
	def getShortVersionPathMission(self):
		"""getShortVersionPathMission(self) -> (list) Return the short version of the path mission."""
		return self.__shortVersionPathMission
	
	def getTargetValidated(self):
		"""getTargetValidated(self) -> (boolean) Return the current value of the __targetValidated attribute."""
		return self.__targetValidated
	
	def setTargetValidated(self, value):
		"""
        Set the value of the __targetValidated attribute to the specified value.

        Args:
        	value (boolean): The value to set the __targetValidated attribute to.
        """
		self.__targetValidated = value

	def getNbCycles(self):
		"""getNbCycles(self) -> Return the number of cycles of the mission."""
		return self.__nbCycles

	def setNbCycles(self, value):
		"""
		Set the number of cycles of the mission to the specified value.
		
		Args:
			value (int): the value of number of cycles.
		"""
		self.__nbCycles = value

	def getTrackingTravelledDistance(self):
		"""getTrackingTravelledDistance(self) -> Return the tracking travelled distance of the mission."""
		return self._trackingTravelledDistance		
	
	def calculateTravelledDistance(self, path_stopped=False):
		"""
		Calculate and return the total distance travelled during the mission.

		Return:
			dist (int): the total distance travelled during the mission.
		"""
		dist = 0
		if not path_stopped:
			for i in range(len(self._trackingTravelledDistance[:-1])): # delete the last track distance node which corresponds to the next move
				dist = dist + self._trackingTravelledDistance[i][2]
		else:
			for i in range(len(self._trackingTravelledDistance)): # the last track distance is counted when the VA is stopped 
				dist = dist + self._trackingTravelledDistance[i][2]
		return dist

		
	def changeNumPos(self):
		"""Update the position of a vehicle along a given path."""
		#txt = "VA {} with self.__numPos = {} and self.__path = {}".format(self.__numVA, self.__numPos, self.__path)
		#eventTrace(txt, simulationTrace)
		try :
			if (self.__path[self.__numPos] == self.__targetPoint):
				self.setTargetValidated(True)
				self.__numPos = self.__numPos + 1
			elif(self.__path[self.__numPos+1] == self.__endPoint):
				self.__numPos = self.__numPos + 1	
			else:
				self.__numPos = self.__numPos + 1
		except IndexError :
			txt = "IndexError : for VA{}".format(self.getNumVA())
			eventTrace(txt, errorTrace)
			txt = "__numPos {}\n path{}\n __target {}\n __endPoint {}\n".format(self.__numPos, self.__path,  self.__targetPoint, self.__endPoint)
			eventTrace(txt, errorTrace)
	
	def setPath(self):	#TODO: adapt new strategy
		"""Set the path for the current mission based on the current numPath attribute."""
		self.__path = self.calculateTaskPath(self.__numPath)
	
	def getPath(self):
		"""getPath(self) -> Return the current path for the mission."""
		return self.__path

	def setTotalDistancePath(self):
		"""Set the total distance of the path for the current mission."""
		return self.__total_distance

	def getTotalDistancePath(self):
		"""getTotalDistancePath(self) -> (int) Return the total distance of the path for the current mission."""
		return self.__total_distance
	
	def getEndPathWithPreviousCheckPoint(self):
		"""getEndPathWithPreviousCheckPoint(self) -> (list) Return a copy of the end path for the current mission, with the previous checkpoint added to the beginning of the path."""
		end_path = self.getEndPath().copy()
		end_path_with_prev_checkpoint = [self.__path[self.__numPos-1]] + end_path 
		return end_path_with_prev_checkpoint

	def newDestination(self):
		"""
		Update the position of the VA along the path and return the new destination node.

		Return: 
			dest(int): the new destination node of the VA.
		"""
		self.changeNumPos()
		try:
			s, d = self.__path[self.__numPos - 1], self.__path[self.__numPos]
		except IndexError:
			txt = " function NewDestination with path {} and numPos {}.".format(self.__path, self.__numPos)
			eventTrace(txt, errorTrace)
			# Task is blocked for example if the target node is not accessible: node is deactivate
			self.task.setStateTask("Block")
			return None
		
		l = self._matAdj[s][d]
		
		if l != INF: 
			self._trackingTravelledDistance.append([s,d,l])
		else : 
			# Case: When VA has to return back because of an obstacle on a segment, that becomes with a length of INF for the DIjkstra algorithm
			# To calculate the length of the VA's path, it is the initial length of this segment, not INF.
			original_mat_adj = self.circuit.getMatAdj()
			l = original_mat_adj[s][d]
			self._trackingTravelledDistance.append([s,d,l])
		return self.__path[self.__numPos]
	
	def getEndPath(self):
		"""getEndPath(self) -> Return the end path for the current mission."""
		path = []
		for i in range(self.__numPos, self.finPath+1):
			path.append(self.__path[i])
			"""
			try :
				path.append(self.__path[i])
			except IndexError: pass
				# because there is a path less large in list than the first one (but it is more expensive this one)
			"""
		return path

	def getFirstPartTrackingTravelledDistance(self):
		"""
		Return the first part of the tracking of the distance travelled by the VA for the current mission.

		Returns: 
			firstPartTrackingTravelledDistance (list): Return a list of sub-paths where each sub-path is represented as a list [source_node, destination_node, cost_distance].
		"""
		return self.firstPartTrackingTravelledDistance

	def setFirstPartTrackingTravelledDistance(self, value):
		"""
		Set the value of firstPartTrackingTravelledDistance attribute for the current mission.
		
		Take a list of sub-paths where each sub-path is represented as a list [source_node, destination_node, cost_distance].
		"""
		self.firstPartTrackingTravelledDistance = value
	
	def getSecondPartTrackingTravelledDistance(self):
		"""
		Return the second part of the tracking of the distance travelled by the VA for the current mission. 
		
		If the vehicle has completed the first part of the mission, it return the tracking of the distance travelled after the first part of the mission. 
		
		If the vehicle has not yet completed the first part of the mission, it return the tracking of the distance travelled for the entire mission.
    
		Returns: 
			_trackingTravelledDistance (list): a list of sub-paths where each sub-path is represented as a list [source_node, destination_node, cost_distance].
		"""
		# this function is exact if the mission is at the end
		if hasattr(self, 'firstPartTrackingTravelledDistance'):
			lastCheckPointOfFirstPart = self.firstPartTrackingTravelledDistance[-1]

			for partTrack in self._trackingTravelledDistance : 
				if partTrack == lastCheckPointOfFirstPart: 
					# return all the track travelled distance after the first part of the mission
					return self._trackingTravelledDistance[self._trackingTravelledDistance.index(partTrack)+1:]
		else: 
			# if the VA get a mission shared with the second task to do, therefore it did not have a self.firstPartTrackingTravelledDistance defined
			return self._trackingTravelledDistance

	def calculateDistanceFromTrackingTravelledDistance(self, trackingTravelledDistance):
		"""
		Calculate the total distance travelled by the VA based on the given tracking of the distance travelled.
    	Take a list of sub-paths where each sub-path is represented as a list [source_node, destination_node, cost_distance].
	    
    	Returns: 
			distance (float): Total distance travelled by the VA.
		"""
		# trackingTravelledDistance = [source_node, destination_node, cost_distance]
		distance = 0
		for sub_path in trackingTravelledDistance:
			distance+= sub_path[2]
		return distance



		
