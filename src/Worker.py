
import time
from Agent import Agent
from Worker_Mission import Worker_Mission
from PyQt6.QtCore import QObject, QThread, pyqtSignal, pyqtSlot,QThread, QWaitCondition, QMutex
from Console import *
from Task import Task
from Message import *

from VA import STOP, CIRCULATES, TERMINATE

class Worker(QObject):
    num_Worker = 0
    __missionAssigned = False
    __status = STOP

    update_worker_position = pyqtSignal(int, list)
    update_listAgents = pyqtSignal(str)

    def __init__(self, _env, _infrastructure, _map, _experiment):
        Worker.num_Worker +=1 
        self.__num =  Worker.num_Worker
        self.agent = Agent("Worker", Worker.num_Worker)

        # launch of the run() function when start() is activate
        super().__init__()
        self.__thread = QThread()
        self.moveToThread(self.__thread)
        self.__thread.started.connect(self.run)

        self.worker_name = "Worker"+str(self.__num)
        self.interface = _env.interface
        self.env = _env

        self.__initPlace = self.interface.circuit.getWorkerNode(self.__num-1) # source point
        self.__coord = self.interface.circuit.getSpecificLoc(self.__initPlace)

        txt = "Launch of Worker" + str(self.__num) + " at node" + str(self.__initPlace)
        appliTrace(txt)

        self.infrastructure = _infrastructure # to communicate with the infrastructure
        self.map = _map		# map shared by all the VAs too
        self.map.initCoordAgent(self.__coord)

        self.available = True # Worker is available

        self.mission = None
        self.setMissionAssigned(False)
        self.missionCompleted = False
        self.pendingMissions = []

        self.experiment = _experiment

		# connect the pyqtsignal update_worker_position to the pyqtslot movingWorker of the interface
        self.update_worker_position.connect(self.interface.movingWorker)
        # connect the pyqtsignal update_position to the pyqtslot updateListAgents of the interface
        self.update_listAgents.connect(self.interface.updateListAgents)

        # Attribute use when the Worker is stop and have to circulates again
        self._running = True        
        self.__thread.start()
        self.cond = QWaitCondition()
        self.mutex = QMutex()
    
    def getNameWorker(self):
        """getNameWorker(self) -> (str) Return the name of the Worker."""
        return self.worker_name

    def getNum(self):
        """getNum(self) -> int: The unique identifier number of the Worker."""
        return self.__num

    def getCoord(self):
        """getCoord(self) -> tuple(float): Return the coordinates of the Worker."""
        return self.__coord
    
    def getMissionAssigned(self):
        """getMissionAssigned(self) -> int: Return the mission assigned to the Worker."""
        return self.__missionAssigned
    
    def setMissionAssigned(self, value):
        """
        Set the mission assigned to the Worker.

        Args:
            value (int): The mission assigned to the Worker.
        """        
        self.__missionAssigned = value
    
    def getMission(self):
        """getMission(self) -> Mission: Return the mission of the Worker."""
        return self.mission
    
    
    def setCurrentSegment(self, newSegment):
        """Set the current segment of the Worker.

        Args:
            newSegment (list[str]): A list of two strings representing the new segment to be set.
        """
        self.__segment = newSegment

    def moveToDestination(self, _speed):
        """
    	Move the Worker towards its destination while handling different types of obstacles and reading tags.

    	Args:
        	_speed (float): The speed of the Worker.
		"""
        speed = _speed

        while (self.interface.circuit.arriveToDestination(self.__segment, self.__coord) == False and self.__status != STOP):
            # check if the simulation is running
            self.mutex.lock()
            if not self._running:
                self.cond.wait(self.mutex)
            self.mutex.unlock()

            # The Worker moves circulates
            self.move(speed)
        if self.mission == None : 
			#Case:  no mission attributed for the moment
            return
        if (self.__segment[1] == self.mission.getTargetPoint()) :
            #simulates stopping at the mission point
            go = 3
            time.sleep(go)
    
    @pyqtSlot(float)
    def move(self, _speed):
        """
        Move the Worker at the given speed along its current segment and emit the updated position to the interface.

        Args:
            _speed (float): the speed at which the VA moves, expressed as a float between 0 and 10.
        """
        # The Worker moves at speed (simulation with the sleep() method)
        time.sleep(0.5-(_speed/11))
        distance = 2

        direction = self.interface.circuit.determineDirection(self.__segment)
        coordOLD = self.__coord
        self.__coord = self.map.updateCoord(coordOLD, direction, distance)
        self.map.changeCoordAgent(coordOLD, self.__coord)
        # update the position in the interface
        self.update_worker_position.emit(self.__num, self.__coord)
        	
    def receive(self, _sender_identity, _msg):
        """
        Receive a message from the given sender and process it.

        Args:
            _sender_identity (dict): The identity of the sender.
            _msg (Message): The message to receive.

        Returns:
            str: The type of message received.
        """
        msgT, msgV =  _msg.getTypeMsg(), _msg.getValueMsg()
        sender_identity = _sender_identity
        self.processMsg(_msg, _sender_identity)
        return msgT
    
    def processMsg(self, _msg, _sender):
        """
        Processes the messages received by the VA.
        
        Args:
            _msg (Message): Message received by the VA
            _sender (dict): Agent identity that sent the message
        """
        type_msg = _msg.getTypeMsg() #type of message
		
        if type_msg == "DENM": 
            denm_content = _msg.getContent()
            if denm_content["type"] == "obstacle":
                node_obstacle = self.interface.circuit.getNodeOfPosition(denm_content["position"])
                if node_obstacle != None:# obstacle is on a node
                    
                    if self.mission == None: 
                        txt = "{} have to go to to the node {} to move an obstacle".format(self.agent.getIdentity(), node_obstacle)
                        eventTrace(txt, missionTrace)
                        pos_node_worker = self.interface.circuit.getNodeOfPosition(self.getCoord())
                        content_task = [pos_node_worker, node_obstacle, pos_node_worker]

                        self.mission = Worker_Mission(self.interface.circuit, self.getNum(), Task(content_task), _msg.getIdentitySender())
                        self.setMissionAssigned(True)
                    else : 
                        # check if the task is not already in progress 
                        #TODO: check if the task is not already ongoing by an other worker
                        if node_obstacle == self.mission.getTargetPoint():
                            txt = "{} is already on going to move the obstacle at the node {}".format(self.agent.getIdentity(), node_obstacle)
                        else :
                            #TODO: put the task in pendingMissions if an other worker didn't took it
                            pass
                else : # obstacle is on a edge
                    #TODO: to be implemented
                    pass
        elif type_msg == "CPM":
            size = int(_msg.getSizeObstacle())
            pos_obstacle =_msg.getPosObstacle()
            node_obstacle = self.interface.circuit.getNodeOfPosition(pos_obstacle) # value: None if the obstacle is not on a edge of the graph, not precisely on a node

            txt = "Worker{} received {} : obstacle with size {} at position {} - node obstacle : {}".format(self.getNum(), type_msg, size, pos_obstacle, node_obstacle)
            eventTrace(txt, messageTrace)
            txt = "{} have to go to to the position {} to verify an obstacle and move it, if necessary".format(self.agent.getIdentity(), pos_obstacle)
            eventTrace(txt, missionTrace)
            pos_node_worker = self.interface.circuit.getNodeOfPosition(self.getCoord())
            #content_task = [pos_node_worker, pos_obstacle, pos_node_worker]
            content_task = [pos_node_worker, node_obstacle, pos_node_worker]

            self.mission = Worker_Mission(self.interface.circuit, self.getNum(), Task(content_task), _msg.getIdentitySender())
            self.setMissionAssigned(True)

    @staticmethod
    def resetNumWorker():
        """Reset the class attribute `num_Worker` to 0."""
        Worker.num_Worker = 0

    def setStatus(self, value):
        """
        Set the status of the Worker.

        Args:
            value (int): The status of the Worker.
        """
        self.__status = value

    def stopMoving(self):
        """Stop the Worker moving."""
        self.setStatus(STOP)
        txt = "Worker{} stops !".format(self.__num)
        eventTrace(txt, missionTrace)


    def saveInfoTask(self, partMission):
        """
        Save the information about the completed task, and update the experiment results data saved.

        Args:
            validate (bool): True if the task is validated, False otherwise.
        """
        # To obtain information about a part of task finished
        txt = "Worker{} travelled a distance of {}".format(self.__num, self.mission.calculateTravelledDistance())
        eventTrace(txt, simulationTrace)
        txt = "Worker{} followed the path : {}".format(self.__num, self.mission.getTrackingTravelledDistance())
        eventTrace(txt, simulationTrace)

        if partMission == 1:
            task = self.mission.getTask().getFirstTask() 
    
            if self.experiment.saveOrNot():
                firstPartTravelledDistance = self.mission.getTrackingTravelledDistance()[:-1].copy() # delete the last track distance node which corresponds to the next move
                self.mission.setFirstPartTrackingTravelledDistance(firstPartTravelledDistance)
                # write results concerning the task in csv file
                self.experiment.writeResult(self.getNameWorker(), self.mission.calculateTravelledDistance(), self.mission.getShortVersionPathMission(), task, True, firstPartTravelledDistance)
        else : 
            task = self.mission.getTask().getSecondTask() 

            if self.experiment.saveOrNot():
                secondPartTravelledDistance = self.mission.getSecondPartTrackingTravelledDistance()
                distanceTravelled = self.mission.calculateDistanceFromTrackingTravelledDistance(secondPartTravelledDistance)
                # write results concerning the task in csv file
                self.experiment.writeResult(self.getNameWorker(), distanceTravelled, self.mission.getShortVersionPathMission(), task, True, secondPartTravelledDistance)
        # save the info that the task is completed in the environment instance


    def run(self):
        """The activity function for the Worker thread. Display the Worker in the interface and wait for assignment missions."""
                        
        self.update_listAgents.emit(self.worker_name)

        while(self.__missionAssigned == False):
            tempo = 0.2
            time.sleep(tempo)
        self.setStatus(CIRCULATES)
        
        txt = "Worker{} : path of the mission = {}".format(self.__num, self.mission.getPath())
        eventTrace(txt, missionTrace)

        txt = "Worker{} begin its mission".format(self.__num)
        eventTrace(txt, missionTrace)
        # determination of the first mission's  destination
        destination = self.mission.newDestination()

        # update the value and the interface
        self.setCurrentSegment([self.__initPlace, destination])
        speed = self.interface.circuit.getSpeed(self.__segment)

        self.moveToDestination(speed)

        while (self.__status != TERMINATE):
            while self.__status == STOP:
                # New mission assigned by sharing, the Worker has to circulate again
                if self.getMissionAssigned() == True:
                    self.setStatus(CIRCULATES)
                    break
            
                #TERMINATE just if all other tasks in the environment are completed by VA
                elif self.env.allTasksAreCompleted():
                    self.setStatus(TERMINATE)
                    break
                else: 
                    pass
        
            oldDestination = self.__segment[1]
            speed = self.interface.circuit.getSpeed(self.__segment)
            #1) the Worker is heading towards its destination at a random speed
            #txt = "Worker{} is on segment: {}".format(self.__num, self.__segment)
            #eventTrace(txt, missionTrace)
            self.moveToDestination(speed)

            if self.mission == None:
                # Use Case: if the Worker has nos mission to do.
                # In the Next iteration of the while loop, the status of the Worker is "STOP" now.
                continue
            
            if destination == self.mission.getTargetPoint(): # Target Validate
                pos_obstacle = self.interface.circuit.getSpecificLoc(destination)
                # The obstacle exists
                txt=(f"destination worker {destination}")
                eventTrace(txt, missionTrace)

                if any(pos_obstacle == obstacle.getLocSpecificObstacle() for obstacle in self.env.getTabObstacles()):
                    txt = "{} removes the obstacle".format(self.agent.getIdentity())
                    eventTrace(txt, simulationTrace)
                    self.env.removeObstacle(self.mission.getTargetPoint()) # remove the obstacle present at the target point of the task: this is the mission
            
                else : # Obstacle not present, it can be an error of the camera detected a fake obstacle
                    agent_identity = self.mission.getAgentTransmitter()
                    denm_content = {"type" : "failure", "agent": agent_identity}
                    denm_msg = DENM(self.agent.getIdentity(), denm_content)
                    self.env.broadcastMsgToVAs(self.agent.getIdentity(), denm_msg)
                    agent_name = str(agent_identity['type']) + str(agent_identity['num'])
                    self.interface.simulateFailureAgent(agent_name)

                msg = CPM(self.agent.getIdentity(), pos_obstacle, 0)
                self.env.broadcastMsgToVAs(self.agent.getIdentity(), msg)

                destination = self.mission.newDestination()
                # update the segment value and the interface
                self.setCurrentSegment([oldDestination, destination])

                self.saveInfoTask(1) # First part of its mission : remove the obstacle

            elif destination == self.mission.getEndPoint():
                #TODO: stop the human
                txt = "{} is come back to to its init place".format(self.agent.getIdentity())
                eventTrace(txt, missionTrace)
                # End of this mission
                self.saveInfoTask(2) # Second part of its mission : come back to its init place
                self.mission = None
                self.setMissionAssigned(False)
                self.stopMoving()
                
            elif destination == self.mission.getStartPoint(): # Start Validate
                destination = self.mission.newDestination()
                # update the segment value and the interface
                self.setCurrentSegment([oldDestination, destination])
            else : 
                destination = self.mission.newDestination()
                # update the segment value and the interface
                self.setCurrentSegment([oldDestination, destination])
                #txt = "{} is in a use case not treated.".format(self.agent.getIdentity())
                #eventTrace(txt, simulationTrace)
        
        self.agent.terminate()

