#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Obstacle class for the VA simulation application with the Warehouse circuit

from Console import *

class Obstacle(object):
	"""
    Represent an obstacle in a 2D space.

    Attributes:
    	- appli (Warehouse_Application): The application
    	- __loc (list): The (x, y) coordinates of the top-left corner of the obstacle
    	- __size (int): The size of the obstacle in pixels
	"""
	__loc = [-1, -1]
	__size = 0
	
	def __init__(self, _appli, _loc, _size = 0):
		"""
        Initialize a new obstacle instance.

        Args:
        	- _appli (object): the application using the obstacle
        	- _loc (list of int): the (x, y) coordinates of the obstacle
       		- _size (int, optional): the size of the obstacle in pixels (default: 0)
        """
		self.appli = _appli
		self.__loc = _loc
		x, y = self.__loc[0], self.__loc[1]
		self.__size = _size
		txt = "Obstacle -> ({},{})".format(self.__size, self.__loc)
		eventTrace(txt, obstacleTrace)
	
	def getLocSpecificObstacle(self):
		"""getLocSpecificObstacle(self) -> (list) Return the location of the obstacle as a tuple of integers (x, y)."""
		return self.__loc