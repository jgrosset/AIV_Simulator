import Dijkstra
from Console import *

class Worker_Mission(object):
    """
    A class that represents a mission for a Worker.

    Attributes:
    	circuit (Warehouse_Circuit): the circuit on which the mission is taking place
    	__numWorker (int): the identifier of the Worker assigned to the mission
    	task (Task): the task assigned to the Worker for the mission
    	__startNodeWorker (int): the starting node of the Worker
    	__startPoint (int): the starting point of the task assigned to the Worker
    	__targetPoint (int): the target point of the task assigned to the Worker
    	__endPoint (int): the ending point of the task assigned to the Worker
    	__shortVersionPathMission (list): the short versrion path to be followed by the Worker for the mission
    	_initAdj (list): the initial adjacency matrix of the circuit 
	"""

    __numWorker = 0
    __path = None
    __numPos = 0
    __startPoint = -1
    __targetPoint = -1
    __endPoint = -1
    __targetValidated = False
	
	
    def __init__(self, circuit, _numWorker,  _t, agent_transmitter):
        """
        Constructor: Initializes a new instance of the Worker Mission class.

        Args:
            circuit (Warehouse_Circuit): the circuit on which the mission is taking place
            _numWorker (int): the identifier of the worker assigned to the mission
            _startNode (int): the starting node of the worker
            _t (Task): the task assigned to the worker for the mission
            agent_transmitter (Agent): The agent transmitter of the mission.
        """

        self.circuit = circuit
        self.__numWorker = _numWorker
        self.task = _t
        self.__startPoint = self.task.getSourceTask()
        self.__targetPoint = self.task.getTargetTask()
        self.__endPoint = self.task.getDestinationTask()
        self.__shortVersionPathMission = self.createShortVersionPathMission()
        self._initAdj = self.circuit.initMatAdj().copy()

        self.__path, self.__total_distance = self.calculateEntirePath()
        self.finPath = len(self.__path)-1
        self._trackingTravelledDistance = []
        self.agent_transmitter = agent_transmitter

    def getNumWorker(self):
        """getNumWorker(self) -> (int) Return the identifier of the Worker assigned to the mission."""
        return self.__numWorker

    def getTask(self):
        """getTask(self) -> (Task) Return the task assigned to the Worker for the mission."""
        return self.task

    def getStartPoint(self):
        """getStartPoint(self) -> (int) Return the starting point of the task assigned to the Worker."""
        return self.__startPoint

    def getTargetPoint(self):
        """getTargetPoint(self) -> (int) Return the target point of the task assigned to the Worker."""
        return self.__targetPoint

    def getEndPoint(self):
        """getEndPoint(self) -> (int) Return the ending point of the task assigned to the worker."""
        return self.__endPoint

    def getPath(self):
        """getPath(self) -> (list) Return the current value of the __path attribute."""
        return self.__path
    
    def getAgentTransmitter(self):
        """
        Retrieve the identity of the agent transmitter associated with this worker mission.

        Returns:
        - AgentTransmitter (dict): The identity of the agent transmitter responsible for communication in this worker mission.
        """
        return self.agent_transmitter

    def calculateTaskPath(self, _s, _d):
        """
        Calculate the shortest path between the start and end nodes of the mission using Dijkstra's algorithm.

        Args:
            _s (int): the ID of the start node
            _d (int): the ID of the end node

        Returns:
            path (list): a list of node IDs representing the shortest path between the start and end nodes
            min_distance (float): the length of the shortest path in terms of edge weights
        """
        #print("Adjacence matrix: {}".format(self._initAdj))
        min_distance, pred = Dijkstra.Dijkstra(self._initAdj, _s, _d)
        path = Dijkstra.shortestPath(_s, _d, pred)
        return path, min_distance

    def calculateEntirePath(self):
        """
        Calculate the entire path of the mission. 

        Returns:
            total_path (list): a list of node IDs representing the entire path of the mission
            total_distance (float): the length of the shortest path in terms of edge weights of the entire path
        """
        #min_distance1 : all the min_distance from the source to all nodes of the circuit		
        path1, min_distance1 = self.calculateTaskPath(self.__startPoint, self.__targetPoint)
        distance_task1 = min_distance1[self.__targetPoint] # to obtain the min_distance from the source node to the _d : destination node
        
        path2, min_distance2 = self.calculateTaskPath(self.__targetPoint, self.__endPoint)
        #min_distance2 : all the min_distance from the source to all nodes of the circuit
        distance_task2 = min_distance2[self.__endPoint]  # to obtain the min_distance from the source node to the d : destination node
        del path2[0]
        
        # A complete path is : path1 + path2
        total_path = path1 + path2
        total_distance = distance_task1 + distance_task2 

        txt = "Total path: {}\n Total distance: {}".format(total_path, total_distance)
        eventTrace(txt, missionTrace)

        return total_path, total_distance

    def getStartPoint(self):
        """getStartPoint(self) -> (int) Return the starting point of the mission."""
        return self.__startPoint

    def getEndPoint(self):
        """getEndPoint(self) -> (int) Return the ending point of the mission."""
        return self.__endPoint

    def getTargetPoint(self):
        """getTargetPoint(self) -> (int) Return the target point of the mission."""
        return self.__targetPoint

    def getNumPos(self):
        """getNumPos(self) -> Return the number pos of the Worker."""
        return self.__numPos

    def createShortVersionPathMission(self):
        """createShortVersionPathMission(self) -> (list) Create and return a short version of the path mission."""
        shortVersionPathMission = []
        shortVersionPathMission.append(self.__startPoint) # source node of the path
        shortVersionPathMission.append(self.__targetPoint) # node of the target point 
        shortVersionPathMission.append(self.__endPoint) # desination node of the path
        return shortVersionPathMission

    def getShortVersionPathMission(self):
        """getShortVersionPathMission(self) -> (list) Return the short version of the path mission."""
        return self.__shortVersionPathMission

    def getTargetValidated(self):
        """getTargetValidated(self) -> (boolean) Return the current value of the __targetValidated attribute."""
        return self.__targetValidated

    def setTargetValidated(self, value):
        """
        Set the value of the __targetValidated attribute to the specified value.

        Args:
            value (boolean): The value to set the __targetValidated attribute to.
        """
        self.__targetValidated = value

    def getTrackingTravelledDistance(self):
        """getTrackingTravelledDistance(self) -> Return the tracking travelled distance of the mission."""
        return self._trackingTravelledDistance		

    def calculateTravelledDistance(self):
        """
        Calculate and return the total distance travelled during the mission.

        Return:
            dist (int): the total distance travelled during the mission.
        """
        dist = 0
        for i in range(len(self._trackingTravelledDistance[:-1])): # delete the last track distance node which corresponds to the next move
            dist = dist + self._trackingTravelledDistance[i][2]
        return dist
        
    def changeNumPos(self):
        """Update the position of a vehicle along a given path."""
        #txt = "VA {} with self.__numPos = {} and self.__path = {}".format(self.__numVA, self.__numPos, self.__path)
        #eventTrace(txt, simulationTrace)

        try :
            if (self.__path[self.__numPos] == self.__targetPoint):
                self.setTargetValidated(True)
                self.__numPos = self.__numPos + 1
            elif(self.__path[self.__numPos+1] == self.__endPoint):
                self.__numPos = self.__numPos + 1	
            else:
                self.__numPos = self.__numPos + 1
        except IndexError :
            txt = f"IndexError : for VA{self.getNumWorker()}"
            eventTrace(txt, errorTrace)
            txt = f"numPos {self.__numPos} ; path{self.__path} ; __target {self.__targetPoint} ; __endPoint {self.__endPoint}"
            eventTrace(txt, errorTrace)

    def setPath(self):	#TODO: adapt new strategy
        """Set the path for the current mission based on the current numPath attribute."""
        self.__path = self.calculateTaskPath(self.__numPath)

    def getPath(self):
        """getPath(self) -> Return the current path for the mission."""
        return self.__path

    def setTotalDistancePath(self):
        """Set the total distance of the path for the current mission."""
        return self.__total_distance

    def getTotalDistancePath(self):
        """getTotalDistancePath(self) -> (int) Return the total distance of the path for the current mission."""
        return self.__total_distance

    def getEndPathWithPreviousCheckPoint(self):
        """getEndPathWithPreviousCheckPoint(self) -> (list) Return a copy of the end path for the current mission, with the previous checkpoint added to the beginning of the path."""
        end_path = self.getEndPath().copy()
        end_path_with_prev_checkpoint = [self.__path[self.__numPos-1]] + end_path 
        return end_path_with_prev_checkpoint

    def newDestination(self):
        """
        Update the position of the VA along the path and return the new destination node.

        Return: 
            dest(int): the new destination node of the VA.
        """
        self.changeNumPos()
        try:
            s, d = self.__path[self.__numPos - 1], self.__path[self.__numPos]
        except IndexError:
            txt = f"path: {self.__path}, {self.__numPos}"
            eventTrace(txt, errorTrace)

            return None
        
        l = self._initAdj[s][d]
            
        self._trackingTravelledDistance.append([s,d,l])
        return self.__path[self.__numPos]

    def getEndPath(self):
        """getEndPath(self) -> Return the end path for the current mission."""
        path = []
        for i in range(self.__numPos, self.finPath+1):
            path.append(self.__path[i])
            """
            try :
                path.append(self.__path[i])
            except IndexError: pass
                # because there is a path less large in list than the first one (but it is more expensive this one)
            """
        return path

    def calculateDistanceFromTrackingTravelledDistance(self, trackingTravelledDistance):
        """
        Calculate the total distance travelled by the Worker based on the given tracking of the distance travelled.
        Take a list of sub-paths where each sub-path is represented as a list [source_node, destination_node, cost_distance].
        
        Returns: 
            distance (float): Total distance travelled by the VA.
        """
        # trackingTravelledDistance = [source_node, destination_node, cost_distance]
        distance = 0
        for sub_path in trackingTravelledDistance:
            distance+= sub_path[2]
        return distance
    
    def setFirstPartTrackingTravelledDistance(self, value):
        """
        Set the value of firstPartTrackingTravelledDistance attribute for the current mission.
        
        Take a list of sub-paths where each sub-path is represented as a list [source_node, destination_node, cost_distance].
        """
        self.firstPartTrackingTravelledDistance = value

    def getSecondPartTrackingTravelledDistance(self):
        """
        Return the second part of the tracking of the distance travelled by the VA for the current mission. 
        
        If the vehicle has completed the first part of the mission, it return the tracking of the distance travelled after the first part of the mission. 
        
        If the vehicle has not yet completed the first part of the mission, it return the tracking of the distance travelled for the entire mission.

        Returns: 
            _trackingTravelledDistance (list): a list of sub-paths where each sub-path is represented as a list [source_node, destination_node, cost_distance].
        """
        # this function is exact if the mission is at the end
        if hasattr(self, 'firstPartTrackingTravelledDistance'):
            lastCheckPointOfFirstPart = self.firstPartTrackingTravelledDistance[-1]
            for partTrack in self._trackingTravelledDistance : 
                if partTrack == lastCheckPointOfFirstPart: 
                    # return all the track travelled distance after the first part of the mission
                    return self._trackingTravelledDistance[self._trackingTravelledDistance.index(partTrack)+1:]
        else: 
            # if the VA get a mission shared with the second task to do, therefore it did not have a self.firstPartTrackingTravelledDistance defined
            return self._trackingTravelledDistance