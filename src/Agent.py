# Agent class for the VA simulation application with the warehouse circuit
from Console import *

class Agent():
	"""
	Agent class for the VA simulation application.
	
	Attributes:
		__MaxAGENT (int): A private class attribute representing the maximum number of agents allowed in the simulation.
		__stop (bool): A private class attribute representing the stop flag for the simulation.
		agents (list): A class attribute representing the list of all agents in the simulation.
		id (int): A class attribute representing the static variable for the agent id.
	"""
	__MaxAGENT = 100
	__stop = False

	agents = []

	id = 0
	
	def __init__(self, type_agent, num):
		"""Constructor.

		Args:
			type_agent (str): Type of the agent, e.g., "VA", "Camera", etc.
			num (int): The number of the specific type of agent. 
		"""
		super().__init__()	# Launch of the run() function
		Agent.id+=1
		self.id_agent = Agent.id 
		self.active = True	

		self.type_agent = type_agent
		self.num = num
		self.identity = {"id_agent": self.id_agent, "type" : self.type_agent, "num": self.num}

		Agent.agents.append(self)

	def getIdentity(self):
		"""getIdentity(self) -> dict: Return the identity of the agent with: id_agent, type, num."""
		return self.identity

	def getId(self):
		"""getId(self) -> int: Return the id of the agent."""
		return self.id_agent
	
	def getActive(self):
		"""getActive(self) -> bool: Return the attribute active."""
		return self.active

	def getAgentName(self):
		"""getAgentName(self) -> str: Return the name of the agent by concatenating the type and number of the agent."""
		return self.identity["type"]+str(self.identity["num"])

	def getTypeAgent(self):
		"""getTypeAgent(self) -> str: Return the type of the agent."""
		return self.type_agent
	
	def getNumType(self):
		"""getNumType(self) -> int: Return the number of the specific type of agent."""
		return self.num
	
	def setType(self, type_ag):
		"""
		Set the type of the agent to the specified value.

    	Args:
        	type_ag (str): the new type of the agent.
		"""
		self.type_agent = type_ag
	
	def getStop(self):
		"""getStop(self) -> bool: Return the value of the stop attribute of the agent."""
		return self.__stop
	
	def terminate(self):
		"""
		Set the active attribute of the agent to False. 
		
		Display a message indicating the termination of the agent.
		"""
		self.active = False
		txt = f"Terminate of agent {self.identity}"
		eventTrace(txt, simulationTrace)
	
	def receive(self, r, e, _msg):
		"""
		Print a message indicating that the agent has received a message.

		Args:
			r: The receiver agent of the message.
			e: The emitter agent of the message.
			_msg: The message received by the agent.
		"""
		print(self.getName(), " receive a message : ", _msg.typeMSG, " !!!")
	
	def send(self, e, r, _msg):
		"""Print a message indicating that the agent has sent a message."""
		print(self.getName(), " send a messsage : ", _msg.typeMSG, " !!!")

	@staticmethod
	def getAgents():
		"""getAgents() -> list: Return the list of all agents in the simulation."""
		return Agent.agents
	
	@staticmethod
	def getSpecificAgent(agent_name):
		"""
		Retrieve a specific Agent instance from the simulation.

		Parameters:
		- agent_name (str): The name of the agent to retrieve.

		Returns:
		- Agent: The Agent instance corresponding to the provided agent_name in the simulation.
		"""
		for agent in Agent.agents:
			
			if agent.getAgentName() == agent_name:
				return agent
	
	@staticmethod
	def deleteVAAgents():
		"""Static method to delete all VA agents from the simulation."""
		Agent.agents = [agent for agent in Agent.agents if agent.type_agent != 'VA']