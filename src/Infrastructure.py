#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Infrastructure class for the VA simulation interfacecation with the Warehouse circuit

from threading import Thread
import time, random
import numpy as np
from Camera import Camera
from Console import *
from Agent import *
from Warehouse_Circuit import *
from Tag import *

class Infrastructure(Agent):
	"""
    A class representing the infrastructure of a facility that contains RFID tags and cameras for tracking objects.
    
    Inherits from the Agent class.
    
    Attributes:
		__stop (bool): A private attribute representing whether the Infrastructure is running.
		__tabTAG (list): A private list containing all the RFID tags in the Infrastructure.
		nbTAG (int): The number of RFID tags in the Infrastructure.
		loc (list): A list containing the locations of all the nodes of the circuit.
		cameras (list): A list containing all the Camera objects in the Infrastructure.
	"""

	__stop = False
	__tabTAG = []
	
	
	def __init__(self, env, interface):
		"""
        Constructor: initializes the Infrastructure object.
        
        Args:
        	env (Environment): The environment where the Infrastructure will be run.
        	interface (Interface): The interface of the simulation.
        """
		self.env = env
		self.interface = interface
		
		# Creation of RFID tags
		self.nbTAG = self.interface.circuit.getNbTAG()
		self.loc = self.interface.circuit.getLoc()
		typeTAG = self.interface.circuit.getTypeTAG()
		for i in range (self.nbTAG):
			tag = Tag(self, i, self.loc[i], typeTAG[i])
			self.__tabTAG.append(tag)
		
		# Place cameras 
		self.cameras = []
		self.placeCameras()
		
		Thread.__init__(self)	# Launch the run() function
		
	
	def run(self):
		"""Run the Infrastructure until it is stopped."""
		appliTrace("Infrastructure active")
		while (self.__stop != True):	
			alea = random.randint(0, 10)
			time.sleep(alea)
		self.terminate()
	
	def distance(self, origin, destination):
		"""
        Calculate the distance between two points on the circuit.
        
        Args:
        	origin (list): The coordinates of the origin point.
        	destination (list): The coordinates of the destination point.
        
        Returns:
        	d (float): The distance between the two points.
        """
		x1, y1 = origin[0], origin[1]
		x2, y2 = destination[0], destination[1]
		d = np.abs(x1 - x2) + np.abs(y1 - y2)
		return d
	
	def readingTags(self, _num, _coord, _infoOldTag):
		"""
        Read RFID tags in the Infrastructure and return tag information.
        
        Args:
        	_num (int): The ID of the tag being read.
        	_coord (tuple): The coordinates of the tag being read.
        	_infoOldTag (list): A list containing information about the tag that was previously read.
        
        Returns:
        	res (list): A list containing information about the tag that was just read.
        """

		res = [-1, -1, -1, -1]
		for i in range (self.nbTAG):
			if self.distance(_coord, self.loc[i]) < 5:
				res = self.__tabTAG[i].receiveTAG(_num)
				if (res[3] == 2 and _infoOldTag[3] == -1) :
					self.changeColorTag(i,"red")
				elif (res[3] == 2 and _infoOldTag[3] == 2) :
					self.changeColorTag(i,"red")
				elif (res[3] == 1 and _infoOldTag[3] == 2) :
					self.changeColorTag(_infoOldTag[0],"white")
				elif (res[3] == 1 and _infoOldTag[3] == 1) :
					self.changeColorTag(_infoOldTag[0],"white")
				elif (res[3] == 2 and _infoOldTag[3] == 1) :
					self.changeColorTag(i,"red")
				else:
					self.changeColorTag(i,"white")
		if (res[3] == -1 and _infoOldTag[3] == 2) :
			self.changeColorTag(_infoOldTag[0],"white")
		return res
	
	def changeColorTag(self, _numTag, _color):
		"""
		Changes the color of a RFID tag.

		Args:
			_numTag (int): The tag number to change the color of.
			_color (str): The new color for the tag.
		"""
		self.interface.changeColorTag(_numTag, _color)
	
	def placeCameras(self):
		"""
		placeCameras(self) -> Place cameras around the circuit.
		"""
		for id in self.interface.circuit.getCameras():
			position = self.interface.circuit.getPositionCamera(id)
			area_size = self.interface.circuit.getAreaSizeOfCamera(id)
			c = Camera(self.interface, self.env, id, position, area_size)
			self.cameras.append(c)

	def getCameras(self):
		"""
		getCameras(self) -> Return the cameras currently placed around the circuit.
		"""
		return self.cameras
