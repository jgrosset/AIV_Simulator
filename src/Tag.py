#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Tag class for the VA simulation application with the Warehouse circuit
# Tag seen as an object to start with

import time

#Classe Tag RFID
class Tag (object):
	"""
	The Tag class represents a Tag RFID object for the VA simulation application in the Warehouse circuit. 
	
	It stores the tag number, location, type, and color, and provides methods to change its color and terminate its activity.

	Attributes:
		__numTag (int): The tag number.
		__locTag (List[int]): The tag location.
		__typeTag (int): The tag type.
		__colorTag (str): The tag color.
		__active (bool): Whether the tag is active.
	"""

	__numTag = -1
	__locTag = [-1, -1]
	__typeTag = -1
	__colorTag = "white"
	__active = False
	
	def __init__(self, _appli, _num, _loc, _typeTag):
		"""Initializes the Tag object with the given application, tag number, location, and type."""
		self.__numTag = _num		
		self.__locTag = _loc		#localisation of the Tag
		self.__typeTag = _typeTag
		self.__appli = _appli		# the application to access the Hmi
		self.__active = True # Tag in activity
	
	def getNumTag(self):
		"""getNumTag(self) -> (int) Returns the tag number."""
		return self.__numTag
	
	def getLocTag(self):
		"""getLocTag(self) -> (list[int]) Returns the tag location."""
		return self.__locTag
	
	def changeColor(self, _color):
		"""	
		Changes the tag color to _color. Modify the color in the application. 

		Args:
			_color (str): new color of the tag.
		"""
		self.__colorTag = _color
		self.__appli.changeColorTag(self.__numTag, self.__colorTag)
			
	def terminate(self):
		"""Terminates the tag activity."""
		super.terminate()
		self.__active = False
	
	def receiveTAG(self, _numVA):
		"""Receive the tag information.

		Args:
			_numVA (int): number of the VA which activates the Tag.

		Returns:
			List: [tag number, location x, location y, tag type]
		"""
		res = [self.__numTag, self.__locTag[0], self.__locTag[1], self.__typeTag]
		return res

