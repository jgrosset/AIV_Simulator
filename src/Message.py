#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# Message class for the VIA simulation application with the Warehouse circuit

#typeMsg = "CPM", "MCM"

from Console import appliTrace


class Message:
	"""
	A class representing a message sent by a sender.

	Attributes:
		identity_sender: The identity of the sender.
		__typeMsg (str): The type of the message.
		__valueMsg (any): The value of the message.
	"""
	def __init__(self, identity, typeMsg, valueMsg=None):
		"""
        Constructor: Initialize a new instance of the Message class.

        Parameters:
            identity: The identity of the sender. Example: {'id_agent': 6, 'type': 'Camera', 'num': 4}
            typeMsg (str): The type of the message.
            valueMsg (any): The value of the message. Default is None.
        """
		self.identity_sender = identity
		self.__typeMsg = typeMsg
		self.__valueMsg = valueMsg

	def getIdentitySender(self):
		""" getIdentitySender(self) -> (dict) Return the identity of the sender."""
		return self.identity_sender

	def setValueMsg(self, valueMsg):
		"""
        Set the value of the message.

        Parameters:
            valueMsg (any): The value of the message.
        """
		self.__valueMsg = valueMsg
	
	def getTypeMsg(self):
		"""getTypeMsg(self) -> (str) Return the type of the message."""
		return self.__typeMsg
	
	def getValueMsg(self):
		"""getValueMsg(self) -> (any) Return the value of the message."""
		return self.__valueMsg

class CPM(Message): # Cooperative Perception Message
	"""
    A message class for Cooperative Perception Message.

    Attributes:
        identity_sender (dict): The sender identity of the message.
        pos_obstacle (tuple): The position of the obstacle.
        size_obstacle (tuple): The size of the obstacle.
    """
	def __init__(self,  identity_sender, pos_obstacle, size_obstacle):
		"""
        Constructor: Initialize a new instance of the Cooperative Perception Message class.

        Args:
            identity_sender (dict): The sender identity of the message.
            pos_obstacle (tuple): The position of the obstacle.
            size_obstacle (tuple): The size of the obstacle.
        """
		super().__init__(identity_sender, "CPM", None)
		self.pos_obstacle = pos_obstacle
		self.size_obstacle = size_obstacle
		self.__typeMsg = "CPM"
	
	def getPosObstacle(self):
		"""getPosObstacle(self) -> (tuple) Return the position of the obstacle. """
		return self.pos_obstacle

	def getSizeObstacle(self):
		"""getSizeObstacle(self) -> (tuple) Return the size of the obstacle. """
		return self.size_obstacle
	
class DENM(Message): # Decentralized Environmental Message
	"""
    A message class for Decentralized Environmental Message.

    Attributes:
        identity_sender (dict): The sender identity of the message.
        content (dict): Content of the message
	
	Example: 
		denm_content = {"type" : "obstacle", "position": pos_obstacle, "size" : size}
    """
	def __init__(self,  identity_sender, content):
		"""
        Constructor: Initialize a new instance of the Cooperative Perception Message class.

        Args:
            identity_sender (dict): The sender identity of the message.
            content (dict): Content of the message
        """
		super().__init__(identity_sender, "DENM", None)
		self.content = content
		self.__typeMsg = "DENM"
	
	def getContent(self):
		return self.content

	def getPosObstacle(self):
		"""getPosObstacle(self) -> (tuple) Return the position of the obstacle. """
		return self.content['pos_obstacle']

	def getSizeObstacle(self):
		"""getSizeObstacle(self) -> (tuple) Return the size of the obstacle. """
		return self.content['size_obstacle']


class MCM(Message): # Maneuver Coordination Message
	"""
    A message class for Maneuver Coordination Message.

    Attributes:
        identity_sender (dict): The sender identity of the message.
        num_intersection (int): The number of intersections.
        entry_or_exit (int): The entry or exit value of the message. Situations in relation to intersections: OUT, IN, CENTER = 0, 1, 2
	"""

	def __init__(self,  identity_sender, num_intersection, entry_or_exit):
		"""
        Constructor: Initialize a new instance of the Maneuver Coordination Message class.

        Args:
            identity_sender (dict): The sender identity of the message.
            num_intersection (int): The number of intersections.
            entry_or_exit (int): The entry or exit value of the message. Situations in relation to intersections: OUT, IN, CENTER = 0, 1, 2
        """
		super().__init__(identity_sender, "MCM", None)
		self.num_intersection = num_intersection
		self.entry_or_exit = entry_or_exit #Situations in relation to intersections : OUT, IN, CENTER = 0, 1, 2
		self.__typeMsg = "MCM"

	def getNumIntersection(self):
		"""getNumIntersection(self) -> (int) Return the number of intersections. """
		return self.num_intersection

	def getEntryOrExitValue(self):
		"""getEntryOrExitValue(self) -> (int) Return the entry or exit value of an intersection."""
		return self.entry_or_exit

class CRM(Message):
	"""
	Cooperative Response Message class.
    
    Feedback on a Cooperative Message.
    """
	def __init__(self, identity_sender, identity_receiver, msg, response):
		super().__init__(identity_sender, "CRM", None)
		self.identity_receiver = identity_receiver
		self.response = response # value : True or False 
		self.msg_to_answer = msg
		self.__typeMsg = "CRM"

	def getResponse(self): 
		"""getResponse(self) -> (boolean) Return the response of a CRM object."""
		return self.response

	def getMsgToAnswer(self):
		"""getMsgToAnswer(self) ) -> (Message) Return the message that needs to be answered by a CRM object."""
		return self.msg_to_answer

class CTM(Message): # Cooperative Tasks Message
	"""
	Cooperative Tasks Message class.
    
    CTM message type can be "bids", "tasks", or "missions".
    """
	# type : , "tasks", "missions"
	def __init__(self, identity_sender, clustered, assigned, type, content, partTaskAccomplished = 0):
		super().__init__(identity_sender, "CTM", None)
		self.clustered = clustered # value: True or False
		self.type = type # value: "bids" "tasks" "mission"
		self.content = content
		self.assigned = assigned # value : True or False depending its missions is assigned or must to be bid on
		self.__typeMsg = "CTM"
		# partTaskAccomplished: int
		# corresponding to the part of the task accomplished ()
		self.partTaskAccomplished = partTaskAccomplished

	def getClustered(self):
		"""getClustered(self) -> (boolean) Return whether the CTM object is clustered."""
		return self.clustered

	def getAssigned(self):
		"""getAssigned(self) -> (boolean) Return whether the CTM object is assigned."""
		return self.assigned

	def getType(self):
		"""getType(self) -> (any) Return the type of the CTM object: "bids", "tasks", "missions" """
		return self.type	

	def getContent(self):
		"""getContent(self) -> (any) Return the content of the CTM object."""
		return self.content

	def getPartTaskAccomplished(self):
		"""getPartTaskAccomplished(self) -> (int) Return the part of the task accomplished by the CTM object."""
		return self.partTaskAccomplished
